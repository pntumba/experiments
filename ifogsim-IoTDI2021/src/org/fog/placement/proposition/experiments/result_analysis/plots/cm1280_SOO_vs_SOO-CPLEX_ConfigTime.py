import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [512,700,965,1221,1500,1792,2000,2100,2256]
TIME_SOO = [17,18,14,16,75,95,78,94,97]
TIME_SOO_CPLEX = [340,294,277,292,236,208,350,274,269]
x_pos = np.arange(len(data_stream_rates))

fig, ax = plt.subplots()
ax.plot(TIME_SOO_CPLEX,data_stream_rates , "--b", label="SOO-CPLEX", markersize=8, markevery=1)
ax.plot(TIME_SOO, data_stream_rates, "-r", label="SOO", markersize=8)
ax.yaxis.grid(True)
ax.xaxis.grid(True)
ax.legend()
plt.ylabel('Data stream rate produced at the Edge (KB/sec)')
plt.xlabel('Resolution time (ms)')
plt.show()


