package org.fog.placement.proposition.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;


public class Graph {

	private Map<Integer, HashMap<Integer, Double>> adjVertices;
	private Queue<Integer> topologicalOrder;
	
	public Graph(){
		
		
		adjVertices = new HashMap<Integer, HashMap<Integer, Double>>();
		
	}
	
	
	public void addVertex(int vertex) {

		adjVertices.putIfAbsent(vertex, new HashMap<Integer, Double>());
	}

	public void removeVertex(int vertex) {

		adjVertices.remove(vertex);
	}

	public boolean addEdge(int vertex1, int vertex2, double weight) {

		adjVertices.get(vertex1).put(vertex2, weight);
		return true;
	}

	public boolean setEdgeWeight(int vertex1, int vertex2, double weight) {

		if (adjVertices.get(vertex1).size() > 0) {

			adjVertices.get(vertex1).put(vertex2, weight);
			return true;
		}
		return false;
	}

	public boolean addEdge(int vertex1, int vertex2) {

		adjVertices.get(vertex1).put(vertex2, 0.0);
		return true;
	}

	public void removeEdge(int vertex1, int vertex2){

		HashMap<Integer, Double> eV1 = adjVertices.get(vertex1);

		if (eV1 != null)
			eV1.remove(vertex2);
	}

	public double getEdgeWeight(int vertex1, int vertex2){
		
		return adjVertices.get(vertex1).get(vertex2);
	}
	
	public double getRawStream(int vertex){
		
		double rawStream = 0.0;
		if(isSource(vertex)) {
			
			for(int vertex_succ : getSuccessors(vertex)) {
				
				rawStream = adjVertices.get(vertex).get(vertex_succ);
			}
		}
		return rawStream;
	}
	
	public boolean isSource(int vertex) {
		
		boolean isSource = false;
		if(getPredecessors(vertex).isEmpty()) {
			
			isSource = true;
		}
		return isSource;
	}

	public boolean isSink(int vertex) {

		boolean isSink = false;
		if(getSuccessors(vertex).isEmpty()) {

			isSink = true;
			
		}else if(!getAdjVertices().containsKey(vertex)) {
			
			isSink = true;
		}
		return isSink;
	}
	
	public int getSource() {
		
		int source = -1;
		for(int vertex1: getAdjVertices().keySet()) {
			
			if(getPredecessors(vertex1).isEmpty()){
				
				source = vertex1;
			}
		}
		return source;
	}
	
	public int getSink() {
		 int sink = -1;
		 
		 for(int vertex1: getAdjVertices().keySet()) {
			 
			 if(getSuccessors(vertex1).isEmpty()) {
				 
			
				 sink = vertex1;
				 
			 }else {
				 
				 for(int vertex2: getSuccessors(vertex1)) {
					 
					 if(!getAdjVertices().containsKey(vertex2)) {
						 
						 sink = vertex1;
					 }
				 }
			 }
			 
		 }
		 return sink;
	}
	public Set<Integer> getPredecessors(int vertex) {

		Set<Integer> predecessors = new HashSet<Integer>();
		for (int vertex1 : getAdjVertices().keySet()) {

			for (int vertex2 : getAdjVertices(vertex1).keySet()) {

				if (vertex == vertex2) {

					predecessors.add(vertex1);
				}
			}

		}
		return predecessors;

	}
	
	
	public Set<Integer> getSuccessors(int vertex1){
		
		if(adjVertices.get(vertex1) == null)
			return  new HashSet<Integer>();
		return adjVertices.get(vertex1).keySet();
	}

	public HashMap<Integer, Double> getAdjVertices(int vertex1) {

		return adjVertices.get(vertex1);
	}

	public Map<Integer, HashMap<Integer, Double>> getAdjVertices() {
		return adjVertices;
	}
	
	
	public void sortGraph() {
		
		
		Map<Integer, HashMap<Integer, Double>> treeMap = new TreeMap<Integer, HashMap<Integer, Double>>(adjVertices);
		this.adjVertices = treeMap;
	}
	public void sortIntopologicalOrder() {

		// Create a array to store indegrees of all
		// vertices. Initialize all indegrees as 0.
		int indegree[] = new int[100];

		// Traverse adjacency lists to fill indegrees of
		// vertices. This step takes O(V+E) time

		for (int vertex1 : getAdjVertices().keySet()) {

			for (int vertex2 : getAdjVertices(vertex1).keySet()) {
				if (getAdjVertices().containsKey(vertex2)) {

					indegree[vertex2]++;
				}
			}
		}

		// Create a queue and enqueue all vertices with
		// indegree 0
		Queue<Integer> q = new LinkedList<Integer>();
		for (int vertex : getAdjVertices().keySet()) {
			if (indegree[vertex] == 0) {

				q.add(vertex);
			}

		}

		// Initialize count of visited vertices
		int cnt = 0;

		// Create a vector to store result (A topological
		// ordering of the vertices)
	
		Queue<Integer> topologicalOrder = new LinkedList<Integer>();
		while (!q.isEmpty()) {
			// Extract front of queue (or perform dequeue)
			// and add it to topological order
			int vertex1 = q.poll();
			topologicalOrder.add(vertex1);

			// Iterate through all its neighbouring nodes
			// of dequeued node u and decrease their in-degree
			// by 1

			for (int vertex2 : getAdjVertices(vertex1).keySet()) {
				// If in-degree becomes zero, add it to queue
				if (--indegree[vertex2] == 0)
					q.add(vertex2);
			}
			cnt++;

		}

		/*// Check if there was a cycle
		if (cnt != getAdjVertices().size()) {
			System.out.println("There exists a cycle in the graph");
			return;
		}*/
		setTopologicalOrder(topologicalOrder);
	}

	public Queue<Integer> getTopologicalOrder() {
		return topologicalOrder;
	}


	
	public Object clone()throws CloneNotSupportedException{  
        
		return (Graph)super.clone();  
    }
	
	public void setTopologicalOrder(Queue<Integer> topologicalOrder) {
		this.topologicalOrder = topologicalOrder;
	}
}
