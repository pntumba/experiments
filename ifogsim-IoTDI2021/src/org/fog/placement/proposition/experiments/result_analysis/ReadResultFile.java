package org.fog.placement.proposition.experiments.result_analysis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class ReadResultFile {

	private final static String path = "/home/pntumba/inria_code/phd_project/PhD-3rd_Year_The-last-dance/IoTDI2021/Results/UR/output/";
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
	         readJsonFile("SOO_UR");
	}
	
	public static void readJsonFile(String pattern) throws IOException {
		
		for(int i=1; i<= 6; i++){
			
			
			double sum_B = 0.0;
			double sum_cru = 0.0;
			double sum_nru = 0.0;
			double sum_CRU = 0.0;
			double sum_NRU = 0.0;
			double sum_RU = 0.0;
			
			
			double sum_cmu_1 = 0.0;
			double sum_cmu_2 = 0.0;
			double sum_cmu_3 = 0.0;
			double sum_cmu_4 = 0.0;
			
			
			double min_B = Double.MAX_VALUE;
			double max_B = 0.0;
			double min_cru = Double.MAX_VALUE;
			double max_cru = 0.0;
			double min_CRU = Double.MAX_VALUE;
			double max_CRU = 0.0;
			double min_nru = Double.MAX_VALUE;
			double max_nru = 0.0;
			double min_NRU = Double.MAX_VALUE;
			double max_NRU = 0.0;
			double min_RU = Double.MAX_VALUE;
			double max_RU = 0.0;
			
			int it = i*2;
			JSONObject metric_object = new JSONObject();
			ArrayList<String> header = new ArrayList<String>();
			int countFiles = 0;
			StringBuilder sbConfiguration = null;
			
			Writer writer = CSVUtils.getWriter(pattern+"_"+it);
			Writer cdf_writer = CSVUtils.getWriter(pattern+"_"+it+"_CDF");
			Writer cmu_writer = CSVUtils.getWriter(pattern+"_"+it+"_CMU");
				
			CSVUtils.writeLine(writer, Arrays.asList("Config","B","nru","NRU","cru","CRU","RU"));
			CSVUtils.writeLine(cdf_writer, Arrays.asList("Config","B","nru","NRU","cru","CRU","RU"));
			CSVUtils.writeLine(cmu_writer, Arrays.asList("Config","Fog_1","Fog_2","Fog_3","Fog_4"));
			  
			
			for(int j=0; j<=10; j++){
			   
				
			   double data_size = (double)256000 + (100000 * j);
			  
			   String str_data_size = it+"_"+data_size;	
			  
			   for(int k=1; k<=10; k++){
				   
				   StringBuilder sb = new StringBuilder();
				   sbConfiguration = new StringBuilder(); 
				   sbConfiguration.append(pattern).append("_").append(str_data_size);
				   sbConfiguration.append("_").append(k);
				   sb.append(path+"result_experiemts_").append(sbConfiguration.toString()).append(".json");
				   String file = sb.toString();	 
				   
				   try{
					
					   File isFile = new File(file);
					   if(!isFile.exists()){continue;}
					   
					   System.out.println("File="+file);
					   countFiles++;
					   JSONObject object = (JSONObject) JSONValue.parse(new FileReader(file));
					   JSONArray metrics = (JSONArray) object.get("metrics");
					   for(int index = 0; index < metrics.size(); index++) {
						   
						   JSONObject obj = (JSONObject) metrics.get(index);
						   metric_object.putAll(obj);
					   }
					  
					   double _B = new BigDecimal( (double)(metric_object.get("B"))).doubleValue();
					   double _nru = new BigDecimal( (double)(metric_object.get("nru"))).doubleValue();
					   double _NRU = new BigDecimal( (double)(metric_object.get("NRU"))).doubleValue();
					   double _cru = new BigDecimal( (double)(metric_object.get("cru"))).doubleValue();
					   double _CRU = new BigDecimal( (double)(metric_object.get("CRU"))).doubleValue();
					   double _RU = new BigDecimal( (double)(metric_object.get("RU"))).doubleValue();
					   
					   if(_B < min_B){min_B = _B;}
					   if(_B > max_B){max_B = _B;}
					   
					   if(_nru < min_nru){min_nru = _nru;}
					   if(_nru > max_nru){max_nru = _nru;}
					   
					   if(_NRU < min_NRU){min_NRU = _NRU; }
					   if(_NRU > max_NRU){max_NRU = _NRU;}
					   
					   if(_cru < min_cru){min_cru = _cru; }
					   if(_cru > max_cru){max_cru = _cru;}
					   
					   if(_CRU < min_CRU){min_CRU = _CRU; }
					   if(_CRU > max_CRU){max_CRU = _CRU;}
					   
					   if(_RU < min_RU){min_RU = _RU;}
					   if(_RU > max_RU){max_RU = _RU;}
					   
					   sum_B = sum_B + _B;
					   sum_nru = sum_nru + _nru;
					   sum_NRU = sum_NRU + _NRU;
					   sum_cru = sum_cru + _cru;
					   sum_CRU = sum_CRU + _CRU;
					   sum_RU = sum_RU + _RU;
					   
					   JSONObject cmu = (JSONObject)object.get("cmu");
					   
					   double cmu_1 = new BigDecimal( (double)(cmu.get("Fog_1"))).doubleValue();
					   double cmu_2 = new BigDecimal( (double)(cmu.get("Fog_2"))).doubleValue();
					   double cmu_3 = new BigDecimal( (double)(cmu.get("Fog_3"))).doubleValue();
					   double cmu_4 = new BigDecimal( (double)(cmu.get("Fog_4"))).doubleValue();
					   
					   sum_cmu_1 = sum_cmu_1 + cmu_1;
					   sum_cmu_2 = sum_cmu_2 + cmu_2;
					   sum_cmu_3 = sum_cmu_3 + cmu_3;
					   sum_cmu_4 = sum_cmu_4 + cmu_4;
					   
				} catch (FileNotFoundException e) {e.printStackTrace();}
			   }
			   
			   
			   StringBuilder sbFile = new StringBuilder();
				 sbFile.append(path+"result_experiemts_").append(sbConfiguration.toString()).append(".json");
				 
				 String file = sbFile.toString();	   
				
				 File isFile = new File(file);
				 if(isFile.exists()){
				
					ArrayList<String> cmu_data = new ArrayList<String>();
					cmu_data.add(String.valueOf(data_size));
					cmu_data.add(String.valueOf(sum_cmu_1/countFiles));
					cmu_data.add(String.valueOf(sum_cmu_2/countFiles));
					cmu_data.add(String.valueOf(sum_cmu_3/countFiles));
				    cmu_data.add(String.valueOf(sum_cmu_4/countFiles));
					CSVUtils.writeLine(cmu_writer, cmu_data);
					   
				   double B = sum_B / countFiles;
				   double nru = sum_nru / countFiles;
				   double NRU = sum_NRU / countFiles;
				   double cru = sum_cmu_1/countFiles + sum_cmu_2/countFiles + sum_cmu_3/countFiles + sum_cmu_4/countFiles;
				   
				   //double cru = sum_cru/countFiles;
				   
				   double CRU = sum_CRU / countFiles;
				   double RU = sum_RU / countFiles;
				   ArrayList<String> data = new ArrayList<String>();
				   data.add(String.valueOf(data_size));
				   data.add(String.valueOf(B));
				   data.add(String.valueOf(nru));
				   data.add(String.valueOf(NRU));
				   data.add(String.valueOf(cru));
				   data.add(String.valueOf(CRU));
				   data.add(String.valueOf(RU));
				   CSVUtils.writeLine(writer, data);
				   
				   ArrayList<String> cdf_data = new ArrayList<String>();
				   cdf_data.add(String.valueOf(data_size));
				   cdf_data.add(String.valueOf(cdf(B,min_B,max_B)));
				   cdf_data.add(String.valueOf(cdf(nru,min_nru,max_nru)));
				   cdf_data.add(String.valueOf(cdf(NRU,min_NRU,max_NRU)));
				   cdf_data.add(String.valueOf(cdf(cru,min_cru,max_cru)));
				   cdf_data.add(String.valueOf(cdf(CRU,min_CRU, max_CRU)));
				   cdf_data.add(String.valueOf(cdf(RU,min_RU, max_RU)));
				   CSVUtils.writeLine(cdf_writer, cdf_data);
				   
				   System.out.println("Op="+it+" "+data);
			   
				
			}
			
					
			 }
			CSVUtils.close(writer);
			CSVUtils.close(cdf_writer);
			CSVUtils.close(cmu_writer);
		}
		
		
	}
	
	
	private static double cdf(double value, double min, double max) {
		
		double cdf_value;
		if(value < min) {
			
			cdf_value = 0.0;
			
		}else if(value > max){
			
			cdf_value = 1.0;
			
		}else{
			cdf_value =  (value - min)/(max - min);
			if(value - min == 0.0)
				cdf_value = 0.0;
				
			
		}
		return cdf_value;
		
	}

}
