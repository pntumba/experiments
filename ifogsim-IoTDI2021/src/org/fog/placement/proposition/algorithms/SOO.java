package org.fog.placement.proposition.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

import javax.imageio.plugins.bmp.BMPImageWriteParam;

import org.apache.commons.math3.analysis.function.Cos;
import org.apache.commons.math3.util.ContinuedFraction;
import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.entities.FogDevice;
import org.fog.placement.proposition.monitoring.CostModel;
import org.fog.placement.proposition.utils.EdgeCut;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.MinMax;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.placement.proposition.utils.UtilFunctions;

public class SOO {

	private Graph graph;
	private Graph graph_rep;
	private double B;
	private double Buthr;
	private Map<Integer, Double> streamsSet;
	private Map<Integer, Double> cumulatedSelectivitiesForMigratedStreams;
	private Map<Integer, Graph> effectiveMapping = new HashMap<Integer, Graph>();
	private Map<Integer, Graph> initialMapping = new HashMap<Integer, Graph>();
	private Map<Integer, Graph> mappingAtDataMinCut = new HashMap<Integer, Graph>();
	private Map<Integer, Integer> sourcesToResourceNodes;
	private Map<Integer, ModuleType> operatorsType;
	private Map<Integer, Double> operatorsSelectivity;
	private Map<Integer, Boolean> operatorsReplicability;
	private Map<Integer, Integer> replicationAndMigrationPoints = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Map<Integer, EdgeCut>> allEdgeCuts = null;
	private Map<Integer, Map<Integer, Double>> allEdgeCutValues = null;
	private int edgeCutHopParameter = 0;
	private boolean betterSolution = false;

	private double w_c;
	private double w_n;
	private double deltaRU = 0.0;

	private MinMax compMinMax = UtilFunctions.compMinMax;
	private MinMax netwMinMax = UtilFunctions.netwMinMax;
	private Map<Integer, Double> processedDataStreams = new ConcurrentHashMap<Integer, Double>();

	private Map<Integer, Double> adjustedNetw_resource_usage_cost = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> adjustedComp_resource_usage_cost = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> deltaRU_set = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> operatorCosts = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Queue<Integer>> datastream_deltaRU_index = new ConcurrentHashMap<Integer, Queue<Integer>>();
	
	private Map<Integer, EdgeCut> acceptedEdgeCutHops = new ConcurrentHashMap<Integer, EdgeCut>();
	private Map<Integer, Double> contributiontoRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> contributiontoCRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> contributiontoNRU = new ConcurrentHashMap<Integer, Double>();
	
	private Map<Integer, Integer> edgeCutIndex_per_deltaRU = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Double> nbu_per_deltaRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> cmu_per_deltaRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Graph> graph_mig_per_deltaRU = new ConcurrentHashMap<Integer, Graph>();
	private ArrayList<Integer> stream_to_update = new ArrayList<Integer>();


	public SOO(Graph graph, Map<Integer, ModuleType> operatorsType, Map<Integer, Double> operatorsSelectivity,
			Map<Integer, Double> operatorCosts, Map<Integer, Boolean> operatorsReplicability){

		this.graph = graph;
		this.operatorsReplicability = operatorsReplicability;
		this.operatorsSelectivity = operatorsSelectivity;
		this.operatorsType = operatorsType;
		this.cumulatedSelectivitiesForMigratedStreams = new ConcurrentHashMap<Integer, Double>();
		this.operatorCosts = operatorCosts;
		compMinMax = UtilFunctions.compMinMax;
		netwMinMax = UtilFunctions.netwMinMax;
		
	}

	public void initialConfiguration(double nd_FC, double nd_EF, double nb_F_jC, Map<Integer, Double> streamsSet) {

		System.out.println("\nStart identifying RU min-cut or data min cuts for all data streams\n");
		allEdgeCuts = new ConcurrentHashMap<Integer, Map<Integer, EdgeCut>>();
		allEdgeCutValues = new ConcurrentHashMap<Integer, Map<Integer, Double>>();
		this.Buthr = nb_F_jC;
		this.streamsSet = streamsSet;
		this.graph_rep = UtilFunctions.replicableGraph(graph, operatorsReplicability);
	
		Queue<Integer> streamQueue = queue(streamsSet);
		
		double cmu_F_j = 0.0;
		double cm_F_j = 0.0;
		double nbu_F_jC = 0.0;
		
		double cru_F_j_p = 0.0;
		double nru_F_j_p = 0.0;
		double CRU_j = 0.0;
		double NRU_j = 0.0;
		
		while (!streamQueue.isEmpty()){

			int S_j = streamQueue.remove();
			System.out.println("\n\t**Search RU min-cut for Gmig_" + S_j);
			int F_j = getSourcesToResourceNodes().get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			int memory_j = fogDevice.getHost().getRamProvisioner().getAvailableRam();
			UtilFunctions.setAvailableMemory(memory_j);
			int sourceVertex = S_j;
			int sinkVertex = graph_rep.getSink();
			Graph graph_candidat = UtilFunctions.candidatSubGraph(graph_rep, sourceVertex, sinkVertex,
					operatorsReplicability);
			System.out.println("\tgraph_candidat"+graph_candidat.getAdjVertices());
			double stream = streamsSet.get(S_j);
			UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_candidat, operatorsType, operatorsSelectivity,
					operatorCosts, sourceVertex, stream);
			Graph graph_sat_j = UtilFunctions.getGraphSatRequirement();
			cmu_F_j = 0.0;
			cm_F_j = UtilFunctions.getAvailableMemory();
			nbu_F_jC = stream;
			CostModel.getEdgeCutMin_fog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
			CostModel.getEdgeCutMin_fog_cpu_memory_usage().put(F_j, 0.0);
			
			cru_F_j_p = Double.MAX_VALUE;
			nru_F_j_p = Double.MAX_VALUE;
			CRU_j = cru_F_j_p / CostModel.getSource_number();
			NRU_j = nru_F_j_p;
			
			
			contributiontoCRU.put(S_j, CRU_j);
			contributiontoNRU.put(S_j, NRU_j);
			contributiontoRU.put(S_j, (CRU_j+NRU_j));
			
			CostModel.getFog_cpu_memory_usage().put(F_j, cmu_F_j);
			CostModel.getFog_max_cpu_memory().put(F_j, cm_F_j);
			CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
			CostModel.getFog_to_cloud_netw_delay().put(F_j, nd_FC);
			CostModel.getEdge_to_fog_netw_delay().put(F_j, nd_EF);
			processedDataStreams.put(S_j, nbu_F_jC);
			System.out.println("\tgraph_sat_"+S_j+" "+graph_sat_j.getAdjVertices());
			if (!graph_sat_j.getAdjVertices().isEmpty()){
				
				// Search data min cut 
				UtilFunctions.findAllEdgeCuts(graph_sat_j, operatorsSelectivity);
				EdgeCut minimumEdgeCut = new EdgeCut(UtilFunctions.getMinCut());
				Graph graph_to_replicate = UtilFunctions.candidatGraphToReplicate(graph_sat_j, sourceVertex,
						minimumEdgeCut.getEdgeCut());

				UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_to_replicate, operatorsType,
						operatorsSelectivity, operatorCosts, sourceVertex, stream);
				
				Graph graph_mig_j = UtilFunctions.getGraphSatRequirement();
				getCumulatedSelectivitiesForMigratedStreams().put(S_j, UtilFunctions.getCumulatedSelectivity());
				mappingAtDataMinCut.put(F_j, graph_mig_j);
				Integer edgeCutIdex = new Integer(UtilFunctions.getMinCutCountIndex());
				replicationAndMigrationPoints.put(S_j, edgeCutIdex);
				System.out.println("\tEdgeCut_"+S_j+" "+replicationAndMigrationPoints.get(S_j));

				Map<Integer, EdgeCut> edgeCuts = new HashMap<Integer, EdgeCut>(UtilFunctions.getAllEdgeCuts());
				allEdgeCuts.put(S_j, edgeCuts);

				Map<Integer, Double> edgeCutValues = new HashMap<Integer, Double>(UtilFunctions.getAllEdgeCutValues());
				getAllEdgeCutValues().put(S_j, edgeCutValues);

				cmu_F_j = UtilFunctions.getRequiredMemory();
				nbu_F_jC = UtilFunctions.getMinCutValue();
				CostModel.getEdgeCutMin_fog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
				CostModel.getEdgeCutMin_fog_cpu_memory_usage().put(F_j, cmu_F_j);
				
				for(int edgeCutIndex : allEdgeCuts.get(S_j).keySet()) {
					
					if(!getAllEdgeCutValues().get(S_j).containsKey(edgeCutIndex)) {
						continue;
					}
					EdgeCut edgeCut = edgeCuts.get(edgeCutIndex);
					graph_to_replicate = UtilFunctions.candidatGraphToReplicate(graph_mig_j, S_j, edgeCut.getEdgeCut());
					UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_to_replicate, operatorsType, operatorsSelectivity,operatorCosts, S_j, stream);
					Graph new_graph_mig_j = UtilFunctions.getGraphSatRequirement();
					cmu_F_j = UtilFunctions.getRequiredMemory();
					nbu_F_jC = getAllEdgeCutValues().get(S_j).get(edgeCutIndex);

					cru_F_j_p = cmu_F_j / cm_F_j;
					nru_F_j_p = (nbu_F_jC / nb_F_jC) * (nd_FC / nd_EF);
					CRU_j = cru_F_j_p / CostModel.getSource_number();
					NRU_j = nru_F_j_p;
					
					if(contributiontoRU.get(S_j) > (CRU_j + NRU_j)) {
						
						contributiontoCRU.put(S_j, CRU_j);
						contributiontoNRU.put(S_j, NRU_j);
						contributiontoRU.put(S_j, CRU_j+NRU_j);	
						if(UtilFunctions.decideToReplicateOnFog(new_graph_mig_j,edgeCut)) {
							
							initialMapping.put(F_j, new_graph_mig_j);
									
						}else{
							
							if(initialMapping.containsKey(F_j)) {
								
								initialMapping.remove(F_j);
							}
						}
						CostModel.getFog_cpu_memory_usage().put(F_j, cmu_F_j);
						CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);	
					}	
				}				
			}
			System.out.print("\tcontribution to CRU =" +contributiontoCRU.get(S_j));
			System.out.print("\tcontribution to NRU =" + contributiontoNRU.get(S_j));
			System.out.println("\tcontribution to RU =" +contributiontoRU.get(S_j));
			if(!initialMapping.containsKey(F_j)) {
				
				System.out.println("\tGmig_" + S_j + ": no graph to repliacte");
				
			}else {
				
				System.out.println("\tGmig_" + S_j + "= " + initialMapping.get(F_j).getAdjVertices());
			}
			
		}
		
		
		CostModel.calculateComp_resource_usage_cost();
		CostModel.calculateNetw_resource_usage_cost();
		CostModel.calculateResource_usage_cost();
		System.out.println("\nResource usage costs when considering RU min-cut");
		System.out.println("\tInitial B=" + CostModel.getFog_to_cloud_overall_netw_bwd_usage());
		System.out.println("\tInitial CRU=" + CostModel.getNorm_comp_resource_usage_cost());
		System.out.println("\tInitial NRU=" + CostModel.getNorm_netw_resource_usage_cost());
		System.out.println("\tInitial RU=" + CostModel.getResource_usage_cost()+"\n");
		B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
		if(B <= Buthr) {
			
			setBetterSolution(true);
			CostModel.getFog_cpu_memory_usage().clear();
			CostModel.getFog_to_cloud_netw_bwd_usage().clear();
		}
		else{
			
			System.out.print("\nCombinatorial case, constraint [B="+B+" <= Bmax="+Buthr+"] is not satisfied" );
			System.out.println("\tStart applying data min cut");
			
			
			for(int S_j : streamsSet.keySet()){
				
				System.out.println("\n\t**Search data min-cut for Gmig_" + S_j+"\n");
				int F_j = getSourcesToResourceNodes().get(S_j);
				nbu_F_jC = CostModel.getEdgeCutMin_fog_to_cloud_netw_bwd_usage().get(F_j);
				cmu_F_j = CostModel.getEdgeCutMin_fog_cpu_memory_usage().get(F_j);
				CostModel.getFog_cpu_memory_usage().put(F_j, cmu_F_j);
				CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
				initialMapping.put(F_j,  mappingAtDataMinCut.get(F_j));
				System.out.println("\tGmig_" + S_j + "= " + initialMapping.get(F_j).getAdjVertices());
				System.out.println("\tEdgeCut_" + S_j + "= " + replicationAndMigrationPoints.get(S_j));
				cru_F_j_p = cmu_F_j / cm_F_j;
				nru_F_j_p = (nbu_F_jC / nb_F_jC) * (nd_FC / nd_EF);
				CRU_j = cru_F_j_p / CostModel.getSource_number();
				NRU_j = nru_F_j_p;
				System.out.print("\tcmu=" + cmu_F_j);
				System.out.println("\tnbu=" + nbu_F_jC);
				contributiontoCRU.put(S_j,  CRU_j);
				contributiontoNRU.put(S_j, NRU_j);
				contributiontoRU.put(S_j, CRU_j + NRU_j);
				System.out.print("\tcontribution to CRU =" +contributiontoCRU.get(S_j));
				System.out.print("\tcontribution to NRU =" + contributiontoNRU.get(S_j));
				System.out.println("\tcontribution to RU =" + contributiontoRU.get(S_j));
			}
			CostModel.calculateComp_resource_usage_cost();
			CostModel.calculateNetw_resource_usage_cost();
			CostModel.calculateResource_usage_cost();
			System.out.println("\nResource usage costs when considering edge-cut data min-cut");
			System.out.println("\tInitial B=" + CostModel.getFog_to_cloud_overall_netw_bwd_usage());
			System.out.println("\tInitial CRU=" + CostModel.getNorm_comp_resource_usage_cost());
			System.out.println("\tInitial NRU=" + CostModel.getNorm_netw_resource_usage_cost());
			System.out.println("\tInitial RU=" + CostModel.getResource_usage_cost());
			B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
		}
		
	}
    
	
	public void adjustConfiguration(double nd_FC, double nd_EF, double nb_F_jC, Map<Integer, Double> streamsSet){

		boolean all_move = false;
		ArrayList<Integer> updated_datastream = new ArrayList<Integer>();
		if (replicationAndMigrationPoints.isEmpty()) {
			return;
		}

		if (getEdgeCutHopParameter() == 0) {

			setEdgeCutHopParameter(graph.getAdjVertices().size());
			all_move = true;
		}

			System.out.println("\nSearch edge-cut move per data stream to improve current RU="
					+ CostModel.getResource_usage_cost() + "\n");
			for (int S_j : replicationAndMigrationPoints.keySet()) {

				if (updated_datastream.contains(S_j) && all_move) {
					continue;
				}
				int F_j = getSourcesToResourceNodes().get(S_j);
				Graph graph_mig_j = initialMapping.get(F_j);
				int edgeCutIndex = replicationAndMigrationPoints.get(S_j);
				EdgeCut edgeCut = allEdgeCuts.get(S_j).get(edgeCutIndex);
				if (UtilFunctions.decideToReplicateOnFog(graph_rep, edgeCut)) {
					
					findBestMoves(S_j, nd_FC, nd_EF, nb_F_jC);

				} else {

					initialMapping.remove(F_j);
					double cmu_F_j_p = 0;
					double nbu_F_j_pC = streamsSet.get(S_j);
					double nru_F_j_p = (nbu_F_j_pC / nb_F_jC) * (nd_FC / nd_EF);
					adjustedNetw_resource_usage_cost.put(F_j, nbu_F_j_pC);
					adjustedComp_resource_usage_cost.put(F_j, cmu_F_j_p);
				}
			}
			
			System.out.println("\tAll deltaRU deltaRU_set=" + deltaRU_set + "\n");
			int selected_S_j = -1;
			Queue<Integer> sortedDeltaRU_set = null;
			sortedDeltaRU_set = UtilFunctions.ascendantSortMap(deltaRU_set);
			int deltaRU_index = sortedDeltaRU_set.remove();
			deltaRU = deltaRU_set.get(deltaRU_index);
//			sortedDeltaRU_set = UtilFunctions.ascendantSortMap(deltaRU_set);
			System.out.println("Start updating data streams based on the " + UtilFunctions.getOrdering()
			+ " deltaRU from deltaRU_set\n");
			while(deltaRU < 0){
					
				    int selected_S_j_index = -1;
					for(int i=0; i<stream_to_update.size();i++){
						
						int S_j = stream_to_update.get(i);
						if(datastream_deltaRU_index.get(S_j).contains(deltaRU_index)){
							
							selected_S_j = S_j;
							selected_S_j_index= i;
							break;
						}
					}
					System.out.println("\tSelect S_" + selected_S_j + " with the best  deltaRU(deltaRU_set["+deltaRU_index+"])=" + deltaRU);
					if(updated_datastream.contains(selected_S_j)) {
						
						System.out.println("\tIgnore this deltaRU="+deltaRU+" for S_" + selected_S_j + " , a deltaRU has been already applied\n");
						deltaRU=0;
						if(!sortedDeltaRU_set.isEmpty()) {
							
							deltaRU_index = sortedDeltaRU_set.remove();
							deltaRU = deltaRU_set.get(deltaRU_index);
						}
						continue;
					}
					int F_j = getSourcesToResourceNodes().get(selected_S_j);
					double current_nbu_F_j = CostModel.getFog_to_cloud_netw_bwd_usage().get(F_j);
					double new_nbu_F_j = nbu_per_deltaRU.get(deltaRU_index);
					double new_cmu_F_j = cmu_per_deltaRU.get(deltaRU_index);
					double cru_F_j_p = new_cmu_F_j / CostModel.getFog_max_cpu_memory().get(F_j);
					double nru_F_j_p = (new_nbu_F_j / nb_F_jC) * (nd_FC / nd_EF);
					double CRU_j = cru_F_j_p / CostModel.getSource_number();
					double NRU_j = nru_F_j_p;
					double RU_j = CRU_j+ NRU_j;
					System.out.print("\tcontribution to CRU =" + CRU_j);
					System.out.print("\tcontribution to NRU =" + NRU_j);
					System.out.println("\tcontribution to RU =" + RU_j);
					System.out.print("\tcurrent B=" + B);
					System.out.print("\texpected B=" + (B - current_nbu_F_j + new_nbu_F_j));
					System.out.print("\tBmax="+Buthr);
					if ((B - current_nbu_F_j + new_nbu_F_j) <= Buthr){
						System.out.println("\texpected B <= Bmax ? Yes");	
					int new_edgeCutIndex = edgeCutIndex_per_deltaRU.get(deltaRU_index);
					replicationAndMigrationPoints.put(selected_S_j, new_edgeCutIndex);
					CostModel.getFog_cpu_memory_usage().put(F_j, new_cmu_F_j);
					CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, new_nbu_F_j);
					CostModel.calculateComp_resource_usage_cost();
					CostModel.calculateNetw_resource_usage_cost();
					CostModel.calculateResource_usage_cost();
					B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
					System.out.print("\tupdate B=" + CostModel.getFog_to_cloud_overall_netw_bwd_usage());
					System.out.print("\tCRU=" + CostModel.getNorm_comp_resource_usage_cost());
					System.out.print("\tNRU=" + CostModel.getNorm_netw_resource_usage_cost());
					System.out.println("\tRU=" + CostModel.getResource_usage_cost());
					
					Graph graph_mig_j = graph_mig_per_deltaRU.get(deltaRU_index);
					if(graph_mig_j == null) {
						
						initialMapping.remove(F_j);
						
					}else{
						
						initialMapping.put(F_j, graph_mig_j);
						
					}
					System.out.print("\tUpdate Gmig_" + selected_S_j);
					System.out.print(
							"\tto be delimited by edge-cut hop " + acceptedEdgeCutHops.get(deltaRU_index).getEdgeCut());
					System.out.println("\t with the deltaRU=" + deltaRU+"\n");
					contributiontoCRU.put(selected_S_j, CRU_j);	
					contributiontoNRU.put(selected_S_j, NRU_j);	
					contributiontoRU.put(selected_S_j, RU_j);
					updated_datastream.add(selected_S_j);
					//stream_to_update.remove(selected_S_j_index);
				}else {
					
					System.out.println("\texpected B <= Bmax ? No\n");
				}
				deltaRU=0;
				if(!sortedDeltaRU_set.isEmpty()) {
						
						deltaRU_index = sortedDeltaRU_set.remove();
						deltaRU = deltaRU_set.get(deltaRU_index);
				}
				selected_S_j=-1;
					
		}
		System.out.print("Final solution B=" + CostModel.getFog_to_cloud_overall_netw_bwd_usage());
		System.out.print("\tCRU=" + CostModel.getNorm_comp_resource_usage_cost());
		System.out.print("\tNRU=" + CostModel.getNorm_netw_resource_usage_cost());
		System.out.println("\tRU=" + CostModel.getResource_usage_cost()+"\n");
		CostModel.getFog_cpu_memory_usage().clear();
		CostModel.getFog_to_cloud_netw_bwd_usage().clear();
	}

	private void findBestMoves(int S_j, double nd_FC, double nd_EF, double nb_F_jC){
		
		Queue<Integer> deltaRU_list_index = new LinkedList<Integer>();
		int F_j = getSourcesToResourceNodes().get(S_j);
		int deepth = 1;
		double delta_RU_F_j = 0;
		double best_delta_RU_F_j = 0;
		int edgeCutIndex_p = 0;
		System.out.println("\t**Search best edge-cut move for Gmig_" + S_j);
		double best_nbu_F_jC = 0;
		double best_nbu_F_j_pC = 0;
		while (deepth < edgeCutHopParameter) {

			Graph graph_mig_j = initialMapping.get(F_j);

			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			int cm_F_j = fogDevice.getHost().getRamProvisioner().getAvailableRam();
			UtilFunctions.setAvailableMemory(cm_F_j);

			int edgeCutIndex = replicationAndMigrationPoints.get(S_j);
			EdgeCut edgeCut = allEdgeCuts.get(S_j).get(edgeCutIndex);
			edgeCutIndex_p = edgeCutIndex - deepth;
			EdgeCut edgeCut_p = allEdgeCuts.get(S_j).get(edgeCutIndex_p);
			System.out.println("\n\t" + deepth + "-hop edge-cut away: " + edgeCut_p.getEdgeCut());
		
			double cmu_F_j_p = 0;
			double nbu_F_j_pC = 0;
			double cru_F_j_p = 0;
			double nru_F_j_p = 0;
			Graph graph_mig_j_p = null;

			if (UtilFunctions.decideToReplicateOnFog(graph_mig_j, edgeCut_p)){

				graph_mig_j_p = UtilFunctions.candidatGraphToReplicate(graph_mig_j, S_j, edgeCut_p.getEdgeCut());
				UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_mig_j_p, operatorsType, operatorsSelectivity,
						operatorCosts, S_j, streamsSet.get(S_j));
				cmu_F_j_p = UtilFunctions.getRequiredMemory();
				cru_F_j_p = cmu_F_j_p / cm_F_j;
				nbu_F_j_pC = allEdgeCutValues.get(S_j).get(edgeCutIndex_p);
				nru_F_j_p = (nbu_F_j_pC / nb_F_jC) * (nd_FC / nd_EF);
				deepth++;

			}else{
				
				System.out.println("\tDecide to not replicate the Gmig_" + S_j);
				cru_F_j_p = cmu_F_j_p / cm_F_j;
				nbu_F_j_pC = streamsSet.get(S_j);
				nru_F_j_p = (nbu_F_j_pC / nb_F_jC) * (nd_FC / nd_EF);
				deepth = edgeCutHopParameter;
	
			}
			
			System.out.print("\tcmu=" + cmu_F_j_p);
			System.out.println("\tnbu=" + nbu_F_j_pC);
			double CRU_j = cru_F_j_p / CostModel.getSource_number();
			double NRU_j = nru_F_j_p;
			double RU_j = CRU_j+ NRU_j;
			System.out.print("\tcontribution to CRU =" + CRU_j);
			System.out.print("\tcontribution to NRU =" + NRU_j);
			System.out.println("\tcontribution to RU =" + RU_j);
			int deltaRU_index = deltaRU_set.size();
			delta_RU_F_j = RU_j - contributiontoRU.get(S_j);
			System.out.println("\tdeltaRU="+delta_RU_F_j);
			deltaRU_list_index.add(deltaRU_index);
			deltaRU_set.put(deltaRU_index, delta_RU_F_j);
			edgeCutIndex_per_deltaRU.put(deltaRU_index, edgeCutIndex_p);
			nbu_per_deltaRU.put(deltaRU_index, nbu_F_j_pC);
			cmu_per_deltaRU.put(deltaRU_index, cmu_F_j_p);
			acceptedEdgeCutHops.put(deltaRU_index, edgeCut_p);
			if(graph_mig_j_p != null){
				
				graph_mig_per_deltaRU.put(deltaRU_index, graph_mig_j_p);
			}
			
		}
		datastream_deltaRU_index.put(S_j, deltaRU_list_index);
		stream_to_update.add(S_j);
		// B = B - best_nbu_F_jC + best_nbu_F_j_pC;
		System.out.println("");
	}

	private Queue<Integer> queue(Map<Integer, Double> streamsSet) {

		Queue<Integer> queue = new LinkedList<Integer>();
		for (int F_j : streamsSet.keySet()) {

			queue.add(F_j);
		}

		return queue;

	}

	public double getW_c() {
		return w_c;
	}

	public void setW_c(double w_c) {
		this.w_c = w_c;
	}

	public double getW_n() {
		return w_n;
	}

	public void setW_n(double w_n) {
		this.w_n = w_n;
	}

	public Map<Integer, Integer> getSourcesToResourceNodes() {
		return sourcesToResourceNodes;
	}

	public void setSourcesToResourceNodes(Map<Integer, Integer> sourcesToResourceNodes) {
		this.sourcesToResourceNodes = sourcesToResourceNodes;
	}

	public Map<Integer, Double> getStreamsSet() {
		return streamsSet;
	}

	public void setStreamsSet(Map<Integer, Double> streamsSet) {

		this.streamsSet = streamsSet;
	}

	public Map<Integer, Double> getCumulatedSelectivitiesForMigratedStreams() {
		return cumulatedSelectivitiesForMigratedStreams;
	}

	public void setCumulatedSelectivitiesForMigratedStreams(
			Map<Integer, Double> cumulatedSelectivitiesForMigratedSreams) {
		this.cumulatedSelectivitiesForMigratedStreams = cumulatedSelectivitiesForMigratedSreams;
	}

	public Map<Integer, Graph> getInitialMapping() {
		return initialMapping;
	}

	public void setInitialMapping(Map<Integer, Graph> initialMapping) {
		this.initialMapping = initialMapping;
	}

	public Map<Integer, Map<Integer, Double>> getAllEdgeCutValues() {
		return allEdgeCutValues;
	}

	public void setAllEdgeCutValues(Map<Integer, Map<Integer, Double>> allEdgeCutValues) {
		this.allEdgeCutValues = allEdgeCutValues;
	}

	public int getEdgeCutHopParameter() {
		return edgeCutHopParameter;
	}

	public void setEdgeCutHopParameter(int edgeCutHopParameter) {
		this.edgeCutHopParameter = edgeCutHopParameter;
	}

	public boolean isBetterSolution() {
		return betterSolution;
	}

	public void setBetterSolution(boolean betterSolution) {
		this.betterSolution = betterSolution;
	}

	public Map<Integer, Graph> getEffectiveMapping() {
		return effectiveMapping;
	}

	public void setEffectiveMapping(Map<Integer, Graph> effectiveMapping) {
		this.effectiveMapping = effectiveMapping;
	}

	
}
