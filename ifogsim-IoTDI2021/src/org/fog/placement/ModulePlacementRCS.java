package org.fog.placement;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppEdge;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.entities.Actuator;
import org.fog.entities.FogDevice;
import org.fog.entities.Sensor;
import org.fog.placement.proposition.algorithms.RCS;
import org.fog.placement.proposition.configuration.ApplicationState;
import org.fog.placement.proposition.configuration.Configuration;
import org.fog.placement.proposition.monitoring.CostModel;
import org.fog.placement.proposition.utils.ALGORITHM;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.placement.proposition.utils.Triggers;
import org.fog.utils.Config;
import org.fog.utils.JsonToApplication;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ModulePlacementRCS extends ModulePlacement {

	private List<Sensor> sensors;
	private List<Actuator> actuators;
    private Map<String, List<String>> mappingModel;
    private Map<Integer, List<String>> removingOperatorsModel;
	protected Map<Integer, Double> currentCpuLoad;
	protected Map<String, Integer> sensorsAssociated;
	private double bandwidthUsage;
	private int cloudId;
	private Configuration reconfigure;
	private Graph graph;
	private RCS algorithmique; 
	private Map<Integer, Double> streamsSet = null;
	private Map<Integer, Graph> graphMapping = null;
	private boolean emptyGraphToRemove = false;
	
	public ModulePlacementRCS(List<FogDevice> fogDevices, List<Sensor> sensors, List<Actuator> actuators,
			Application application){
		
		this.setUsedAlgorithm(ALGORITHM.RCS);
		this.setFogDevices(fogDevices);
		this.setApplication(application);
		this.setSensors(sensors);
		this.setActuators(actuators);
		this.setModuleToDeviceMap(new HashMap<String, List<Integer>>());
		this.setDeviceToModuleMap(new HashMap<Integer, List<AppModule>>());
		this.setModuleInstanceCountMap(new HashMap<Integer, Map<String, Integer>>());
		this.setModulesScheduled(new HashMap<Integer, List<String>>());
		sensorsAssociated = new HashMap<String, Integer>();
		currentCpuLoad = new HashMap<Integer, Double>();
		this.cloudId = CloudSim.getEntityId("cloud");
		
		reconfigure = new Configuration(getFogDevices(), getSensors(), getActuators());
		reconfigure.setApplication(getApplication());
		ApplicationState.setConfig(reconfigure);
		
		mapModules();
		
	}
	
	@Override
	protected void mapModules(){
		// TODO Auto-generated method stub
		
		if(Config.USE_APPLICATION_STATE) {
			
			ApplicationState.loadStaticGraphState();
			ApplicationState.loadEvolvingGraphState();
			graph = reconfigure.getStaticGraph();
			algorithmique = new RCS(graph, reconfigure.getOperatorsType(), reconfigure.getOperatorsSelectivity(), reconfigure.getOperatorCosts(),reconfigure.getOperatorsReplicability());
			algorithmique.setCumulatedSelectivitiesForMigratedSreams(reconfigure.getCumulatedSelectivitiesForMigratedSreams());
			
			
			for (int F_j :reconfigure.getEffectiveMapping().keySet()){

				System.out.println("getEffectiveMapping("+CloudSim.getEntityName(F_j)+" )="+reconfigure.getEffectiveMapping().get(F_j).getAdjVertices());
			}
			
			algorithmique.setEffectiveMapping(reconfigure.getEffectiveMapping());
			dynamicMapModules(Triggers.REUSE_DEPLOYMENT, 1.0, 1.0, 0, 0, null); 
			Config.REUSE_DEPLOYMENT = true;
				
			
		}else{
			
			reconfigure.convertApplicationToGraph(getApplication());
			graph = reconfigure.getInitialGraph();
			algorithmique = new RCS(graph, reconfigure.getOperatorsType(), reconfigure.getOperatorsSelectivity(), reconfigure.getOperatorCosts(),reconfigure.getOperatorsReplicability());
			dynamicMapModules(Triggers.CLOUD_ONLY, 1,1,0, 0, null); 
		}
		
	}

	
	protected void dynamicMapModules(Triggers triggers, double nd_FC, double nd_EF,double fog_cloud_netw_bwd_usage, double fog_cloud_upper_threshold_netw_bwd, Map<Integer, Double> streamsSet) {
		// TODO Auto-generated method stub
		
		System.out.println("Trigger "+triggers.toString());
		algorithmique.setNd_EF(nd_EF);
		algorithmique.setNd_FC(nd_FC);
		JSONArray mappings = null;
		if(StaticVariables.jsonObject == null){
			 
			 StaticVariables.jsonObject = new JSONObject();
		 }
		
		Map<Integer, Double> streamsSet_ = null;
		switch (triggers){
		
		case MIGRATE_TOFOG:
	        
			setStartTime();
			streamsSet_ = mapDataStreamSourceToGraph(streamsSet);
			algorithmique.setSourcesToResourceNodes(reconfigure.getSourcesToResourceNodes());
			graphMapping = algorithmique.replicateAndMigratePartOfApplicationOnFog(fog_cloud_netw_bwd_usage, fog_cloud_upper_threshold_netw_bwd, streamsSet_);
			
			JSONArray configurations = new JSONArray();
			for(int S_j : graphMapping.keySet()){
		    
		    	JSONObject configuration = new JSONObject();
		    	configuration.put(CloudSim.getEntityName(S_j), graphMapping.get(S_j).getAdjVertices().toString());
		    	configurations.add(configuration);
		    }
			StaticVariables.jsonObject.put("Reconfiguration", configurations);
			reconfigure.reconfigureApplicationTodeployOnFog(graphMapping);
			setApplication(reconfigure.getApplication());
			implementMappingModel(reconfigure.getMappingModel());
			reconfigure.getMappingModel().clear();
			reconfigure.setCumulatedSelectivitiesForMigratedSreams(algorithmique.getCumulatedSelectivitiesForMigratedSreams());
			reconfigure.setEffectiveMapping(algorithmique.getEffectiveMapping());
			setFinishTime();
			StaticVariables.reconfigurationTime += duration(); 
			
		break;
		
		case MIGRATE_BACK_INCLOUD:
			
			setStartTime();
			streamsSet_ = mapDataStreamSourceToGraph(streamsSet);
			algorithmique.setSourcesToResourceNodes(reconfigure.getSourcesToResourceNodes());
			Map<Integer, Graph> graphRemoving = algorithmique.migratePartOfApplicationBackToCloud(fog_cloud_netw_bwd_usage, fog_cloud_upper_threshold_netw_bwd, streamsSet_);
			reconfigure.setEffectiveMapping(algorithmique.getEffectiveMapping());
			setEmptyGraphToRemove(graphRemoving.isEmpty());
			if(!graphRemoving.isEmpty()){
				
				System.out.println("graphRemoving "+graphRemoving);
				reconfigure.reconfigureApplicationToReleaseTheFog(graphRemoving);
				setApplication(reconfigure.getApplication());
				implementRemovingModel(reconfigure.getRemovingModel());
				reconfigure.setCumulatedSelectivitiesForMigratedSreams(algorithmique.getCumulatedSelectivitiesForMigratedSreams());
			}
			setFinishTime();
			StaticVariables.reconfigurationTime += duration();
			
			for(int F_j : reconfigure.getEffectiveMapping().keySet()) {
				
				System.out.println(CloudSim.getEntityName(F_j)+" "+reconfigure.getEffectiveMapping().get(F_j).getAdjVertices());
			}
			
				
		break;
			
		case ADJUST_EDGECUT:
			
			setStartTime();
			algorithmique.setSourcesToResourceNodes(reconfigure.getSourcesToResourceNodes());
			graphRemoving = algorithmique.adjustEdgeCutToSatfisfyFogNodeResources(streamsSet, fog_cloud_upper_threshold_netw_bwd);
			reconfigure.setEffectiveMapping(algorithmique.getEffectiveMapping());
			setEmptyGraphToRemove(graphRemoving.isEmpty());
			if(!graphRemoving.isEmpty()){
			
				reconfigure.reconfigureApplicationToReleaseTheFog(graphRemoving);
				setApplication(reconfigure.getApplication());
				implementRemovingModel(reconfigure.getRemovingModel());
				reconfigure.setCumulatedSelectivitiesForMigratedSreams(algorithmique.getCumulatedSelectivitiesForMigratedSreams());
				StaticVariables.exeedAvailableComputationalResources.clear();
			}
			setFinishTime();
			StaticVariables.reconfigurationTime += duration();
			for(int F_j : reconfigure.getEffectiveMapping().keySet()) {
				
				System.out.println(CloudSim.getEntityName(F_j)+" "+reconfigure.getEffectiveMapping().get(F_j).getAdjVertices());
			}
			
		break;
		
		case CLOUD_ONLY:
			setStartTime();
			JSONObject doc = new JSONObject();
			mappings = new JSONArray();
			List<AppModule> modules = getApplication().getModules();
			for(AppModule module : modules){
				
				FogDevice cloud = getDeviceById(cloudId);
				createModuleInstanceOnDevice(module, cloud);
				JSONObject mapping = new JSONObject();
				mapping.put("operator_name", module.getName());
				mapping.put("resource_node", cloud.getName());
				mappings.add(mapping);
				
			}
			setFinishTime();
			StaticVariables.reconfigurationTime += duration();
			doc.put("mappings", mappings);
			JsonToApplication.generateJsonFile(doc, StaticVariables.application_topology);
			break;
			
		case REUSE_DEPLOYMENT:
			
			setStartTime();
			JSONObject appDoc = JsonToApplication.convertJsonFileToObject(StaticVariables.application_topology);
			mappings = (JSONArray) appDoc.get("mappings");
			modules = getApplication().getModules();
			for(AppModule op : modules){
				boolean exist = false;
				AppModule module = getApplication().getModuleByName(op.getName());
				FogDevice resource_node = getDeviceById(cloudId);
				for(int i = 0; i < mappings.size(); i++){
					
					JSONObject mapping = (JSONObject) mappings.get(i);
					if(mapping.get("operator_name").equals(module.getName())) {
						
						resource_node = (FogDevice) CloudSim.getEntity((String) mapping.get("resource_node"));
						exist = true;
						break;
					}
				}
				if(!exist) {
					
					String[] split = module.getName().split("_");
					if(split.length > 3){
						
						int S_j = Integer.valueOf(split[3]);
						int F_j = reconfigure.getSourcesToResourceNodes().get(S_j);
						resource_node = (FogDevice) CloudSim.getEntity(F_j);
						if(!resource_node.getName().equals("cloud")) {
							
							JSONObject mapping = new JSONObject();
							mapping.put("operator_name", module.getName());
							mapping.put("resource_node", resource_node.getName());
							mappings.add(mapping);
						}
					}
				}
				createModuleInstanceOnDevice(module, resource_node);
			}
			setFinishTime();
			StaticVariables.reconfigurationTime += duration();
			appDoc.put("mappings", mappings);
		    JsonToApplication.generateJsonFile(appDoc, StaticVariables.application_topology);
		    break;
		
		}
		
		
	}

	public void implementMappingModel(Map<Integer, List<String>> mappingModel){
		
		System.out.println("mappingModel "+mappingModel);
		
		JSONObject doc = JsonToApplication.convertJsonFileToObject(StaticVariables.application_topology);
		JSONArray mappings = new JSONArray();
		if(doc.containsKey("mappings")){
			
			mappings = (JSONArray) doc.get("mappings");
		}
		
		for (int device_id : mappingModel.keySet()) {

			for (String moduleName : mappingModel.get(device_id)) {
                
				AppModule module = getApplication().getModuleByName(moduleName);
				FogDevice device = getDeviceById(device_id);
				createModuleInstanceOnDevice(module, device);
				JSONObject mapping = new JSONObject();
				mapping.put("operator_name", module.getName());
				mapping.put("resource_node", device.getName());
				mappings.add(mapping);
				
			}
			
		}
		doc.put("mappings", mappings);
		JsonToApplication.generateJsonFile(doc, StaticVariables.application_topology);
		
	}
	
	public void implementRemovingModel(Map<Integer, List<String>> removingModel) {
		
		
		JSONObject doc = JsonToApplication.convertJsonFileToObject(StaticVariables.application_topology);
		JSONArray mappings = (JSONArray) doc.get("mappings");
		for (int device_id : removingModel.keySet()){
			
			System.out.println("Removing operators on "+CloudSim.getEntity(device_id).getName()+ ":"+ removingModel.get(device_id));
			for (String moduleName : removingModel.get(device_id)) {
				
				JSONObject removedMapping = new JSONObject();
				removedMapping.put("operator_name", moduleName);
				removedMapping.put("resource_node", CloudSim.getEntity(device_id).getName());
				if(mappings.contains(removedMapping)) {
					
					mappings.remove(removedMapping);
				}
				AppModule module = getApplication().getModuleByName(moduleName);
				FogDevice device = getDeviceById(device_id);
				removeModuleInstanceOnDevice(module, device);
				reconfigure.removeAppModule(moduleName);
				
				
			}
		}
		doc.put("mappings", mappings);
		setRemovingOperatorsModel(removingModel);
		JsonToApplication.generateJsonFile(doc, StaticVariables.application_topology);
	}
	/**
	 * Gets all sensors associated with fog-device <b>device</b>
	 * 
	 * @param device
	 * @return map from sensor type to number of such sensors
	 */
	private void setAssociatedSensors(FogDevice device) {
		Map<String, Integer> endpoints = new HashMap<String, Integer>();
		for (Sensor sensor : getSensors()){
			if (sensor.getGatewayDeviceId() == device.getId()) {
				if (!sensorsAssociated.containsKey(sensor.getTupleType()))
					sensorsAssociated.put(sensor.getTupleType(), 0);
				sensorsAssociated.put(sensor.getTupleType(), sensorsAssociated.get(sensor.getTupleType()) + 1);
			}
		}
	}

	protected double getRateOfSensor(String sensorType) {

		for (Sensor sensor : getSensors()) {
			if (sensor.getTupleType().equals(sensorType))
				return 1 / sensor.getTransmitDistribution().getMeanInterTransmitTime();
		}
		return 0;
	}

	private Map<Integer, Double> mapDataStreamSourceToGraph(Map<Integer, Double> input_streamsSet){
		
		Map<Integer, Double> streamsSet = new ConcurrentHashMap<Integer, Double>();
		for(Sensor sensor: getSensors()) {
			
			if(input_streamsSet.containsKey(sensor.getGatewayDeviceId())) {
			
				int S_j = reconfigure.getApplicationToGraph().get(sensor.getName());
				double data_stream = input_streamsSet.get(sensor.getGatewayDeviceId());
				streamsSet.put(S_j, data_stream);
			}
			
		}
		return streamsSet;
	}

	public List<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(List<Sensor> sensors) {
		this.sensors = sensors;
	}

	public List<Actuator> getActuators() {
		return actuators;
	}

	public void setActuators(List<Actuator> actuators) {
		this.actuators = actuators;
	}

	public double getBandwidthUsage() {
		return bandwidthUsage;
	}

	public void setBandwidthUsage(double bandwidthUsage) {
		this.bandwidthUsage = bandwidthUsage;
	}

	

	public Map<Integer, List<String>> getRemovingOperatorsModel() {
		return removingOperatorsModel;
	}

	public void setRemovingOperatorsModel(Map<Integer, List<String>> removingOperatorsModel) {
		this.removingOperatorsModel = removingOperatorsModel;
	}

	public boolean isEmptyGraphToRemove() {
		return emptyGraphToRemove;
	}

	public void setEmptyGraphToRemove(boolean emptyGraphToRemove) {
		this.emptyGraphToRemove = emptyGraphToRemove;
	}

	
}
