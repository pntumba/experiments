import matplotlib.pyplot as plt
import numpy as np

operators = ['Source S_j','O_1','O_2','O_3','O_4','O_5','O_6','O_7', 'O_8']
CONTRIBUTION_CRU = [0,0.09399414063,0.1760253906,0.2382324219,0.2784277344,0.3128808594,0.3312558594,0.3390652344,0.3440264844]
CONTRIBUTION_NRU = [0.4827586207,0.3862068966,0.2703448276,0.1622068966,0.1297655172,0.06488275862,0.02595310345,0.01557186207,0.01090030345]
CONTRIBUTION_RU = [0.4827586207,0.4802010372,0.4463702182,0.4004393184,0.4081932516,0.377763618,0.3572089628,0.3546370964,0.3549267878]

fig, ax = plt.subplots()
ax.plot(operators, CONTRIBUTION_CRU, "-*b", label="CRU_j", markersize=8)
ax.plot(operators, CONTRIBUTION_NRU, "-+r", label="NRU_j",markersize=8)
ax.plot(operators, CONTRIBUTION_RU, "-y", label="RU_j",markersize=8)
ax.legend()
plt.ylabel('Resource usage costs')
plt.xlabel('Gim_j includes operators up to this one')
plt.show()


