import matplotlib.pyplot as plt
import numpy as np

operators = [256,512,700,965, 1221, 1500, 1792, 2000, 2256, 2400]
B_RCS = [255.210101,511.1441077,697.63367,929.3750842,1085.89899,1071.363636,1150.486195,1232.43165,1148.847138,1299.509091]
B_SOO = [4.866666667,8.666666667,20.93333333,47.93333333,217.7333333,375.8,518.0666667,714.8666667,892.4,1028]

fig, ax = plt.subplots()
ax.plot(operators, B_RCS, "-*b", label="RCS", markersize=8)
ax.plot(operators, B_SOO, "-+r", label="SOO",markersize=8)
ax.legend()
plt.ylabel('Fog to Cloud bandwidth usage (KB/sec)')
plt.xlabel('Sum-rates of data streams produced at the Edge (KB/sec)')
plt.show()


