import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [512,700,965,1221,1500,1792,2000,2100,2256]
RU_SOO = [0.3531034483,0.4827586207,0.6655172414,0.8420689655,1.050173806,1.295941855,1.492491604,1.590512213,1.745402613]
RU_SOOCPLEX = [0.3531034483,0.4827586207,0.6655172414,0.8420689655,1.049473375,1.292932202,1.490741379,1.588485722,1.745402613]
RU_RCS = [0.3531034483,0.4827586207,0.6720762392,0.901867412,1.119512617,1.303216191,1.504339754,1.603064027,1.745537311]

fig, ax = plt.subplots()
ax.plot(data_stream_rates, RU_SOO, "-sr", label="SOO",markersize=8, markevery=2)
ax.plot(data_stream_rates, RU_SOOCPLEX, "--^b", label="SOO-CPLEX", markersize=8, markevery=1)
ax.plot(data_stream_rates, RU_RCS, "-ok", label="RCS", markersize=8)
ax.legend()
plt.ylabel('Resource usage costs- RU')
plt.xlabel('Data stream rate produced at the Edge (KB/sec)')
plt.show()
