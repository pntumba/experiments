import matplotlib.pyplot as plt
import numpy as np

operators = ['Source S_j','O_1','O_2','O_3','O_4','O_5','O_6','O_7', 'O_8']
CONTRIBUTION_CRU = [0,0.4846875,0.9076875,1.2284625,1.4357325,1.6133925,1.7081445,1.7484141,1.77399714]
CONTRIBUTION_NRU = [1.555862069,1.244689655,0.8712827586,0.5227696552,0.4182157241,0.2091078621,0.08364314483,0.0501858869,0.03513012083]
CONTRIBUTION_RU = [1.555862069,1.729377155,1.778970259,1.751232155,1.853948224,1.822500362,1.791787645,1.798599987,1.809127261]

fig, ax = plt.subplots()
ax.plot(operators, CONTRIBUTION_CRU, "-*b", label="CRU_j", markersize=8)
ax.plot(operators, CONTRIBUTION_NRU, "-+r", label="NRU_j",markersize=8)
ax.plot(operators, CONTRIBUTION_RU, "-y", label="RU_j",markersize=8)
ax.legend()
plt.ylabel('Resource usage costs')
plt.xlabel('Gim_j includes operators up to this one')
plt.show()


