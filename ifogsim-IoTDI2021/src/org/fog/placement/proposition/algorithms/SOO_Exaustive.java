package org.fog.placement.proposition.algorithms;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.entities.FogDevice;
import org.fog.placement.proposition.monitoring.CostModel;
import org.fog.placement.proposition.utils.EdgeCut;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.MinMax;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.placement.proposition.utils.UtilFunctions;

public class SOO_Exaustive {

	private Graph graph;
	private Graph graph_rep;
	private double B;
	private double Buthr;
	private Map<Integer, Double> streamsSet;
	private Map<Integer, Double> cumulatedSelectivitiesForMigratedStreams;
	private Map<Integer, Graph> initialMapping = new HashMap<Integer, Graph>();
	private Map<Integer, Integer> sourcesToResourceNodes;
	private Map<Integer, ModuleType> operatorsType;
	private Map<Integer, Double> operatorsSelectivity;
	private Map<Integer, Boolean> operatorsReplicability;
	private Map<Integer, Integer> replicationAndMigrationPoints = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Map<Integer, EdgeCut>> allEdgeCuts = null;
	private Map<Integer, Map<Integer, Double>> allEdgeCutValues = null;
	private Map<Integer, Double> operatorCosts = new ConcurrentHashMap<Integer, Double>();
	private double RU = 0.0;
	private double w_c;
	private double w_n;
	private double cru = 0.0;
	private double nru = 0.0;
	private double CRU = 0.0;
	private double NRU = 0.0;
	private MinMax compMinMax = UtilFunctions.compMinMax;
	private MinMax netwMinMax = UtilFunctions.netwMinMax;

	public SOO_Exaustive(Graph graph, Map<Integer, ModuleType> operatorsType, Map<Integer, Double> operatorsSelectivity,
			Map<Integer, Double> operatorCosts, Map<Integer, Boolean> operatorsReplicability) {

		this.graph = graph;
		this.operatorsReplicability = operatorsReplicability;
		this.operatorsSelectivity = operatorsSelectivity;
		this.operatorsType = operatorsType;
		this.cumulatedSelectivitiesForMigratedStreams = new ConcurrentHashMap<Integer, Double>();
		this.operatorCosts = operatorCosts;
		compMinMax = UtilFunctions.compMinMax;
		netwMinMax = UtilFunctions.netwMinMax;

	}

	public void initialConfiguration(double nd_FC, double nd_EF, double nb_F_jC, Map<Integer, Double> streamsSet) {

		allEdgeCuts = new ConcurrentHashMap<Integer, Map<Integer, EdgeCut>>();
		allEdgeCutValues = new ConcurrentHashMap<Integer, Map<Integer, Double>>();
		this.Buthr = nb_F_jC;
		this.streamsSet = streamsSet;
		this.graph_rep = UtilFunctions.replicableGraph(graph, operatorsReplicability);

		Queue<Integer> streamQueue = queue(streamsSet);

		while (!streamQueue.isEmpty()) {

			int S_j = streamQueue.remove();
			int F_j = getSourcesToResourceNodes().get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			int memory_j = fogDevice.getHost().getRamProvisioner().getAvailableRam();
			UtilFunctions.setAvailableMemory(memory_j);
			int sourceVertex = S_j;
			int sinkVertex = graph_rep.getSink();
			Graph graph_candidat = UtilFunctions.candidatSubGraph(graph_rep, sourceVertex, sinkVertex,
					operatorsReplicability);
			double stream = streamsSet.get(S_j);
			UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_candidat, operatorsType, operatorsSelectivity,operatorCosts,
					sourceVertex, stream);
			Graph graph_sat_j = UtilFunctions.getGraphSatRequirement();
			if (!graph_sat_j.getAdjVertices().isEmpty()) {

				UtilFunctions.findAllEdgeCuts(graph_sat_j, operatorsSelectivity);
				EdgeCut edgeCut = new EdgeCut(UtilFunctions.getMinCut());
				Graph graph_to_replicate = UtilFunctions.candidatGraphToReplicate(graph_sat_j, sourceVertex,
						edgeCut.getEdgeCut());

				UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_to_replicate, operatorsType,
						operatorsSelectivity,operatorCosts, sourceVertex, stream);
				Graph graph_mig_j = UtilFunctions.getGraphSatRequirement();

				getCumulatedSelectivitiesForMigratedStreams().put(S_j, UtilFunctions.getCumulatedSelectivity());
				initialMapping.put(F_j, graph_mig_j);
				replicationAndMigrationPoints.put(S_j, UtilFunctions.getMinCutCountIndex());

				Map<Integer, EdgeCut> edgeCuts = new HashMap<Integer, EdgeCut>(UtilFunctions.getAllEdgeCuts());
				allEdgeCuts.put(S_j, edgeCuts);

				Map<Integer, Double> edgeCutValues = new HashMap<Integer, Double>(UtilFunctions.getAllEdgeCutValues());
				getAllEdgeCutValues().put(S_j, edgeCutValues);

				double cmu_F_j = UtilFunctions.getRequiredMemory();
				double cm_F_j = UtilFunctions.getAvailableMemory();
				CostModel.getFog_cpu_memory_usage().put(F_j,cmu_F_j);
				CostModel.getFog_max_cpu_memory().put(F_j, cm_F_j);

				double nbu_F_jC = UtilFunctions.getMinCutValue();
				CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
				CostModel.getFog_to_cloud_netw_delay().put(F_j, nd_FC);
				CostModel.getEdge_to_fog_netw_delay().put(F_j, nd_EF);

			}

		}
		CostModel.calculateComp_resource_usage_cost();
		CostModel.calculateNetw_resource_usage_cost();
		CostModel.calculateResource_usage_cost();
		B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();

		System.err.println(
				"Initial CostModel.getFog_comp_resource_usage_cost() " + CostModel.getFog_comp_resource_usage_cost());
		System.err.println("Initial CostModel.getFog_to_cloud_netw_resource_usage_cost() "
				+ CostModel.getFog_to_cloud_netw_resource_usage_cost());
		System.out.println("Initial B=" + B);
		System.err.println("Initial CostModel.getResource_usage_cost() " + CostModel.getResource_usage_cost());
	}

	public void adjustConfiguration(double nd_FC, double nd_EF, double nb_F_jC, Map<Integer, Double> streamsSet) {

		Map<Integer, Graph> adjustedMapping = new HashMap<Integer, Graph>();
		Map<Integer, Double> adjustedNetw_resource_usage = new ConcurrentHashMap<Integer, Double>();
		Map<Integer, Double> adjustedComp_resource_usage = new ConcurrentHashMap<Integer, Double>();

		double delta_RU = Double.MAX_VALUE;
		Queue<Integer> streamQueue = queue(streamsSet);
		Map<Integer, Double> allDeltaRU = new ConcurrentHashMap<Integer, Double>();
		Map<Integer, Integer> RM = new ConcurrentHashMap<Integer, Integer>();

		if (replicationAndMigrationPoints.isEmpty()) {
			return;
		}

		int nbrStream = replicationAndMigrationPoints.size();
		int[] arrayIndexStream = new int[nbrStream];
		int[] RMArray = new int[nbrStream];
		int index = 0;
		int maxEdgeCutNbr = 0;
		for (int S_j : replicationAndMigrationPoints.keySet()) {

			arrayIndexStream[index] = S_j;
			index++;
			if (maxEdgeCutNbr < allEdgeCuts.get(S_j).size()) {

				maxEdgeCutNbr = allEdgeCuts.get(S_j).size();
			}
		}

		for (int S_j_index = 0; S_j_index < nbrStream; S_j_index++) {

			int S_j = arrayIndexStream[S_j_index];
			RMArray[S_j_index] = replicationAndMigrationPoints.get(S_j);
		}

		Map<Integer, int[]> allPossibleMR = possibleMigrationAndReplicationPoints(nbrStream, arrayIndexStream, RMArray);
		int[] improvedMR = null;
		for (int key : allPossibleMR.keySet()) {

			Map<Integer, Double> netw_resource_usage_cost_p = new ConcurrentHashMap<Integer, Double>();
			Map<Integer, Double> comp_resource_usage_cost_p = new ConcurrentHashMap<Integer, Double>();

			Map<Integer, Double> possibleAdjustedNetw_resource_usage = new ConcurrentHashMap<Integer, Double>();
			Map<Integer, Double> possibleAdjustedComp_resource_usage = new ConcurrentHashMap<Integer, Double>();

			Map<Integer, Graph> possibleBestAdjustedMapping = new HashMap<Integer, Graph>();

			double newB = 0;
			int[] possibleMR = allPossibleMR.get(key);
			boolean next = false;
			for (int S_j_index = 0; S_j_index < nbrStream; S_j_index++) {

				int S_j = arrayIndexStream[S_j_index];
				int F_j = getSourcesToResourceNodes().get(S_j);

				FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
				int cm_F_j = fogDevice.getHost().getRamProvisioner().getAvailableRam();
				UtilFunctions.setAvailableMemory(cm_F_j);
				Graph graph_mig_j = initialMapping.get(F_j);
				int edgeCutIndex = possibleMR[S_j_index];
				EdgeCut edgeCut_j = allEdgeCuts.get(S_j).get(edgeCutIndex);
				boolean isSource = false;
				for (int vertex : edgeCut_j.getEdgeCut().keySet()) {

					if (graph_mig_j.isSource(vertex)) {

						isSource = true;

						break;
					}
				}

				double cmu_F_j_p = 0;
				double nbu_F_j_pC = 0;
				double cru_F_j_p = 0;
				double nru_F_j_p = 0;
				Graph graph_mig_j_p = null;

				if (isSource) {

					cru_F_j_p = cmu_F_j_p / cm_F_j;
					nbu_F_j_pC = streamsSet.get(S_j);
					nru_F_j_p = (nbu_F_j_pC / nb_F_jC) * (nd_FC / nd_EF);

				} else {

					graph_mig_j_p = UtilFunctions.candidatGraphToReplicate(graph_mig_j, S_j, edgeCut_j.getEdgeCut());
					UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_mig_j_p, operatorsType,
							operatorsSelectivity,operatorCosts, S_j, streamsSet.get(S_j));
					cmu_F_j_p = UtilFunctions.getRequiredMemory();
					cru_F_j_p = cmu_F_j_p / cm_F_j;
					nbu_F_j_pC = allEdgeCutValues.get(S_j).get(edgeCutIndex);
					nru_F_j_p = (nbu_F_j_pC / nb_F_jC) * (nd_FC / nd_EF);
					possibleBestAdjustedMapping.put(F_j, graph_mig_j_p);
				}
				netw_resource_usage_cost_p.put(S_j, nru_F_j_p);
				comp_resource_usage_cost_p.put(S_j, cru_F_j_p);
				newB = newB + nbu_F_j_pC;
				possibleAdjustedNetw_resource_usage.put(F_j, nbu_F_j_pC);
				possibleAdjustedComp_resource_usage.put(F_j, cmu_F_j_p);
			}
			
			System.out.print("\n\nChecking the configuration: ");
			for(int i=0; i<possibleMR.length; i++) {
				
				int edgeCutIndex = possibleMR[i];
				int S_j = arrayIndexStream[i];
				EdgeCut edgeCut_j = allEdgeCuts.get(S_j).get(edgeCutIndex);
				int F_j = getSourcesToResourceNodes().get(S_j);
				System.out.print(" Fog["+F_j+"] edge-cut="+edgeCut_j.getEdgeCut());
			}
			
			System.out.print("\n");
			double RU_p = newResourceUsageCost(netw_resource_usage_cost_p, comp_resource_usage_cost_p);
			double tmp_delta_RU = RU_p - CostModel.getResource_usage_cost(); 
			System.out.println("\tmp_delta_RU="+tmp_delta_RU);
			
			if (newB > Buthr){
				
				System.out.println("\tBandwidth constrain "+newB + " > " + Buthr);
				continue;
			}
			
			
			if ( tmp_delta_RU < delta_RU){

				System.out.println("\t**************************");
				System.out.println("\t*** Find new B=" + newB);
				System.out.println("\t*** Find new RU=" + RU_p);
				System.out.println("\t**************************\n");
				delta_RU = RU_p - CostModel.getResource_usage_cost();
				improvedMR = possibleMR;
				for (int F_j : possibleBestAdjustedMapping.keySet()) {

					adjustedMapping.put(F_j, possibleBestAdjustedMapping.get(F_j));
					double nbu_F_j_pC = possibleAdjustedComp_resource_usage.get(F_j);
					double cmu_F_j_p = possibleAdjustedNetw_resource_usage.get(F_j);
					adjustedNetw_resource_usage.put(F_j, nbu_F_j_pC);
					adjustedComp_resource_usage.put(F_j, cmu_F_j_p);
				}
			}
		}
		if(delta_RU < 0){

			for (int S_j : replicationAndMigrationPoints.keySet()) {

				int F_j = getSourcesToResourceNodes().get(S_j);
				Graph graph_mig_j = adjustedMapping.get(F_j);
				initialMapping.put(F_j, graph_mig_j);
				CostModel.getFog_cpu_memory_usage().put(F_j, adjustedComp_resource_usage.get(F_j));
				CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, adjustedNetw_resource_usage.get(F_j));
			}
			CostModel.calculateComp_resource_usage_cost();
			CostModel.calculateNetw_resource_usage_cost();
			CostModel.calculateResource_usage_cost();
			System.out.println("\n\tApply new configuration\n******************************\n");
		}
	}

	public Map<Integer, int[]> possibleMigrationAndReplicationPoints(int nbrStream, int[] arrayIndexStream,
			int[] RMArray) {

		Map<Integer, int[]> allMR = new ConcurrentHashMap<Integer, int[]>();
		recursivePossibleMigrationAndReplicationPoints(0, nbrStream, arrayIndexStream, RMArray, 0, allMR);
		return allMR;
	}

	public void recursivePossibleMigrationAndReplicationPoints(int S_j_index, int nbrS_j, int[] arrayIndexStream,
			int[] RMArray, int count, Map<Integer, int[]> allMR) {

		int S_j = arrayIndexStream[S_j_index];

		if (S_j_index < nbrS_j - 1) {

			int bottomS_j_index = S_j_index + 1;
			int bottomS_j = arrayIndexStream[bottomS_j_index];

			if (RMArray[bottomS_j_index] == 1) {

				int currentEdgeCutIndex = RMArray[S_j_index];
				if (currentEdgeCutIndex - 1 > 0) {

					// int upS_j_index = S_j_index - 1;
					// int upS_j = arrayIndexStream[upS_j_index];
					RMArray[S_j_index] = currentEdgeCutIndex - 1;

					RMArray[bottomS_j_index] = replicationAndMigrationPoints.get(bottomS_j) + 1;
					if (allMR.containsKey(count)) {

						allMR.get(count)[S_j_index] = RMArray[S_j_index];

					} else {

						int[] MR = new int[nbrS_j];
						MR[S_j_index] = RMArray[S_j_index];
						allMR.put(count, MR);
					}

				} else {

					int[] MR = new int[nbrS_j];
					MR[S_j_index] = RMArray[S_j_index];
					allMR.put(count, MR);
					System.out.println(allMR.containsKey(count) + "  " + Arrays.toString(MR));

				}
			} else {

				int currentEdgeCutIndex = replicationAndMigrationPoints.get(S_j);

				if (RMArray[S_j_index] < replicationAndMigrationPoints.get(S_j)) {

					currentEdgeCutIndex = RMArray[S_j_index];
				}

				if (allMR.containsKey(count)) {

					allMR.get(count)[S_j_index] = currentEdgeCutIndex;

				} else {

					int[] MR = new int[nbrS_j];
					MR[S_j_index] = currentEdgeCutIndex;
					allMR.put(count, MR);

				}
			}
			S_j_index++;
			recursivePossibleMigrationAndReplicationPoints(S_j_index, nbrS_j, arrayIndexStream, RMArray, count, allMR);

		} else {

			int currentEdgeCutIndex = replicationAndMigrationPoints.get(S_j);
			int newEdgeCutIndex = currentEdgeCutIndex;
			allMR.get(count)[S_j_index] = currentEdgeCutIndex;
			for (int index = 1; index < currentEdgeCutIndex; index++) {

				int[] MR = new int[nbrS_j];
				for (int i = 0; i < nbrS_j; i++) {

					MR[i] = allMR.get(count)[i];
				}
				newEdgeCutIndex = currentEdgeCutIndex - index;
				MR[S_j_index] = newEdgeCutIndex;
				count++;
				allMR.put(count, MR);
			}
			count++;
			RMArray[S_j_index] = newEdgeCutIndex;
			boolean terminate = true;
			for (int rm : RMArray) {

				if (rm > 1) {

					terminate = false;
					break;
				}
			}
			if (!terminate) {

				recursivePossibleMigrationAndReplicationPoints(0, nbrS_j, arrayIndexStream, RMArray, count, allMR);
			}

		}
	}

	public double newResourceUsageCost(int F_j, double cru_F_j_p, double nru_F_j_p) {

		Map<Integer, Double> netw_resource_usage_cost_p = new ConcurrentHashMap<Integer, Double>();
		Map<Integer, Double> comp_resource_usage_cost_p = new ConcurrentHashMap<Integer, Double>();

		Map<Integer, Double> netw_resource_usage_cost = new HashMap<Integer, Double>();
		Map<Integer, Double> comp_resource_usage_cost = new HashMap<Integer, Double>();
		for (int F_k : CostModel.getFog_to_cloud_netw_resource_usage_cost().keySet()) {

			netw_resource_usage_cost.put(F_k, CostModel.getFog_to_cloud_netw_resource_usage_cost().get(F_k));
			comp_resource_usage_cost.put(F_k, CostModel.getFog_comp_resource_usage_cost().get(F_k));
		}

		for (int F_k : comp_resource_usage_cost.keySet()) {

			double cru_F_k = comp_resource_usage_cost.get(F_k);
			double nru_F_k = netw_resource_usage_cost.get(F_k);

			if (F_k == F_j) {

				comp_resource_usage_cost_p.put(F_k, cru_F_j_p);
				netw_resource_usage_cost_p.put(F_k, nru_F_j_p);

			} else {

				comp_resource_usage_cost_p.put(F_k, cru_F_k);
				netw_resource_usage_cost_p.put(F_k, nru_F_k);
			}
		}

		double cru_p = 0;
		double nru_p = 0;
		double CRU_p = 0;
		double NRU_p = 0;
		for (int F_k : comp_resource_usage_cost_p.keySet()) {

			cru_p += comp_resource_usage_cost_p.get(F_k);
			nru_p += netw_resource_usage_cost_p.get(F_k);
		}
		CRU_p = (cru_p - compMinMax.getMin()) / (compMinMax.getMax() - compMinMax.getMin());
		NRU_p = (nru_p - netwMinMax.getMin()) / (netwMinMax.getMax() - netwMinMax.getMin());

		System.out.print("\tcurrent CRU=" + CostModel.getNorm_comp_resource_usage_cost());
		System.out.print(" new CRU=" + CRU_p);
		System.out.print("| current NRU=" + CostModel.getNorm_netw_resource_usage_cost());
		System.out.println(" new NRU=" + NRU_p);
		double RU = CostModel.getWeight_for_comp() * CRU_p + CostModel.getWeight_for_netw() * NRU_p;
		return RU;
	}

	public double newResourceUsageCost(Map<Integer, Double> netw_resource_usage_cost_p,
			Map<Integer, Double> comp_resource_usage_cost_p) {

		double cru_p = 0;
		double nru_p = 0;
		double CRU_p = 0;
		double NRU_p = 0;
		for (int F_k : comp_resource_usage_cost_p.keySet()) {

			cru_p += comp_resource_usage_cost_p.get(F_k);
			nru_p += netw_resource_usage_cost_p.get(F_k);
		}
		CRU_p = (cru_p - compMinMax.getMin()) / (compMinMax.getMax() - compMinMax.getMin());
		NRU_p = (nru_p - netwMinMax.getMin()) / (netwMinMax.getMax() - netwMinMax.getMin());

		System.out.print("current CRU=" + CostModel.getNorm_comp_resource_usage_cost());
		System.out.print(" new CRU=" + CRU_p);
		System.out.print("\tcurrent NRU=" + CostModel.getNorm_netw_resource_usage_cost());
		System.out.println(" new NRU=" + NRU_p);
		double RU_p = CostModel.getWeight_for_comp() * CRU_p + CostModel.getWeight_for_netw() * NRU_p;
		System.out.println("Current RU=" + CostModel.getResource_usage_cost() + " New RU=" + RU_p);
		return RU_p;
	}

	private Queue<Integer> queue(Map<Integer, Double> streamsSet) {

		Queue<Integer> queue = new LinkedList<Integer>();
		for (int F_j : streamsSet.keySet()) {

			queue.add(F_j);
		}

		return queue;

	}

	public double getW_c() {
		return w_c;
	}

	public void setW_c(double w_c) {
		this.w_c = w_c;
	}

	public double getW_n() {
		return w_n;
	}

	public void setW_n(double w_n) {
		this.w_n = w_n;
	}

	public Map<Integer, Integer> getSourcesToResourceNodes() {
		return sourcesToResourceNodes;
	}

	public void setSourcesToResourceNodes(Map<Integer, Integer> sourcesToResourceNodes) {
		this.sourcesToResourceNodes = sourcesToResourceNodes;
	}

	public Map<Integer, Double> getStreamsSet() {
		return streamsSet;
	}

	public void setStreamsSet(Map<Integer, Double> streamsSet) {

		this.streamsSet = streamsSet;
	}

	public Map<Integer, Double> getCumulatedSelectivitiesForMigratedStreams() {
		return cumulatedSelectivitiesForMigratedStreams;
	}

	public void setCumulatedSelectivitiesForMigratedStreams(
			Map<Integer, Double> cumulatedSelectivitiesForMigratedSreams) {
		this.cumulatedSelectivitiesForMigratedStreams = cumulatedSelectivitiesForMigratedSreams;
	}

	public Map<Integer, Graph> getInitialMapping() {
		return initialMapping;
	}

	public void setInitialMapping(Map<Integer, Graph> initialMapping) {
		this.initialMapping = initialMapping;
	}

	public Map<Integer, Map<Integer, Double>> getAllEdgeCutValues() {
		return allEdgeCutValues;
	}

	public void setAllEdgeCutValues(Map<Integer, Map<Integer, Double>> allEdgeCutValues) {
		this.allEdgeCutValues = allEdgeCutValues;
	}
}
