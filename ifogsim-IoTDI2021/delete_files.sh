#!/bin/bash

rm -rf src/org/fog/placement/proposition/experiments/input/graph_SOO*
rm -rf src/org/fog/placement/proposition/experiments/output/*

rm -rf src/org/fog/placement/proposition/experiments/input/graph_RCS*
mkdir src/org/fog/placement/proposition/experiments/output
