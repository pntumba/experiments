package org.fog.utils;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.lang.model.type.UnionType;
import javax.xml.crypto.Data;

import org.fog.application.AppEdge;
import org.fog.application.AppLoop;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.application.selectivity.FractionalSelectivity;
import org.fog.entities.Tuple;
import org.fog.placement.proposition.experiments.simulation.DataPoints;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.placement.proposition.utils.StaticVariables;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JsonToApplication{
	
	private static Random random_operator_cost = null;	
	private static Random random_selectvity = null;	
	private static int number_of_sources;
	private static int number_of_operators;
	public static JSONObject getApplicationJson(int operator_number, int source_number, double min_data_rate, double total_data_rate, double min_selectivity, double max_selectivity,double min_operator_cost, double max_operator_cost, int exp_nbr) {
	
		setNumber_of_operators(operator_number);
		Map<String, Double> datastreams = distributeTotalDataStreamRates(source_number, exp_nbr,total_data_rate);
		setNumber_of_sources(source_number);
		if(Config.USE_APPLICATION_STATE){
			
		  if(new File(StaticVariables.application_topology).exists()){
			  
			  System.out.println("Use existing state of an application topology");	
			  Config.APPLICATION_STATE_FILE_EXISTS = true;
			  JSONObject appDoc = convertJsonFileToObject(StaticVariables.application_topology);
			  JSONObject applicationStatObject = (JSONObject) appDoc.get("application_state");
			  return updateDatarate(applicationStatObject, datastreams);
		  }	
		  
		}
		System.out.println("Generate new application topology");	
		
		random_operator_cost = new Random();	
		random_selectvity = new Random();
		int seed = operator_number;
		random_operator_cost.setSeed(seed);
		random_selectvity.setSeed(seed);
		JSONObject doc = new JSONObject();
		JSONArray operators = new JSONArray(); 
		double cmu = getCmu(min_data_rate, total_data_rate);
		double cumulated_selectivity = 1.0;
		System.out.println("operator_number="+operator_number);
		
		for(int count = 0; count < operator_number; count++ ){
			
			String operator_name = "op_"+(count+1);
			JSONObject operator = new JSONObject();
		    String type = getRandomType();
		    Boolean replicability = true;
		    double selectivity = 1.0;
		    double operator_cost = 1.0;
		    if(Config.STATIC_OPERATOR_PARAMETER){
		    	
		    	 selectivity = getSelecitity(operator_name);
				 operator_cost = getOperatorCost(operator_name);
		    	
		    }else {
		    	
		    	 selectivity = getSelecitity(min_selectivity,max_selectivity);
				 operator_cost = getOperatorCost(min_operator_cost, max_operator_cost);
		    	
		    }
		   
			if(count == 0){
				
				type = "UNION";
				selectivity = 1.0;
				operator_cost = 1.0;
				operator_name = "union";
			}
			else if(count == operator_number-1) {
				
				operator_name = "Sink";
				selectivity = 1.0;
				operator_cost = 1.0;
				type = "SINGLE_INPUT_EDGE";
				replicability = false;
			}
			operator.put("name", operator_name);
			operator.put("cmu", cmu * cumulated_selectivity);
			operator.put("type", type);
			operator.put("replicability", replicability);
			operator.put("selectivity", selectivity);
			operator.put("operator_cost", operator_cost);
			operators.add(operator);
			
			cumulated_selectivity = cumulated_selectivity * selectivity;
			
		}
		
		doc.put("operators", operators);
		
		
		JSONArray source_edges = new JSONArray(); 
		JSONArray tuples = new JSONArray(); 
		for(String antenna: datastreams.keySet()){
			
			JSONObject source_edge = new JSONObject();
			JSONObject tuple = new JSONObject();
			source_edge.put("source",antenna);
			source_edge.put("destination","union");
			double source_data_rate = datastreams.get(antenna);
			source_edge.put("data_rate",source_data_rate);
			source_edges.add(source_edge);
			
			tuple.put("operator_name", "union");
			tuple.put("input", antenna);
			tuple.put("output", "union");
			tuples.add(tuple);
			
			
		}
		doc.put("source_edges", source_edges);
		
		JSONArray union_edges = new JSONArray();
		JSONObject union_edge = new JSONObject();
		JSONObject operator = (JSONObject) operators.get(1);
		union_edge.put("source","union");
		union_edge.put("destination",(String)operator.get("name"));
		union_edge.put("data_rate",cmu);
		union_edges.add(union_edge);
		doc.put("union_edges", union_edges);
		JSONArray edges = new JSONArray();
		for(int count=1; count < operator_number; count++){
			
			JSONObject operator1 = (JSONObject) operators.get(count-1);
			JSONObject operator2 = (JSONObject) operators.get(count);
			if(count >= 2) {
				
				JSONObject edge = new JSONObject();
				edge.put("source",(String)operator1.get("name"));
				edge.put("destination",(String)operator2.get("name"));
				edge.put("data_rate",(double)operator2.get("cmu"));
				edges.add(edge);
			}
			// Set tupple mapping
			JSONObject tuple = new JSONObject();
			tuple.put("operator_name", (String)operator2.get("name"));
			tuple.put("input", (String)operator1.get("name"));
			tuple.put("output", (String)operator2.get("name"));
			tuples.add(tuple);
		}
		doc.put("edges", edges);
		doc.put("tuples", tuples);
		
		jsonToFile(doc);
		return doc;
	}
	
	public static Map<String, Double> distributeTotalDataStreamRates(int source_number, int exp_nbr, double input_total_data_rate){
		
		
		JSONObject Objexecution = new JSONObject();
		JSONArray executions = new JSONArray();
		JSONArray total_data_rates = new JSONArray();
		if(new File(StaticVariables.executionLogFile).exists()){
			
			 JSONObject executionLog = JsonToApplication.convertJsonFileToObject(StaticVariables.executionLogFile);
			 
			 if(executionLog != null){
				 
				 executions = (JSONArray) executionLog.get("executions");
				 total_data_rates = (JSONArray) executionLog.get("execution_total_data_rates");
				 executionLog.remove("data_rates");
			 }	 
		}
		
		int id = 1;
		int index = 0;
		boolean found = false;
		for(int i=0; i<total_data_rates.size(); i++){
				
				JSONObject obj = (JSONObject) total_data_rates.get(i);
				if(obj.containsKey(""+input_total_data_rate)){
					id = (int) (new BigDecimal( (long)(obj.get(""+input_total_data_rate))).longValue());
					id = id + 1;
					index = i;
					found=true;
					break;
				}		
		}
		JSONObject Objtotal_data_rate = new JSONObject();
	    int experiment_id_per_total_data_rate = exp_nbr;
	    experiment_id_per_total_data_rate = id;
		if(exp_nbr == 0){
			
			id=0;
		}
		
		Objtotal_data_rate.put(input_total_data_rate, id);
		if(found) {
			
			total_data_rates.set(index, Objtotal_data_rate);
			
		}else{
			
			total_data_rates.add(Objtotal_data_rate);
		}
		
		Objexecution.put("execution_total_data_rates", total_data_rates);
		
		DataPoints.fillDataPoints();
		JSONObject data_rate_per_sources = new JSONObject();
		Map<String, Double> datastreams = new HashMap<String, Double>();
		
		for(int intenna_id = 0; intenna_id < source_number; intenna_id++){
			
			
			String antenna = "Antenna_"+(intenna_id+1);
			double data_rate = DataPoints.getData_points().get(input_total_data_rate).get(experiment_id_per_total_data_rate).get(intenna_id);
			datastreams.put(antenna, data_rate);
			data_rate_per_sources.put(antenna, data_rate);
		}
		
		JSONObject execution = new JSONObject();
		String experiment_id = "Experiment_"+id;
		execution.put("Execution", exp_nbr);
		execution.put("Total_data_rates", input_total_data_rate);
		execution.put("Experiment_id", experiment_id);
		execution.put("Data_rate_per_source", data_rate_per_sources);
		executions.add(execution);
		Objexecution.put("executions", executions);
		
		generateJsonFile(Objexecution, StaticVariables.executionLogFile);
		StaticVariables.experiment = StaticVariables.experiment+"_"+getNumber_of_operators()+"_"+input_total_data_rate+"_"+id;
		return datastreams;
	}
	public static Application getApplication(int userId, String appId, JSONObject doc) throws Exception{
		
		 Application application = new  Application(appId, userId);
		 ArrayList<String> loop_entities = new ArrayList<String>();
		try {
			
			//JSONObject doc = (JSONObject) JSONValue.parse(new FileReader(applicationFile));
			JSONArray source_edges = (JSONArray) doc.get("source_edges");
			String source_name = null;
			@SuppressWarnings("unchecked")
			Iterator<JSONObject> iter = source_edges.iterator(); 
			while(iter.hasNext()){
				
				JSONObject source_edge = iter.next();
				String source = (String) source_edge.get("source");
				source_name = source;
				String union = (String) source_edge.get("destination");
				
				double data_rate = new BigDecimal((double)source_edge.get("data_rate")).doubleValue();
				
				application.addAppEdge(source, union, 2, data_rate, source, Tuple.UP, AppEdge.SENSOR);
			}
			loop_entities.add(source_name);
			
			JSONArray operators = (JSONArray) doc.get("operators");
    		
			iter =operators.iterator(); 
			while(iter.hasNext()){
				
				JSONObject opertor = iter.next();
				String name = (String) opertor.get("name");
				
				double cmu_tmp = new BigDecimal((double)opertor.get("cmu")).doubleValue();
				
				int cmu = (int) cmu_tmp; 
				String type = (String) opertor.get("type");
				ModuleType operatorType = null;
				switch (type){
				case "JOIN":
					operatorType = ModuleType.JOIN;
					break;
				case "UNION":
					operatorType = ModuleType.UNION;
					break;
				case "SINGLE_INPUT_EDGE":
					operatorType = ModuleType.SINGLE_INPUT_EDGE;
					break;
				case "MULTIPLE_OUTPUT_EDGES":
					operatorType = ModuleType.MULTIPLE_OUTPUT_EDGES;
					break;
				}
				Boolean replicability = (Boolean) opertor.get("replicability");
				double selectivity = (Double) opertor.get("selectivity");
				double operator_cost = (Double) opertor.get("operator_cost");
				application.addAppModule(name, cmu, operatorType, replicability, selectivity, operator_cost);
				loop_entities.add(name);
			}
			
			
			JSONArray union_edges = (JSONArray) doc.get("union_edges");
			iter = union_edges.iterator(); 
			while(iter.hasNext()){
	
				JSONObject source_edge = iter.next();
				String union = (String) source_edge.get("source");
				String destination = (String) source_edge.get("destination");
				double data_rate =  new BigDecimal((double)source_edge.get("data_rate")).doubleValue();
				application.addAppEdge(union, destination, 2, data_rate, union, Tuple.UP, AppEdge.SENSOR);
				
			}
			
			JSONArray edges = (JSONArray) doc.get("edges");
			iter = edges.iterator(); 
			while(iter.hasNext()){
	
				JSONObject edge = iter.next();
				String source = (String) edge.get("source");
				String destination = (String) edge.get("destination");
				double data_rate =  new BigDecimal((double)edge.get("data_rate")).doubleValue();
				application.addAppEdge(source, destination, 2, data_rate, source, Tuple.UP, AppEdge.SENSOR);
				
			}
			
			JSONArray tuples = (JSONArray) doc.get("tuples");
			iter = tuples.iterator(); 
			while(iter.hasNext()){
				
				JSONObject tuple = iter.next();
				String operator_name = (String) tuple.get("operator_name");
				String input = (String) tuple.get("input");
				String output = (String) tuple.get("output");
				double selectivity = application.getModuleByName(operator_name).getSelectivity();
				application.addTupleMapping(operator_name, input, output, new FractionalSelectivity(selectivity));
				
			}
			
		}catch (Exception e) {
			
			System.out.println(e.getMessage());
		}
		final AppLoop appLoop = new AppLoop(loop_entities);
		List<AppLoop> loop = new ArrayList<AppLoop>(){
			{add(appLoop);}
		};
		application.setLoops(loop);
		return application;
	}
	
	
	public static JSONObject applicationToJson(Application app){
		
		JSONObject doc = new JSONObject();
		JSONArray operators = new JSONArray(); 
		for(AppModule physOp : app.getModules()){
			
			JSONObject operator = new JSONObject();
			String operator_name = physOp.getName();
		    String type = typeToString(physOp.getType());
		    Boolean replicability = physOp.isLocallyReplicable();
		    double selectivity = physOp.getSelectivity();
		    double cmu = physOp.getRam();
		    double operator_cost = physOp.getOperatorCost();
			operator.put("name", operator_name);
			operator.put("cmu", cmu);
			operator.put("type", type);
			operator.put("replicability", replicability);
			operator.put("selectivity", selectivity);	
			operator.put("operator_cost", operator_cost);	
			operators.add(operator);
		}
		doc.put("operators", operators);
		
		JSONArray source_edges = new JSONArray(); 
		JSONArray tuples = new JSONArray(); 
		
		for(int count = 1; count <= getNumber_of_sources(); count++){
			
			JSONObject source_edge = new JSONObject();
			JSONObject tuple = new JSONObject();
			String antenna = "Antenna_"+(count);
			AppEdge appEdge = app.getEdgeMap().get(antenna);
			source_edge.put("source",appEdge.getSource());
			source_edge.put("destination",appEdge.getDestination());
			source_edge.put("data_rate",appEdge.getTupleNwLength());
			source_edges.add(source_edge);
			
			tuple.put("operator_name", appEdge.getDestination());
			tuple.put("input",appEdge.getSource());
			tuple.put("output", appEdge.getDestination());
			tuples.add(tuple);
		}
		doc.put("source_edges", source_edges);
		
		JSONArray union_edges = new JSONArray();
		
		JSONArray edges = new JSONArray();
		
		for(AppModule physOp : app.getModules()){
			
			if(physOp.getType().equals(ModuleType.UNION)){
				
				JSONObject union_edge = new JSONObject();
				AppEdge appEdgeUnion = app.getEdgeMap().get(physOp.getName());
				union_edge.put("source",appEdgeUnion.getSource());
				union_edge.put("destination",appEdgeUnion.getDestination());
				union_edge.put("data_rate",appEdgeUnion.getTupleNwLength());
				union_edges.add(union_edge);
				
				// Set tupple mapping
				JSONObject tuple = new JSONObject();
				tuple.put("operator_name", appEdgeUnion.getDestination());
				tuple.put("input", appEdgeUnion.getSource());
				tuple.put("output",appEdgeUnion.getDestination());
				tuples.add(tuple);
				
			}else if(!physOp.getName().equals("Sink")){
				
				
				JSONObject edge = new JSONObject();
				AppEdge appEdge = app.getEdgeMap().get(physOp.getName());
				edge.put("source",appEdge.getSource());
				edge.put("destination",appEdge.getDestination());
				edge.put("data_rate",appEdge.getTupleNwLength());
				edges.add(edge);
				
				// Set tupple mapping
				JSONObject tuple = new JSONObject();
				tuple.put("operator_name", appEdge.getDestination());
				tuple.put("input", appEdge.getSource());
				tuple.put("output",appEdge.getDestination());
				tuples.add(tuple);
			}
		}
		doc.put("union_edges", union_edges);
		doc.put("edges", edges);
		doc.put("tuples", tuples);
		return doc;
	}
	
	public static JSONObject updateDatarate(JSONObject appJsonDoc, Map<String, Double> datastreams){
		
		
		JSONObject updatedAppJsonDoc = new JSONObject();
		
		JSONArray operators = new JSONArray(); 
		JSONArray source_edges = new JSONArray(); 
		JSONArray union_edges = new JSONArray();
		JSONArray edges = new JSONArray();
		JSONArray tuples =  (JSONArray) appJsonDoc.get("tuples"); 
		
		JSONArray prec_source_edges = (JSONArray) appJsonDoc.get("source_edges");
		ArrayList<String> visited = new ArrayList<String>();
		
		for(int i = 0; i<prec_source_edges.size(); i++){
			
			JSONObject source_edge = (JSONObject) prec_source_edges.get(i);
			String source_name = (String) source_edge.get("source");
			double datastream_rate = datastreams.get(source_name);
			source_edge.put("data_rate", datastream_rate);
			source_edges.add(source_edge);
			
			String destination = (String) source_edge.get("destination"); 
			while(!destination.equals("Sink")){
				
				JSONObject edge_object = nextEdge(appJsonDoc, destination);
				String edge_type = (String) edge_object.get("type");
				JSONObject edge = (JSONObject)edge_object.get("edge");
				JSONObject operator = getOperator(appJsonDoc, destination);
				double cmu = datastream_rate;
				if(visited.contains(operator.get("name"))){
				
					double current_cmu = (double)operator.get("cmu");
					cmu = cmu + current_cmu;
					operator.put("cmu", cmu);
					operators = removeObject(operators, destination);
					if(edge_type.equals("union_edges")){
						
						union_edges = removeObject(union_edges, destination);
						
					}else if(edge_type.equals("edges")){
						
						edges = removeObject(edges, destination);
					}
					
				}else{
					
					operator.put("cmu", cmu);
					visited.add((String)operator.get("name"));
				}
				double selectivity = (double)operator.get("selectivity");
				datastream_rate = cmu * selectivity;
				edge.put("data_rate", datastream_rate);
				operators.add(operator);
				if(edge_type.equals("union_edges")) {
					
					union_edges.add(edge);
					
				}else if(edge_type.equals("edges")) {
					
					edges.add(edge);
				}
				destination = (String)edge.get("destination");
				if(destination.equals("Sink")){
					
					JSONObject sink_operator = getOperator(appJsonDoc, destination);
					if(visited.contains(sink_operator.get("name"))){
						
						double current_cmu = (double)operator.get("cmu");
						cmu = datastream_rate + current_cmu;
						sink_operator.put("cmu", cmu);
						operators = removeObject(operators, "Sink");
						
					}else{
						
						sink_operator.put("cmu", datastream_rate);
						visited.add((String)sink_operator.get("name"));
					}
					operators.add(sink_operator);
				}
				
			}
			
		}
		
		
		// As we do not visit all union edge which is not connected with a source we add these edges anyway
		JSONArray prev_union_edges = (JSONArray) appJsonDoc.get("union_edges");
		JSONArray prev_edges = (JSONArray) appJsonDoc.get("edges");
		for(int i=0; i<prev_union_edges.size(); i++) {
			
			JSONObject obj = (JSONObject) prev_union_edges.get(i);
			String source = (String) obj.get("source");
			if(!visited.contains(source)) {
				
				JSONObject operator = getOperator(appJsonDoc, source);
				operators.add(operator);
				union_edges.add(obj);
			}
			
		}
		
		for(int i=0; i<prev_edges.size(); i++) {
			
			JSONObject obj = (JSONObject) prev_edges.get(i);
			String source = (String) obj.get("source");
			if(!visited.contains(source)) {
				
				JSONObject operator = getOperator(appJsonDoc, source);
				operators.add(operator);
				edges.add(obj);
			}
			
		}
		
		updatedAppJsonDoc.put("operators", operators);
		updatedAppJsonDoc.put("source_edges", source_edges);
		updatedAppJsonDoc.put("union_edges", union_edges);
		updatedAppJsonDoc.put("edges", edges);
		updatedAppJsonDoc.put("tuples", tuples);
		return updatedAppJsonDoc;
	}
	
	public static JSONArray removeObject(JSONArray object_array,String target_name) {
		
		for(int j=0; j<object_array.size(); j++) {
			
			JSONObject obj = (JSONObject) object_array.get(j);
			if(obj.containsKey("name")) {
				
				if(obj.get("name").equals(target_name)) {
					
					object_array.remove(j);
					break;
				}
				
			}else if(obj.containsKey("source")) {
				
				if(obj.get("source").equals(target_name)){
					
					object_array.remove(j);
					break;
				}
				
			}
		}
		return object_array;
	}
	public static JSONObject getOperator(JSONObject appJsonDoc, String operator_name){
		
		JSONObject operator = null;
		JSONArray operators = (JSONArray) appJsonDoc.get("operators");
		for(int i=0; i<operators.size(); i++) {
			
			JSONObject obj = (JSONObject) operators.get(i);
			if(obj.get("name").equals(operator_name)) {
				
				operator = obj;
				break;
			}
		}
		return operator;
	}
	public static JSONObject nextEdge(JSONObject appJsonDoc, String destination){
		
		JSONObject edge_object = null;
		boolean edgeFound = false;
		JSONArray union_edges = (JSONArray) appJsonDoc.get("union_edges");
		JSONArray edges = (JSONArray) appJsonDoc.get("edges");
		for(int i=0; i<union_edges.size(); i++) {
			
			JSONObject obj = (JSONObject) union_edges.get(i);
			if(obj.get("source").equals(destination)) {
				
				edge_object = new JSONObject();
				edge_object.put("type", "union_edges");
				edge_object.put("edge", obj);
				edgeFound = true;
				break;
			}
		}
		if(edgeFound) {
			
			return edge_object;
		}
		for(int i=0; i< edges.size(); i++) {
			
			JSONObject obj = (JSONObject) edges.get(i);
			if(obj.get("source").equals(destination)) {
				
				edge_object = new JSONObject();
				edge_object.put("type", "edges");
				edge_object.put("edge", obj);
				break;
			}
		}
		return edge_object;
	}
	public static JSONObject convertJsonFileToObject(String json_file) {
		
		JSONObject doc = null;
		try {
			doc = (JSONObject) JSONValue.parse(new FileReader(json_file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return doc;
	}
	
	
	private static void jsonToFile(JSONObject json) {
		
		StaticVariables.input_graph = StaticVariables.input_graph+"_"+StaticVariables.experiment+".json";
		generateJsonFile(json, StaticVariables.input_graph);
		
		
	}
	
	
	public static void generateJsonFile(JSONObject json, String file_path) {
		

		FileWriter file = null;
		try {
			 
            file = new FileWriter(file_path);
            file.write(json.toString());
 
        } catch (IOException e) {
            e.printStackTrace();
 
        } finally {
 
            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		
	}
	private static double getCmu(double min_data_rate, double max_data_rate) {
		
		return min_data_rate * max_data_rate;
		
	}
	
	private static double getSelecitity(double min_selectivity, double max_selectivity) {
		
		double selectivity = random_selectvity.nextDouble()*(max_selectivity-min_selectivity) + min_selectivity;
		return selectivity;
	}
	

	private static double getOperatorCost(double min_operator_cost, double max_operator_cost) {
		
		double operator_cost = random_operator_cost.nextDouble()*(max_operator_cost-min_operator_cost) + min_operator_cost;
		return operator_cost;
	}

	private static double getSelecitity(String operator_name){
		
		
		JSONObject appsDoc = JsonToApplication.convertJsonFileToObject(StaticVariables.custom_application);
		JSONObject appDoc = (JSONObject) appsDoc.get("C");
		JSONObject opDoc = (JSONObject) appDoc.get(operator_name);
		System.out.println("test="+operator_name);
		double selectivity = new BigDecimal( (double)(opDoc.get("selectivity"))).doubleValue();
		System.out.print(operator_name+"[selectivity="+selectivity);
		return selectivity;
		
	}

	private static double getOperatorCost(String operator_name){
		
		JSONObject appsDoc = JsonToApplication.convertJsonFileToObject(StaticVariables.custom_application);
		JSONObject appDoc = (JSONObject) appsDoc.get("C");
		JSONObject opDoc = (JSONObject) appDoc.get(operator_name);
		double operator_cost = new BigDecimal( (double)(opDoc.get("operator_cost"))).doubleValue();
		System.out.println("\toperator_cost="+operator_cost+"]");
		return operator_cost;
	}


	private static String getRandomType() 
    { 
		
		List<String> list = new ArrayList<String>();
		list.add("SINGLE_INPUT_EDGE");
		list.add("JOIN");
		Random rand = new Random(); 
        return list.get(rand.nextInt(list.size())); 
    }
	
	private static String typeToString(ModuleType operatorType) {
		
		String type = null;
		switch (operatorType){
		case JOIN:
			
			type = "JOIN";
			break;
		case UNION:
			type = "UNION";
			break;
		case SINGLE_INPUT_EDGE:
			
			type = "SINGLE_INPUT_EDGE";
			break;
		case MULTIPLE_OUTPUT_EDGES:
			
			type = "MULTIPLE_OUTPUT_EDGES";
			break;
		}
		return type;
	}

	public static int getNumber_of_sources() {
		return number_of_sources;
	}

	public static void setNumber_of_sources(int number_of_sources) {
		JsonToApplication.number_of_sources = number_of_sources;
	}

	public static int getNumber_of_operators() {
		return number_of_operators;
	}

	public static void setNumber_of_operators(int number_of_operators) {
		JsonToApplication.number_of_operators = number_of_operators;
	}

	
}
