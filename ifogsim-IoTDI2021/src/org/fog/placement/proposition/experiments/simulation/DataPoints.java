package org.fog.placement.proposition.experiments.simulation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.utils.JsonToApplication;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class DataPoints {

	private static Map<Double, Map<Integer, List<Double>>> data_points;

	public static void fillDataPoints() {

		data_points = new HashMap<Double, Map<Integer, List<Double>>>();
		ArrayList<String> datapointvalues = new ArrayList<String>() {
			{
				add("256");add("512");add("700");add("965");add("1221");add("1500");add("1700");add("1792");add("2000");add("2100");add("2128");add("2156");add("2256");add("2400");add("2656");add("2800");add("3023");add("3256");add("3400");

			}
		};

		switch (StaticVariables.problem_size) {
		case "_scale_6":

			datapointvalues = new ArrayList<String>() {
				{
					add("768");add("1050");add("1448");add("1832");add("2250");add("2688");add("3000");add("3150");add("3384");
				}
			};

			break;
			
		case "_scale_8":
			datapointvalues = new ArrayList<String>() {
				{
					add("1024");add("1400");add("1930");add("2442");add("3000");add("3584");add("4000");add("4200");add("4512");
				}
			};

			break;

		case "_scale_10":
			datapointvalues = new ArrayList<String>() {
				{
					add("1280");add("1750");add("2413");add("3053");add("3750");add("4480");add("5000");add("5250");add("5640");
				}
			};

			break;
			
		case "_scale_12":
			datapointvalues = new ArrayList<String>() {
				{

					add("1536");add("2100");add("2895");add("3663");add("4500");add("5376");add("6000");add("6300");add("6768");
				}
			};

			break;
		
		case "_scale_14":
			datapointvalues = new ArrayList<String>() {
				{
					add("1792");add("2450");add("3378");add("4274");add("5250");add("6272");add("7000");add("7350");add("7896");
				}
			};

			break;
			
		case "_scale_16":
			datapointvalues = new ArrayList<String>() {
				{
					add("2048");add("2800");add("3860");add("4884");add("6000");add("7168");add("8000");add("8400");add("9024");
				}
			};

			break;
		
		case "_scale_18":
			datapointvalues = new ArrayList<String>() {
				{
					add("2304");add("3150");add("4343");add("5495");add("6750");add("8064");add("9000");add("9450");add("10152");
				}
			};

			break;
		
		case "_scale_20":
			datapointvalues = new ArrayList<String>() {
				{
					add("2560");add("3500");add("4825");add("6105");add("7500");add("8960");add("10000");add("10500");add("11280");
				}
			};

			break;
			
		default:
			break;
		}

		for (String datapointvalue : datapointvalues) {
			int elements = 0;
			Map<Integer, List<Double>> experimentMap = new HashMap<Integer, List<Double>>();
			JSONObject datapointsObject = JsonToApplication.convertJsonFileToObject(StaticVariables.dataPointFile);
			JSONArray experiments = (JSONArray) datapointsObject.get(datapointvalue);
			for (int i = 0; i < experiments.size(); i++) {

				JSONObject experiment = (JSONObject) experiments.get(i);
				JSONArray datarates = (JSONArray) experiment.get("experiment_" + (i + 1));
				List<Double> distribution = new ArrayList<Double>();
				for (int j = 0; j < datarates.size(); j++) {

					double data_rate = new BigDecimal((long) datarates.get(j)).doubleValue();
					distribution.add(data_rate);
					elements = elements + 1;
				}
				experimentMap.put(i + 1, distribution);
			}
			double data_point = Double.valueOf(datapointvalue);
			data_points.put(data_point, experimentMap);
			System.out.println("Load " + elements + " data rate values for the data points " + data_point);
		}

	}

	public static Map<Double, Map<Integer, List<Double>>> getData_points() {
		return data_points;
	}

	public static void setData_points(Map<Double, Map<Integer, List<Double>>> data_points) {
		DataPoints.data_points = data_points;
	}

}
