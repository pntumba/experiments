import matplotlib.pyplot as plt
import numpy as np

data_point_965 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_1221 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_1500 = [0.001062095905,0,0,0,0,0,0,0,0,0,0,0,0,0.009444369612,0]
data_point_1792 = [0.01922615841,0,0,0,0,0,0,0,0.01204606681,0.008342537716,0,0,0,0.005530037716,0]
data_point_2000 = [0,0,0.001649380388,0.006868938578,0,0,0,0,0,0,0,0.008505522629,0,0,0.009229525862]
data_point_2100 = [0.008001751078,0.009275323276,0,0,0.005813577586,0,0,0,0.007306707974,0,0,0,0,0,0]
data_point_2256 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


# Calculate the average
data_point_965_mean = np.max(data_point_965)
data_point_1221_mean = np.max(data_point_1221)
data_point_1500_mean = np.max(data_point_1500)
data_point_1792_mean = np.max(data_point_1792)
data_point_2000_mean = np.max(data_point_2000)
data_point_2100_mean = np.max(data_point_2100)
data_point_2256_mean = np.max(data_point_2256)

max_differences = [data_point_1500_mean, data_point_1792_mean, data_point_2000_mean, data_point_2100_mean]

print '\n'
print np.min(max_differences)
print np.max(max_differences)


# Calculate the standard deviation
data_point_965_std = np.std(data_point_965)
data_point_1221_std = np.std(data_point_1221)
data_point_1500_std = np.std(data_point_1500)
data_point_1792_std = np.std(data_point_1792)
data_point_2000_std = np.std(data_point_2000)
data_point_2100_std = np.std(data_point_2100)
data_point_2256_std = np.std(data_point_2256)



# Define labels, positions, bar heights and error bar heights
labels = ['965', '1221', '1500','1792','2000','2100','2256']
x_pos = np.arange(len(labels))
CTEs = [data_point_965_mean, data_point_1221_mean, data_point_1500_mean,data_point_1792_mean, data_point_2000_mean, data_point_2100_mean,data_point_2256_mean]
error = [data_point_965_std, data_point_1221_std, data_point_1500_std,data_point_1792_std, data_point_2000_std, data_point_2100_std,data_point_2256_std]


# Build the plot
fig, ax = plt.subplots()
ax.bar(x_pos, CTEs,
       yerr=error,
       align='center',
       alpha=0.5,
       ecolor='black',
       capsize=10)

ax.set_ylabel('Difference of RU between SOO and SOO-CPLEX')
ax.set_xlabel('Data stream rate produced at the Edge (KB/sec)')
ax.set_xticks(x_pos)
ax.set_xticklabels(labels)
ax.yaxis.grid(True)


# Save the figure and show
plt.tight_layout()
plt.savefig('bar_plot_with_error_bars.png')
plt.show()



fig, ax = plt.subplots()
ax.plot(count, sequence, "-*b", label="data point", markersize=4)
ax.plot(count, Bmin, "->r", label="Bmin",markersize=4)
ax.plot(count, Bmax, "-<g", label="Bmax",markersize=4)
ax.legend()
plt.ylabel('Data stream rate produced at the Edge (KB/sec)')
plt.xlabel('Experiment number')
plt.show()


