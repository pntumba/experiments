package org.fog.placement.proposition.experiments.test;


import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppEdge;
import org.fog.application.AppLoop;
import org.fog.application.Application;
import org.fog.application.selectivity.FractionalSelectivity;
import org.fog.entities.FogBroker;
import org.fog.entities.PhysicalTopology;
import org.fog.entities.Tuple;
import org.fog.placement.Controller;
import org.fog.placement.ModulePlacementRCS;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.utils.JsonToApplication;
import org.fog.utils.JsonToTopology;
import org.json.simple.JSONObject;

/**
 * Simulation setup for EEG Beam Tractor Game extracting physical topology
 * 
 * @author Harshit Gupta
 *
 */
public class Start_RCS_with_main_class{

	public static void main(String[] args){

		Log.printLine("Starting TMS...");
		Map<String, List<String>> mappingModel = new HashMap<String, List<String>>();
		try {

			Log.disable();
			int num_user = 1; // number of cloud users
			Calendar calendar = Calendar.getInstance();
			boolean trace_flag = false; // mean trace events

			CloudSim.init(num_user, calendar, trace_flag);

			String appId = "IoTDI2021";

			FogBroker broker = new FogBroker("broker");

			Application application = createApplication(appId, broker.getId());
			application.setUserId(broker.getId());

			/*
			 * Creating the physical topology from specified JSON file
			 */
			
			PhysicalTopology physicalTopology = JsonToTopology.getPhysicalTopology(broker.getId(), appId,
					"src/org/fog/placement/proposition/experiments/topology/topology_test.json");
			Controller controller = new Controller("master-controller", physicalTopology.getFogDevices(),
					physicalTopology.getSensors(), physicalTopology.getActuators());

			
			/**
			 * 
			 * Defining the mapping model of the application
			 * 
			 */
			controller.submitApplication(application, 0,
					new ModulePlacementRCS(physicalTopology.getFogDevices(),
							physicalTopology.getSensors(), physicalTopology.getActuators(), application));

			CloudSim.startSimulation();

			CloudSim.stopSimulation();

		} catch (Exception e){

			e.printStackTrace();
			Log.printLine("Unwanted errors happen");
		}
		Log.printLine("TMS finished!");
	}

	@SuppressWarnings({ "serial" })
	private static Application createApplication(String appId, int userId){

		/*Application application = Application.createApplication(appId, userId);
		application.addAppModule("Union", 1162800, ModuleType.UNION,true);
		application.addAppModule("Join", 1162800, ModuleType.JOIN,true);
		application.addAppModule("Aggregation", 930240, ModuleType.SINGLE_INPUT_EDGE,true);
		application.addAppModule("Selection", 697680, ModuleType.SINGLE_INPUT_EDGE,true);
		application.addAppModule("Sink", 418608, ModuleType.SINGLE_INPUT_EDGE,true);
		
		application.addAppEdge("Antenna_1", "Union", 2000, 864200, "Antenna_1", Tuple.UP, AppEdge.SENSOR);
		application.addAppEdge("Antenna_2", "Union", 2000, 154600, "Antenna_2", Tuple.UP, AppEdge.SENSOR);
		application.addAppEdge("Antenna_3", "Union", 2000, 144000, "Antenna_3", Tuple.UP, AppEdge.SENSOR);
		application.addAppEdge("Union", "Join", 2000, 1162800, "Union", Tuple.UP, AppEdge.MODULE);
		application.addAppEdge("Join", "Aggregation", 2000, 930240, "Join", Tuple.UP, AppEdge.MODULE);
		application.addAppEdge("Aggregation", "Selection", 2000, 697680, "Aggregation", Tuple.UP, AppEdge.MODULE);
		application.addAppEdge("Selection", "Sink", 2000, 418608, "Selection", Tuple.UP, AppEdge.MODULE);

		application.addTupleMapping("Union", "Antenna_1", "Union", new FractionalSelectivity(1.0));
		application.addTupleMapping("Union", "Antenna_2", "Union", new FractionalSelectivity(1.0));
		application.addTupleMapping("Union", "Antenna_3", "Union", new FractionalSelectivity(1.0));
		application.addTupleMapping("Join", "Union", "Join", new FractionalSelectivity(0.8));
		application.addTupleMapping("Aggregation", "Join", "Aggregation", new FractionalSelectivity(0.75));
		application.addTupleMapping("Selection", "Aggregation", "Selection", new FractionalSelectivity(0.6));
		application.addTupleMapping("Sink", "Selection", "Sink", new FractionalSelectivity(1.0));
		
		final AppLoop loop1 = new AppLoop(new ArrayList<String>(){
			{
				add("Antenna_1");
				add("Union");
				add("Join");
				add("Aggregation");
				add("Selection");
				add("Sink");
			}
		});

		final AppLoop loop2 = new AppLoop(new ArrayList<String>() {
			{

				add("Antenna_2");
				add("Union");
				add("Join");
				add("Aggregation");
				add("Selection");
				add("Sink");
			}
		});
		final AppLoop loop3 = new AppLoop(new ArrayList<String>() {
			{

				add("Antenna_3");
				add("Union");
				add("Join");
				add("Aggregation");
				add("Selection");
				add("Sink");
			}

		});
		List<AppLoop> loops = new ArrayList<AppLoop>() {
			{
				add(loop1);
				add(loop2);
				add(loop3);
			}
		};
		application.setLoops(loops);*/
		
		//JSONObject object = JsonToApplication.getApplicationJson(5, 3, 1,256000.0, 0.2, 1.8,1);
		Application application = null;
		try {
		//	application = JsonToApplication.getApplication(userId, appId, object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// GeoCoverage geoCoverage = new GeoCoverage(-100, 100, -100, 100);
		return application;
	}
}