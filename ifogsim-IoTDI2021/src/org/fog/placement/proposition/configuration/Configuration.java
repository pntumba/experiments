package org.fog.placement.proposition.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.math3.util.Pair;
import org.checkerframework.checker.units.qual.A;
import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppEdge;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.application.selectivity.FractionalSelectivity;
import org.fog.application.selectivity.SelectivityModel;
import org.fog.entities.Actuator;
import org.fog.entities.FogDevice;
import org.fog.entities.Sensor;
import org.fog.entities.Tuple;
import org.fog.placement.proposition.utils.Convertor;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.utils.DynamicTupleSize;

public class Configuration{
	
	private Application application;
	private List<FogDevice> fogDevices;
	private List<Sensor> sensors;
	
	private List<Actuator> actuators;
	private Map<String, Integer> applicationToGraph = new ConcurrentHashMap<String, Integer>();
	private Map<Integer, String> graphToApplication = new ConcurrentHashMap<Integer, String>();
	private Map<Integer, Integer> sourcesToResourceNodes = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Integer> resourceNodeToSource = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Integer> resourcesToSinkNodes = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, ModuleType> operatorsType = new ConcurrentHashMap<Integer, ModuleType>();
	private Map<Integer, Double> operatorsSelectivity = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> operatorCosts = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Boolean> operatorsReplicability = new ConcurrentHashMap<Integer, Boolean>();
	private Map<Integer, Integer> countConnectedReplicatedGraphs = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Double> cumulatedSelectivitiesForMigratedSreams = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Graph> effectiveMapping = new HashMap<Integer, Graph>();
	private double bandwidthUsage;
	private Graph initialGraph = null;
	private Graph staticGraph = null;
	private Map<Integer, List<String>> mappingModel = new HashMap<Integer, List<String>>();
	private Map<Integer, List<String>> removingModel = new HashMap<Integer, List<String>>();
	private Map<Integer, List<String>> edgeCutAdjustingModel = new HashMap<Integer, List<String>>();
	private Map<Integer, Double> initialDataStreams = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Map<Integer, String>> replicatedGraphToApplication = new ConcurrentHashMap<Integer, Map<Integer,String>>();
	
	public Configuration(List<FogDevice> fogDevices, List<Sensor> sensors, List<Actuator> actuators){
		
		this.fogDevices = fogDevices;
		this.sensors = sensors;
		this.actuators = actuators;
	}
	
	public void convertApplicationToGraph(Application application) {
		
		Convertor convertor = new Convertor(this);
		convertor.applicationToGraph();
		for(AppModule module: application.getModules()) {
			
			if(module.getName().startsWith("Union_")){
				
				int vertex = getApplicationToGraph().get(module.getName());
				if(getCountConnectedReplicatedGraphs().containsKey(vertex)){
					
					int count = getCountConnectedReplicatedGraphs().get(vertex)+1;
					getCountConnectedReplicatedGraphs().put(vertex, count);
					
				}else{
					
					getCountConnectedReplicatedGraphs().put(vertex, 1);
				}
			}
		}

	}
	public void reconfigureApplicationTodeployOnFog(Map<Integer, Graph> graphMapping){
		
		
		ReconfigurationDownToFog downToFog = new ReconfigurationDownToFog(this);
		// applicationToString();
		for(int F_j: graphMapping.keySet()){
			
		   int S_j = resourceNodeToSource.get(F_j);
		   downToFog.replicateOperators(graphMapping.get(F_j), S_j);
		   downToFog.removeSourceFromInitialGraph(graphMapping.get(F_j), S_j);
		   downToFog.connectReplicatedGraphToInitialGraph(graphMapping.get(F_j), S_j); 	
		}
		applicationToString();
	}
	
	
	public void reconfigureApplicationToReleaseTheFog(Map<Integer, Graph> graphToRemove) {
		// TODO Auto-generated method stub
		ReconfigurationUpToCloud upToCloud = new ReconfigurationUpToCloud(this);	
		for(int F_j: graphToRemove.keySet()) {
			
			 int S_j = resourceNodeToSource.get(F_j);
			 
			 upToCloud.redirectSource(graphToRemove.get(F_j), S_j);
			
			 upToCloud.deleteReplicatedOperators(graphToRemove.get(F_j), S_j);
			 
			 if(StaticVariables.exeedAvailableComputationalResources.containsKey(S_j)) {
				 
				 StaticVariables.exeedAvailableComputationalResources.remove(S_j);
			 }
		}
		applicationToString();
	}

	
	public void addOperatorToMappingModel(int nodeId, String operatorName){
		
		if(!mappingModel.containsKey(nodeId)) {
			 
			 mappingModel.put(nodeId, new ArrayList<String>() {{add(operatorName);}});
			 
		 }else{
			 
			 mappingModel.get(nodeId).add(operatorName);
		 }
	}
	
	
	public void addOperatorToRemovingModel(int nodeId, String operatorName) {
		
		if(!removingModel.containsKey(nodeId)) {
			 
			removingModel.put(nodeId, new ArrayList<String>() {{add(operatorName);}});
			 
		 }else{
			 
			 removingModel.get(nodeId).add(operatorName);
		 }
	}
	
	public void updateReplicatedGraphToApplication(int FogNode, int vertex, String operatorName) {
		
		
		if(!replicatedGraphToApplication.containsKey(FogNode)) {
			 
			Map<Integer, String> graphToApplication = new ConcurrentHashMap<Integer, String>();
			graphToApplication.put(vertex, operatorName);
			replicatedGraphToApplication.put(FogNode, graphToApplication);
			 
		 }else{
			 
			 replicatedGraphToApplication.get(FogNode).put(vertex, operatorName);
		 }
		
		
	}
	
	public void addOperatorSelectivity(AppModule operator, int vertex) {

		double selectivity = 1.0;
		if (operator != null) {
			
			for (Pair<String, String> pair : operator.getSelectivityMap().keySet()) {

				selectivity = operator.getSelectivityMap().get(pair).getMeanRate();
			}
			operatorCosts.put(vertex, operator.getOperatorCost());
		}
		operatorsSelectivity.put(vertex, selectivity);
		

	}
	
	public AppEdge removeAppEdge(String source) {
		
		AppEdge appEdge = null;
		int edge_index = 0;
		for(int i =0; i < application.getEdges().size(); i++) {
			
			if(application.getEdges().get(i).getSource().equals(source)) {
				
				appEdge = application.getEdges().remove(i);
				
				break;
			}
		}
		return appEdge;
		
	}
	
	public void removeAppModule(String moduleName) {
		
		for(int i= 0; i < application.getModules().size(); i++) {
			
			if(application.getModules().get(i).getName().equals(moduleName)) {
				
				application.getModules().remove(i);
			}
		}
	}	
	
	public void removeTuppleMapping(String vertex1_name, String vertex2_name){
		
		if(application.getModuleByName(vertex2_name) != null){
			
			
			if(application.getModuleByName(vertex2_name).getSelectivityMap() != null) {
				
				application.getModuleByName(vertex2_name).getSelectivityMap()
				.remove(new Pair<String, String>(vertex1_name, vertex2_name));
			}
		}
		
		
	}
	public AppEdge getAppEdgeBySource(String source) {
		
		for(AppEdge appEdge: application.getEdges()) {
			
			
			if(appEdge.getSource().equals(source)) {
				
				return appEdge;
			}
		}
		return null;
	}
	public void applicationToString() {
		
		System.out.println("===================");
		System.out.println("Application modules");
		for(AppModule module : application.getModules()){
			
			System.out.println(module.getName()+":"+module.getRam()+","+module.getType()+","+module.isLocallyReplicable()+","+module.getSelectivity());
		}
		
		System.out.println("\nApplication edges");
		for(AppEdge appEdge : application.getEdges()){
		
			System.out.println(appEdge.toString()+" tupleNwLength="+appEdge.getTupleNwLength());
		}
		
		System.out.println("\nApplication tupple mapping");
		for(AppModule module : application.getModules()){
			
			for(Pair<String, String> pair : module.getSelectivityMap().keySet()) {
				
				System.out.println(module.getName()+" ["+pair.getFirst()+", "+pair.getSecond()+"] Selectivity="+module.getSelectivityMap().get(pair).getMeanRate());
			}
			
		}
		System.out.println("===================");
		
	}
	public Map<Integer, Boolean> getOperatorsReplicability() {
		
		return operatorsReplicability;
	}

	public Map<Integer, ModuleType> getOperatorsType() {
		
		return operatorsType;
	}

	public Map<Integer, Double> getOperatorsSelectivity() {
		
		return operatorsSelectivity;
	}
	
	public Map<Integer, Integer> getSourcesToResourceNodes() {
		
		return sourcesToResourceNodes;
	}

	public void setOperatorSelectivity(Map<Integer, Double> operatorsSelectivity) {
		this.operatorsSelectivity = operatorsSelectivity;
	}

	public double getBandwidthUsage() {
		return bandwidthUsage;
	}

	public void setBandwidthUsage(double bandwidthUsage){
		this.bandwidthUsage = bandwidthUsage;
	}

	public Graph getInitialGraph() {
		return initialGraph;
	}

	public void setInitialGraph(Graph initialGraph) {
		this.initialGraph = initialGraph;
	}

	public Map<Integer, List<String>> getMappingModel() {
		return mappingModel;
	}

	public void setMappingModel(Map<Integer, List<String>> mappingModel) {
		this.mappingModel = mappingModel;	
	}
	
	public Map<String, Integer> getApplicationToGraph() {
		return applicationToGraph;
	}

	public void setApplicationToGraph(Map<String, Integer> applicationToGraph) {
		this.applicationToGraph = applicationToGraph;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Map<Integer, String> getGraphToApplication() {
		return graphToApplication;
	}

	public void setGraphToApplication(Map<Integer, String> graphToApplication) {
		this.graphToApplication = graphToApplication;
	}

	public Map<Integer, Integer> getResourceNodeToSource() {
		return resourceNodeToSource;
	}

	public void setResourceNodeToSource(Map<Integer, Integer> resourceNodeToSource) {
		this.resourceNodeToSource = resourceNodeToSource;
	}

	public Map<Integer, Integer> getResourcesToSinkNodes() {
		return resourcesToSinkNodes;
	}

	public void setResourcesToSinkNodes(Map<Integer, Integer> resourcesToSinkNodes) {
		this.resourcesToSinkNodes = resourcesToSinkNodes;
	}

	public Map<Integer, Map<Integer, String>> getReplicatedGraphToApplication() {
		return replicatedGraphToApplication;
	}

	public void setReplicatedGraphToApplication(Map<Integer, Map<Integer, String>> replicatedGraphToApplication) {
		this.replicatedGraphToApplication = replicatedGraphToApplication;
	}

	public void setSourcesToResourceNodes(Map<Integer, Integer> sourcesToResourceNodes) {
		this.sourcesToResourceNodes = sourcesToResourceNodes;
	}

	public void setOperatorsType(Map<Integer, ModuleType> operatorsType) {
		this.operatorsType = operatorsType;
	}

	public void setOperatorsSelectivity(Map<Integer, Double> operatorsSelectivity) {
		this.operatorsSelectivity = operatorsSelectivity;
	}

	public void setOperatorsReplicability(Map<Integer, Boolean> operatorsReplicability) {
		this.operatorsReplicability = operatorsReplicability;
	}
	
	public List<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(List<Sensor> sensors) {
		this.sensors = sensors;
	}

	public List<Actuator> getActuators() {
		return actuators;
	}

	public void setActuators(List<Actuator> actuators) {
		this.actuators = actuators;
	}

	public Map<Integer, List<String>> getRemovingModel() {
		return removingModel;
	}

	public void setRemovingModel(Map<Integer, List<String>> removingModel) {
		this.removingModel = removingModel;
	}

	public Map<Integer, List<String>> getEdgeCutAdjustingModel() {
		return edgeCutAdjustingModel;
	}

	public void setEdgeCutAdjustingModel(Map<Integer, List<String>> edgeCutAdjustingModel) {
		this.edgeCutAdjustingModel = edgeCutAdjustingModel;
	}

	public Map<Integer, Integer> getCountConnectedReplicatedGraphs() {
		return countConnectedReplicatedGraphs;
	}

	public void setCountConnectedReplicatedGraphs(Map<Integer, Integer> countConnectedReplicatedGraphs) {
		this.countConnectedReplicatedGraphs = countConnectedReplicatedGraphs;
	}

	public Map<Integer, Double> getInitialDataStreams() {
		return initialDataStreams;
	}

	public void setInitialDataStreams(Map<Integer, Double> initialDataStreams) {
		this.initialDataStreams = initialDataStreams;
	}

	public Graph getStaticGraph() {
		return staticGraph;
	}

	public void setStaticGraph(Graph staticGraph) {
		this.staticGraph = staticGraph;
	}

	public Map<Integer, Double> getCumulatedSelectivitiesForMigratedSreams() {
		return cumulatedSelectivitiesForMigratedSreams;
	}

	public void setCumulatedSelectivitiesForMigratedSreams(Map<Integer, Double> cumulatedSelectivitiesForMigratedSreams) {
		this.cumulatedSelectivitiesForMigratedSreams = cumulatedSelectivitiesForMigratedSreams;
	}

	public Map<Integer, Graph> getEffectiveMapping() {
		return effectiveMapping;
	}

	public void setEffectiveMapping(Map<Integer, Graph> effectiveMapping) {
		this.effectiveMapping = effectiveMapping;
	}

	public Map<Integer, Double> getOperatorCosts() {
		return operatorCosts;
	}

	public void setOperatorCosts(Map<Integer, Double> operatorCosts) {
		this.operatorCosts = operatorCosts;
	}



}
