package org.fog.placement.proposition.utils;

public enum Triggers {
	
	MIGRATE_TOFOG,
	MIGRATE_BACK_INCLOUD,
	ADJUST_EDGECUT,
	CLOUD_ONLY,
	REUSE_DEPLOYMENT,
}
