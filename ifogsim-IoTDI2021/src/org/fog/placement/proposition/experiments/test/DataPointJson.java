package org.fog.placement.proposition.experiments.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.utils.JsonToApplication;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class DataPointJson {

	private static double min_threshold = 10;
	private static double max_threshold = 1000;
	private static ArrayList<Double> totalDataStreamRates = new ArrayList<Double>();
	private static Map<Integer, Integer> datasources = null;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		generateDataPoint(15, 20, 5);
	}

	public static void generateDataPoint(int expNbr, int data_rate_nbr, double ratio) {
        
		totalDataStreamRates.add(512.0 * ratio);
		totalDataStreamRates.add(700.0 * ratio);
	    totalDataStreamRates.add(965.0 * ratio);
	   
		totalDataStreamRates.add(1221.0 * ratio);
		
		totalDataStreamRates.add(1500.0 * ratio);
		totalDataStreamRates.add(1792.0 * ratio);
		
		
		totalDataStreamRates.add(2000.0 * ratio);
		totalDataStreamRates.add(2100.0 * ratio);
		
		totalDataStreamRates.add(2256.0 * ratio);

		JSONObject datapointsObject = new JSONObject();
		for (double data_rate : totalDataStreamRates) {
			
			double totalDataRate = Math.ceil(data_rate);
			System.out.println("\nDistribute totalDataRate="+totalDataRate);
			JSONArray experiments = new JSONArray();
			for (int i = 1; i <= expNbr; i++) {

				JSONArray datarates = new JSONArray();
				datasources = new ConcurrentHashMap<Integer, Integer>();
				double total = generateInitialRate(0.0, totalDataRate, data_rate_nbr);
				System.out.println("\ttotal="+total + " experiment_" + i + " " + datasources);
				for (int source_id : datasources.keySet()) {

					datarates.add((int) datasources.get(source_id));

				}
				JSONObject experiment = new JSONObject();
				experiment.put("experiment_" + i, datarates);
				experiments.add(experiment);
			}
			datapointsObject.put("" + (int) totalDataRate, experiments);

		}
		String datapointsFile = StaticVariables.input_directory + "datapoints_scale_"+data_rate_nbr+".json";
		JsonToApplication.generateJsonFile(datapointsObject, datapointsFile);

	}

	private static double generateInitialRate(double min_rate, double max_rate, int data_rate_nbr) {

		double total = 0.0;
		boolean isLower = false;
		boolean isHigher = false;
		double min = min_rate;
		double max = max_rate * 1 / 10;
		for (int i = 1; i <= data_rate_nbr - 1; i++) {

			double x = min + Math.random() * ((max - min) + 1);
			double rate = Math.ceil(x);
			datasources.put(i, (int)rate);
			if (rate < min_threshold) {

				isLower = true;
				
			}else if (rate > max_threshold) {

				isHigher = true;
			}
			total = total + rate;
		}
		if (total < max_rate) {

			double rate = max_rate - total;
			datasources.put(data_rate_nbr, (int)rate);
			total = total + rate;
			if (rate < min_threshold) {

				isLower = true;
				
			}else if (rate > max_threshold) {

				isHigher = true;
			}

		} 
		else {

			return generateInitialRate(min_rate, max_rate, data_rate_nbr);

		}
		// System.out.println(max + "datasources= "+ datasources);
		if (isLower || isHigher) {

			return generateInitialRate(min_rate, max_rate, data_rate_nbr);

		}
		return total;

	}

	public void printDataPoint(String jsonFileName, int totalDataStreamRate) {

		String datapointsFile = StaticVariables.input_directory + "" + jsonFileName;
		JSONObject datapointsObject = JsonToApplication.convertJsonFileToObject(datapointsFile);
		JSONArray experiments = (JSONArray) datapointsObject.get("" + totalDataStreamRate);
		for (int i = 0; i < experiments.size(); i++) {

			JSONObject experiment = (JSONObject) experiments.get(i);
			JSONArray datarates = (JSONArray) experiment.get("experiment_" + (i + 1));
			for (int j = 0; j < datarates.size(); j++) {

				System.out.print(datarates.get(j) + "\t");
			}
			System.out.println();

		}

	}
}
