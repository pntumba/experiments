package org.fog.placement;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.entities.FogDevice;
import org.fog.placement.proposition.monitoring.CostModel;
import org.fog.placement.proposition.utils.ALGORITHM;
import org.fog.placement.proposition.utils.Triggers;

public abstract class ModulePlacement {
	
	
	public static int ONLY_CLOUD = 1;
	public static int EDGEWARDS = 2;
	public static int USER_MAPPING = 3;
    
	
	private List<FogDevice> fogDevices;
	private Application application;
	private Map<String, List<Integer>> moduleToDeviceMap;
	private Map<Integer, List<AppModule>> deviceToModuleMap;
	private Map<Integer, Map<String, Integer>> moduleInstanceCountMap;
	private Map<Integer, List<String>> modulesScheduled;
	
	
	
	private boolean useExistingApplicationState = false;
	private ALGORITHM usedAlgorithm = null;
	protected abstract void mapModules();
	
	
	private Instant startTime = null;
	private Instant finishTime = null;
	
	protected boolean canBeCreated(FogDevice fogDevice, AppModule module){
		return fogDevice.getVmAllocationPolicy().allocateHostForVm(module);
	}
	
	protected void deallocate(FogDevice fogDevice, AppModule module){
		
		fogDevice.getHost().getRamProvisioner().deallocateRamForVm((Vm)module);
		fogDevice.getHost().deallocatePesForVm((Vm)module);
		fogDevice.getVmAllocationPolicy().deallocateHostForVm((Vm)module);
		fogDevice.getVmList().remove((Vm)module);
	}
	
	protected int getParentDevice(int fogDeviceId){
		return ((FogDevice)CloudSim.getEntity(fogDeviceId)).getParentId();
	}
	
	protected FogDevice getFogDeviceById(int fogDeviceId){
		return (FogDevice)CloudSim.getEntity(fogDeviceId);
	}
	
	protected boolean createModuleInstanceOnDevice(AppModule _module, final FogDevice device, int instanceCount){
		return false;
	}
	
	protected boolean createModuleInstanceOnDevice(AppModule _module, final FogDevice device){
		AppModule module = null;
		System.out.println(_module.getName()+" Required memory ="+_module.getRam() * _module.getOperatorCost());
		if(getModuleToDeviceMap().containsKey(_module.getName()))
			module = new AppModule(_module);
		else
			module = _module;
			
		if(canBeCreated(device, module)){
			System.out.println("Creating "+module.getName()+" on device "+device.getName());
			if(!device.getName().equals("cloud")) {
				
				if(CostModel.getFog_cpu_memory_usage().containsKey(device.getId())) {
					
					double current_ram = CostModel.getFog_cpu_memory_usage().get(device.getId());
					CostModel.getFog_cpu_memory_usage().put(device.getId(), (double)module.getRam() * module.getOperatorCost()+current_ram);
				}else {
					
					CostModel.getFog_cpu_memory_usage().put(device.getId(), (double)module.getRam()*module.getOperatorCost());
				}
			}	
			if(!getDeviceToModuleMap().containsKey(device.getId()))
				getDeviceToModuleMap().put(device.getId(), new ArrayList<AppModule>());
			getDeviceToModuleMap().get(device.getId()).add(module);
			
			if(!getModuleToDeviceMap().containsKey(module.getName()))
				getModuleToDeviceMap().put(module.getName(), new ArrayList<Integer>());
			getModuleToDeviceMap().get(module.getName()).add(device.getId());
			return true;
		} else {
			System.err.println("Module "+module.getName()+" cannot be created on device "+device.getName());
			System.err.println("Terminating");
			return false;
		}
	}
	
	protected boolean removeModuleInstanceOnDevice(AppModule _module, final FogDevice device) {

		AppModule module = null;
		if (getModuleToDeviceMap().containsKey(_module.getName()))
			module = new AppModule(_module);
		else
			module = _module;

			deallocate(device, module);
			/*boolean moduleExist = false;
			if(getDeviceToModuleMap().containsKey(device.getId())) {
				
				for(AppModule appModule: getDeviceToModuleMap().get(device.getId())) {
					
					if(appModule.getName().equals(module.getName())) {
						
						moduleExist = true;
					}
				}
				
			}
			if(!moduleExist){
				
				System.err.println("Module " + module.getName() + " cannot be removed on device " + device.getName());
				return false;
			}*/
			if (!getModuleToDeviceMap().containsKey(module.getName())) {
				System.err.println("Module " + module.getName() + " cannot be removed to device " + device.getName());
				return false;
			}
			getModuleToDeviceMap().remove(module.getName());
			getDeviceToModuleMap().get(device.getId()).remove(module);
			System.out.println("Romoving " + module.getName() + " on device " + device.getName());
			if(device.getName().equals("cloud")) {
				
				if(CostModel.getFog_cpu_memory_usage().containsKey(device.getId())){
					
					double current_ram = CostModel.getFog_cpu_memory_usage().get(device.getId());
					double new_ram = current_ram - (double)module.getRam() * module.getOperatorCost();
					if(new_ram < 0) {
						
						new_ram = 0.0;
					}
					CostModel.getFog_cpu_memory_usage().put(device.getId(),new_ram);
				}

			}else {
				
				if(CostModel.getFog_cpu_memory_usage().containsKey(device.getId())){
					
					CostModel.getFog_cpu_memory_usage().put(device.getId(), 0.00);
				}
			}
			return true;

	}
	
	protected FogDevice getDeviceByName(String deviceName) {
		for(FogDevice dev : getFogDevices()){
			if(dev.getName().equals(deviceName))
				return dev;
		}
		return null;
	}
	
	protected FogDevice getDeviceById(int id){
		for(FogDevice dev : getFogDevices()){
			if(dev.getId() == id)
				return dev;
		}
		return null;
	}
	
	protected long duration() {
		
		return Duration.between(getStartTime(), getFinishTime()).toMillis();
	}
	
	public List<FogDevice> getFogDevices() {
		return fogDevices;
	}

	public void setFogDevices(List<FogDevice> fogDevices) {
		this.fogDevices = fogDevices;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Map<String, List<Integer>> getModuleToDeviceMap() {
		return moduleToDeviceMap;
	}

	public void setModuleToDeviceMap(Map<String, List<Integer>> moduleToDeviceMap) {
		this.moduleToDeviceMap = moduleToDeviceMap;
	}

	public Map<Integer, List<AppModule>> getDeviceToModuleMap() {
		return deviceToModuleMap;
	}

	public void setDeviceToModuleMap(Map<Integer, List<AppModule>> deviceToModuleMap) {
		this.deviceToModuleMap = deviceToModuleMap;
	}

	public Map<Integer, Map<String, Integer>> getModuleInstanceCountMap() {
		return moduleInstanceCountMap;
	}

	public void setModuleInstanceCountMap(Map<Integer, Map<String, Integer>> moduleInstanceCountMap) {
		this.moduleInstanceCountMap = moduleInstanceCountMap;
	}
	
	public Map<Integer, List<String>> getModulesScheduled() {
		return modulesScheduled;
	}

	public void setModulesScheduled(Map<Integer, List<String>> modulesScheduled) {
		this.modulesScheduled = modulesScheduled;
	}


	public ALGORITHM getUsedAlgorithm() {
		return usedAlgorithm;
	}


	public void setUsedAlgorithm(ALGORITHM usedAlgorithm) {
		this.usedAlgorithm = usedAlgorithm;
	}


	public boolean isUseExistingApplicationState() {
		return useExistingApplicationState;
	}


	public void setUseExistingApplicationState(boolean useExistingApplicationState) {
		this.useExistingApplicationState = useExistingApplicationState;
	}

	public Instant getStartTime() {
		return startTime;
	}

	public void setStartTime(){
		
		this.startTime = Instant.now();
	}

	public Instant getFinishTime(){
		return finishTime;
	}

	public void setFinishTime(){
		
		this.finishTime = Instant.now();
	}
}
