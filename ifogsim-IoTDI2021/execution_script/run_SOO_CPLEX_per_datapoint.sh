#!/bin/bash

rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json

total_data_rates=(256 512 700 965 1221 1500 1792 2000 2256 2400 2656 2800 3023)
#mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS_UR 8 ${total_data_rates[0]} 0 n"
#echo "Looping for RCS[0]: data stream=${total_data_rates[0]}, iteration=1 has finished!"		
#sleep 10	
execution_counter=1
total_data_rate=1500
for i in {1..15..1}
do	
			
				mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_SOO_CPLEX" -Dexec.args="SOO_CPLEX 10 $total_data_rate $execution_counter n"
				echo "Looping for SOOwithILP[$execution_counter]: data stream=$total_data_rate, iteration=$i has finished!"
				let execution_counter=execution_counter+1 
				sleep 1
done
