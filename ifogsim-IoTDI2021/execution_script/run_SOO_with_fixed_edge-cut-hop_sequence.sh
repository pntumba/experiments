#!/bin/bash

total_data_rates=(3023 1792 965 2256 2656 2000 2800 1221 1500 2400 1221 2656 1792 2256 965 2400 1500 2800 2000 3023 965 2256 2000 1500 1221 3023 2656 2400 2800 1792 2000 1500 1792 965 2800 2400 3023 2656 2256 1221 2400 2656 1221 2000 3023 2800 1792 965 1500 2256 1792 2256 2800 2000 3023 1500 2656 965 2400 1221 1792 3023 2656 2256 2400 965 1221 2000 2800 1500 1500 2000 1792 2400 2800 2256 2656 3023 1221 965 2656 2000 1500 2800 3023 1792 1221 2400 2256 965 2800 1792 2000 965 1221 2400 1500 2656 2256 3023 965 2000 2256 2800 1500 1221 3023 1792 2400 2656 965 2000 2400 2656 1221 2256 1500 3023 2800 1792 3023 2000 965 2256 1221 2656 2400 2800 1792 1500 2800 3023 2400 2656 1792 965 2000 1500 2256 1221 2656 2256 1500 2000 3023 965 1221 1792 2400 2800)
rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json
for k in {1..3..1}
do
	execution_counter=0
	for i in {0..149..1}
	do	
			
				total_data_rate=${total_data_rates[$i]}
				let execution_counter=execution_counter+1
				mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_SOO_DeltaRU" -Dexec.args="SOO_DeltaRU_UR 10 $total_data_rate $execution_counter $k n"
				echo "Looping for SOO_DeltaRU[$execution_counter]: k=$k, data stream=$total_data_rate, iteration=$i has finished!" 
				sleep 10
	done
	mkdir -p src/org/fog/placement/proposition/experiments/Results_log/2048MB/SOO_FIXED_HOPS_$k
	mv src/org/fog/placement/proposition/experiments/output/* src/org/fog/placement/proposition/experiments/Results_log/2048MB/SOO_FIXED_HOPS_$k/
done



