package org.fog.placement.proposition.experiments.simulation;

import java.io.FileReader;
import java.math.BigDecimal;
import java.util.Calendar;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.Application;
import org.fog.entities.FogBroker;
import org.fog.entities.PhysicalTopology;
import org.fog.placement.Controller;
import org.fog.placement.ModulePlacementRCS;
import org.fog.placement.proposition.monitoring.CostModel;
import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.placement.proposition.utils.UtilFunctions;
import org.fog.utils.Config;
import org.fog.utils.JsonToApplication;
import org.fog.utils.JsonToTopology;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Simulation setup for EEG Beam Tractor Game extracting physical topology
 * @author Harshit Gupta
 *
 */

public class Start_RCS{
	
	private static String dataPointFile = "datapoints.json";
	
	public static void main(String[] args){

		if(args.length != 5){
			
			System.out.println("Require 5 arguments");
			System.out.println("ALgorithm operator_factor total_data_rate iteration usage_of_app_state");
			System.out.println("Example: RCS_UR 1 256 1 y");
			System.exit(0);
		}
         
		Log.printLine("Starting TMS...");
		try {

			Log.disable();
			int num_user = 1; // number of cloud users
			Calendar calendar = Calendar.getInstance();
			boolean trace_flag = false; // mean trace events

			CloudSim.init(num_user, calendar, trace_flag);

			String appId = "IoTDI2021";
			FogBroker broker = new FogBroker("broker");
			StaticVariables.experiment  = String.valueOf(args[0]).toString();
			int operator_number = Integer.valueOf(args[1]).intValue();
			double total_data_rate = Double.valueOf(args[2]).doubleValue();
			int iteration = Integer.valueOf(args[3]).intValue();
			String USE_APP_STATE = String.valueOf(args[4]);
			if (USE_APP_STATE.equals("y")) {

				System.out.println("Use application state");
				Config.USE_APPLICATION_STATE = true;
			}else if (USE_APP_STATE.equals("s")) {

				System.out.println("Use application state and static parameters");
				Config.STATIC_OPERATOR_PARAMETER = true;
				Config.USE_APPLICATION_STATE = false;

			} else {

				Config.USE_APPLICATION_STATE = false;
			}
			double max_data_rate  = total_data_rate;
			CostModel.setSource_number(StaticVariables.source_number);
			CostModel.setFog_to_cloud_upper_threshold_netw_bwd(StaticVariables.Buthr);
			CostModel.setFog_to_cloud_lower_threshold_netw_bwd(StaticVariables.Blthr);			
			Application application = createApplication(
														appId, 
														broker.getId(), 
														operator_number, 
														StaticVariables.source_number, 
														iteration,
														StaticVariables.min_data_rate, 
														max_data_rate, 
														StaticVariables.min_selectivity,
														StaticVariables.max_selectivity,
														StaticVariables.min_operator_cost,
														StaticVariables.max_operator_cost);
			application.setUserId(broker.getId());
			
			/*
			 * Creating the physical topology from specified JSON file
			 */
			
			PhysicalTopology physicalTopology = JsonToTopology.getPhysicalTopology(broker.getId(), appId,StaticVariables.physical_topology);
			Controller controller = new Controller("master-controller", physicalTopology.getFogDevices(),
					physicalTopology.getSensors(), physicalTopology.getActuators());
			
			/**
			 * 
			 * Defining the mapping model of the application
			 * 
			 */
			
			controller.submitApplication(application, 0,
					new ModulePlacementRCS(physicalTopology.getFogDevices(),
							physicalTopology.getSensors(), physicalTopology.getActuators(), application));

			CloudSim.startSimulation();

			CloudSim.stopSimulation();

		} catch (Exception e){

			e.printStackTrace();
			Log.printLine("Unwanted errors happen");
		}
		Log.printLine("TMS finished!");
	}

	@SuppressWarnings({ "serial" })
	private static Application createApplication(String appId, int userId, int operator_number, int source_number, int iteration, double min_data_rate, double max_data_rate, double min_selectivity, double max_selectivity,double min_operator_cost, double max_operator_cost){
         
		StaticVariables.data_stream_size = max_data_rate;
		JSONObject object = JsonToApplication.getApplicationJson(operator_number, source_number, min_data_rate, max_data_rate, min_selectivity, max_selectivity,min_operator_cost,max_operator_cost,iteration);
		
		Application application = null;
		try {
			application = JsonToApplication.getApplication(userId, appId, object);
			//UtilFunctions.applicationToString(application);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //System.exit(0);
		// GeoCoverage geoCoverage = new GeoCoverage(-100, 100, -100, 100);
		return application;
	}
}