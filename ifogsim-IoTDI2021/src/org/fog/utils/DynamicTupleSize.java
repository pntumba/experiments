package org.fog.utils;

public class DynamicTupleSize {

	private static double load=1.0;

	public static double getLoad() {
		return load;
	}

	public static void setLoad(double load){
		DynamicTupleSize.load = load;
	}
}
