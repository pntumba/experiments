package org.fog.placement.proposition.configuration;

import org.apache.commons.math3.util.Pair;
import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppEdge;
import org.fog.application.AppModule;
import org.fog.application.selectivity.FractionalSelectivity;
import org.fog.entities.Tuple;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.utils.DynamicTupleSize;

public class ReconfigurationDownToFog {

	private Configuration reconfigure;

	public ReconfigurationDownToFog(Configuration reconfigure){

		this.reconfigure = reconfigure;
	}

	public void replicateOperators(Graph graphToReplicate, int S_j) {

		
		int F_j = reconfigure.getSourcesToResourceNodes().get(S_j);

		String sourceName = reconfigure.getGraphToApplication().get(S_j);
		int  sourceVetex = S_j;

		for (int vertex1 : graphToReplicate.getAdjVertices().keySet()){

			// Create application module for the replicated operator
			if (!graphToReplicate.isSource(vertex1)
					&& reconfigure.getOperatorsType().get(vertex1) != ModuleType.UNION){
                
				int ram = 0;
				for(int predVeretx: graphToReplicate.getPredecessors(vertex1)) {
					
					ram = (int)graphToReplicate.getEdgeWeight(predVeretx, vertex1);
				}
				String vertexName = reconfigure.getGraphToApplication().get(vertex1);
				String rep_vertexName = "rep_" + reconfigure.getGraphToApplication().get(vertex1) + "_" + S_j;
				AppModule operator = reconfigure.getApplication().getModuleByName(vertexName);
				reconfigure.getApplication().addAppModule(rep_vertexName, ram, operator.getType(),
						operator.isLocallyReplicable(), operator.getSelectivity(), operator.getOperatorCost());
				reconfigure.addOperatorToMappingModel(F_j, rep_vertexName);
				reconfigure.updateReplicatedGraphToApplication(F_j, vertex1, rep_vertexName);
				sourceVetex = vertex1;
				
			}
		}

		for (int vertex1 : graphToReplicate.getAdjVertices().keySet()) {

			// Create application edge between the replicated operators
			if (reconfigure.getOperatorsType().get(vertex1) != ModuleType.UNION) {

				String rep_vertex1_name = null;
				String rep_vertex2_name = null;
				double data_stream_rate = 0.0;

				// Connect the source with the replicated operators
				if (graphToReplicate.isSource(vertex1)) {

					rep_vertex1_name = reconfigure.getGraphToApplication().get(S_j);
					int vertex2 = 0;
					for (int vertex : graphToReplicate.getAdjVertices(vertex1).keySet()) {

						if (reconfigure.getOperatorsType().get(vertex) == ModuleType.UNION) {

							for (int successor : graphToReplicate.getSuccessors(vertex)) {

								// Connect the source with the first operators in the replicatred graph.
								vertex2 = successor;
								data_stream_rate = graphToReplicate.getEdgeWeight(vertex, vertex2);
								System.out.println("F_j="+F_j+" vertex2="+vertex2);
								System.out.println("getReplicatedGraphToApplication="+reconfigure.getReplicatedGraphToApplication());
								rep_vertex2_name = reconfigure.getReplicatedGraphToApplication().get(F_j).get(vertex2);
								reconfigure.getApplication().addAppEdge(rep_vertex1_name, rep_vertex2_name, 2,
										data_stream_rate, rep_vertex1_name, Tuple.UP, AppEdge.SENSOR);
                                
								// Update the remaining data stream rate flowing for the sources connected to
								// the initial graph.

								String vertex_name = reconfigure.getGraphToApplication().get(vertex);
								AppEdge appEdge = reconfigure.getAppEdgeBySource(vertex_name);
								if (appEdge != null) {
									double tupleNwLength = appEdge.getTupleNwLength() - data_stream_rate;
									if(tupleNwLength < 0) {
										
										tupleNwLength = 0.0;
									}
									reconfigure.getApplication().getEdgeMap().get(vertex_name)
											.setTupleNwLength(tupleNwLength);
								}

							}
						}

					}

				} else {

					// Create application edge between the replicated operators
					rep_vertex1_name = reconfigure.getReplicatedGraphToApplication().get(F_j).get(vertex1);
					for (int vertex2 : graphToReplicate.getSuccessors(vertex1)) {

						if (!graphToReplicate.getAdjVertices().containsKey(vertex2)) {

							continue;
						}

						rep_vertex2_name = reconfigure.getReplicatedGraphToApplication().get(F_j).get(vertex2);
						data_stream_rate = graphToReplicate.getEdgeWeight(vertex1, vertex2)/DynamicTupleSize.getLoad();
						reconfigure.getApplication().addAppEdge(rep_vertex1_name, rep_vertex2_name, 2,
								data_stream_rate, rep_vertex1_name, Tuple.UP, AppEdge.SENSOR);

						String vertex1_name = reconfigure.getGraphToApplication().get(vertex1);
						AppEdge appEdge = reconfigure.getApplication().getEdgeMap().get(vertex1_name);
						double tupleNwLength = appEdge.getTupleNwLength() - data_stream_rate;
						if(tupleNwLength < 0) {
							
							tupleNwLength = 0.0;
						}
						reconfigure.getApplication().getEdgeMap().get(vertex1_name).setTupleNwLength(tupleNwLength);
					}

					// Create tuple mapping for each replicated operator
					for (int preceding_vertex : graphToReplicate.getPredecessors(vertex1)) {

						if (reconfigure.getOperatorsType().get(preceding_vertex) == ModuleType.UNION) {

							reconfigure.getApplication().addTupleMapping(rep_vertex1_name, sourceName, rep_vertex1_name,
									new FractionalSelectivity(1.0));

						} else {

							String preceding_vertex_name = reconfigure.getReplicatedGraphToApplication().get(F_j)
									.get(preceding_vertex);
							double selectivity = reconfigure.getOperatorsSelectivity().get(vertex1);
							reconfigure.getApplication().addTupleMapping(rep_vertex1_name, preceding_vertex_name,
									rep_vertex1_name, new FractionalSelectivity(selectivity));
						}
					}

				}

			}

		}
		
	}

	public void removeSourceFromInitialGraph(Graph graphToReplicate, int S_j) {

		int vertex_source = S_j;
		String vertex_source_name = reconfigure.getGraphToApplication().get(S_j);
		

		for (int next_vertex : graphToReplicate.getSuccessors(vertex_source)) {

			String next_vertex_name = reconfigure.getGraphToApplication().get(next_vertex);
			reconfigure.getApplication().getModuleByName(next_vertex_name).getSelectivityMap()
					.remove(new Pair<String, String>(vertex_source_name, next_vertex_name));
		}
		//reconfigure.getApplication().getEdgeMap().remove(vertex_source_name);
				//reconfigure.removeAppEdge(vertex_source_name);

	}

	public void connectReplicatedGraphToInitialGraph(Graph graphToReplicate, int S_j){
		
		int F_j = reconfigure.getSourcesToResourceNodes().get(S_j);
		int vertex_source = S_j;
		String vertex_source_name = reconfigure.getGraphToApplication().get(vertex_source);
		int vertex_sink = graphToReplicate.getSink();
		String vertex_sink_name = reconfigure.getGraphToApplication().get(vertex_sink);
		
		
		System.out.println("vertex_sink="+vertex_sink+"  F_j="+CloudSim.getEntityName(F_j)+" "+reconfigure.getReplicatedGraphToApplication().get(F_j));
		
		String rep_vertex_sink_name = reconfigure.getReplicatedGraphToApplication().get(F_j).get(vertex_sink);
		double data_stream_rate = 0;
		
		
		for(int vertex_succ : reconfigure.getInitialGraph().getSuccessors(vertex_sink)){
			
			String vertex_succ_name = reconfigure.getGraphToApplication().get(vertex_succ);
			if(reconfigure.getOperatorsType().get(vertex_succ) == ModuleType.UNION){
				
				System.out.println("Update edge between the replicated application and the initial application");
				// Update edge between the replicated application and the initial application
				int count = reconfigure.getCountConnectedReplicatedGraphs().get(vertex_succ);
				count = count + 1;
				reconfigure.getCountConnectedReplicatedGraphs().put(vertex_succ, count);
				for(int vertex_old_succ: graphToReplicate.getSuccessors(vertex_sink)) {
					
					data_stream_rate = graphToReplicate.getEdgeWeight(vertex_sink, vertex_old_succ);
					graphToReplicate.removeEdge(vertex_sink, vertex_old_succ);
				}
			
				String rep_vertex_union_name = reconfigure.getGraphToApplication().get(vertex_succ);
				graphToReplicate.addEdge(vertex_sink, vertex_succ, data_stream_rate);
				reconfigure.getApplication().addAppEdge(rep_vertex_sink_name, rep_vertex_union_name, 2, data_stream_rate, rep_vertex_sink_name, Tuple.UP, AppEdge.SENSOR);
				double selectivity = reconfigure.getOperatorsSelectivity().get(vertex_succ);
				reconfigure.getApplication().addTupleMapping(rep_vertex_union_name, rep_vertex_sink_name, rep_vertex_union_name, new FractionalSelectivity(selectivity));
				AppEdge appEdge = reconfigure.getApplication().getEdgeMap().get(vertex_sink_name);
				reconfigure.getApplication().getEdgeMap().get(vertex_sink_name).setTupleNwLength(appEdge.getTupleNwLength() - data_stream_rate);
				
			}else{
				
				System.out.println("Remove exiting edge before introducing the vertex");
				// Remove exiting edge before introducing the vertex
				AppEdge appEdge = reconfigure.getApplication().getEdgeMap().get(vertex_sink_name);
				reconfigure.getInitialGraph().removeEdge(vertex_sink, vertex_succ);
				reconfigure.removeAppEdge(vertex_sink_name);
				AppModule appModule = reconfigure.getApplication().getModuleByName(vertex_sink_name);
				
				
				for(int i = 0; i < appModule.getSelectivityMap().size(); i++){
				    
					//lista.get(i).action();
					appModule.getSelectivityMap().remove(i);
				}
				
				/*for(Pair<String, String> key : appModule.getSelectivityMap().keySet()){
					
					appModule.getSelectivityMap().remove(key);
				}*/
				
			
				// Insert the new vertex, application modules and the corresponding edges and application edges
				
				String rep_vertex_union_name = "Union_"+F_j;
				if(reconfigure.getApplicationToGraph().containsKey(rep_vertex_union_name)) {
					
					rep_vertex_union_name = rep_vertex_union_name+"_"+vertex_succ;
				}
				int vertex_union = reconfigure.getInitialGraph().getAdjVertices().size()+1;
				while(reconfigure.getInitialGraph().getAdjVertices().containsKey(vertex_union)){
					
					vertex_union = vertex_union+1;
				}
				reconfigure.getInitialGraph().addVertex(vertex_union);
				data_stream_rate = appEdge.getTupleNwLength();
				reconfigure.getApplication().addAppModule(rep_vertex_union_name, (int)data_stream_rate, ModuleType.UNION,true,1.0, 1.0);
				reconfigure.addOperatorToMappingModel(CloudSim.getEntityId("cloud"), rep_vertex_union_name);
				reconfigure.getApplicationToGraph().put(rep_vertex_union_name,vertex_union);
				reconfigure.getGraphToApplication().put(vertex_union, rep_vertex_union_name);
				reconfigure.getOperatorsType().put(vertex_union, ModuleType.UNION);
				reconfigure.getOperatorsSelectivity().put(vertex_union, 1.0);
				reconfigure.getOperatorsReplicability().put(vertex_union, true);
				reconfigure.getCountConnectedReplicatedGraphs().put(vertex_union, 1);
				reconfigure.getInitialGraph().addEdge(vertex_sink, vertex_union);
				data_stream_rate = appEdge.getTupleNwLength() - graphToReplicate.getEdgeWeight(vertex_sink, vertex_succ);
				if(data_stream_rate < 0) {
					
					data_stream_rate = 0.0;
				}
				reconfigure.getApplication().addAppEdge(vertex_sink_name, rep_vertex_union_name, 2, data_stream_rate, vertex_sink_name, Tuple.UP, AppEdge.SENSOR);
				reconfigure.getApplication().addTupleMapping(rep_vertex_union_name, vertex_sink_name, rep_vertex_union_name, new FractionalSelectivity(1.0));
				
				data_stream_rate = appEdge.getTupleNwLength();
				reconfigure.getInitialGraph().addEdge(vertex_union, vertex_succ);
				reconfigure.getApplication().addAppEdge(rep_vertex_union_name,vertex_succ_name, 2, data_stream_rate, rep_vertex_union_name, Tuple.UP, AppEdge.SENSOR);
				double selectivity = reconfigure.getOperatorsSelectivity().get(vertex_succ);
				reconfigure.getApplication().addTupleMapping(vertex_succ_name, rep_vertex_union_name, vertex_succ_name, new FractionalSelectivity(selectivity));
				
				
				// Update edge between the replicated application and the initial application
				data_stream_rate = graphToReplicate.getEdgeWeight(vertex_sink, vertex_succ);
				graphToReplicate.removeEdge(vertex_sink, vertex_succ);
				graphToReplicate.addEdge(vertex_sink, vertex_union, data_stream_rate);
				reconfigure.getApplication().addAppEdge(rep_vertex_sink_name, rep_vertex_union_name, 2, data_stream_rate, rep_vertex_sink_name, Tuple.UP, AppEdge.SENSOR);
				reconfigure.getApplication().addTupleMapping(rep_vertex_union_name, rep_vertex_sink_name, rep_vertex_union_name, new FractionalSelectivity(selectivity));
			}
			
		}
       
	}
}
