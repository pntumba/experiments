package org.fog.placement.proposition.experiments.test;

import java.util.Random;

public class Equilibrium {

	public static void main( String[] args ){
		
		
		Random rand = new Random();
		double max_sum_csel_cost = 4.0260928;
		double max_csel = 0.0225792;
		int max_data_rate = 1536;
		int min_data_rate = 1400;
		double diff = Double.MAX_VALUE;
		double cmu = 0;
		double Bmax = 0;
		int count = 0;
		while(count < Integer.MAX_VALUE) {
			
			count++;
			double cmu_value = rand.nextInt(max_data_rate-min_data_rate) + min_data_rate;
			double Bmax_value = rand.nextInt(max_data_rate-min_data_rate) + min_data_rate;
			double cmu_ratio = max_sum_csel_cost/(4*cmu_value);
			double nbu_ratio = max_csel/Bmax_value;
			
			double diff_tmp = cmu_ratio - nbu_ratio;
			System.out.println("["+count+"] Generate equilibrium points:");
			System.out.print("\tdiff="+diff_tmp);
			System.out.print("\tcmu="+cmu_value);
			System.out.println("\tBmax="+Bmax_value);
			if(diff > cmu_ratio - nbu_ratio) {
				
				diff = cmu_ratio - nbu_ratio;
				cmu = cmu_value;
				Bmax = Bmax_value;
				System.out.println("\nAccpted Equilibrium points:");
				System.out.print("\tdiff="+diff);
				System.out.print("\tcmu="+cmu);
				System.out.println("\tBmax="+Bmax);
				
			}

		}
		System.out.println("\nBest equilibrium points:");
		System.out.print("\tdiff="+diff);
		System.out.print("\tcmu="+cmu);
		System.out.println("\tBmax="+Bmax);
	}
}
