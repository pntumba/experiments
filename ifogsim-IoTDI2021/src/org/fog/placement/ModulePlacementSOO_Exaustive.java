package org.fog.placement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppEdge;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.entities.Actuator;
import org.fog.entities.FogDevice;
import org.fog.entities.Sensor;
import org.fog.placement.proposition.algorithms.SOO_Exaustive;
import org.fog.placement.proposition.configuration.Configuration;
import org.fog.placement.proposition.utils.ALGORITHM;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.placement.proposition.utils.Triggers;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ModulePlacementSOO_Exaustive extends ModulePlacement {

	private List<Sensor> sensors;
	private List<Actuator> actuators;
    private Map<String, List<String>> mappingModel;
    private Map<Integer, List<String>> removingOperatorsModel;
	protected Map<Integer, Double> currentCpuLoad;
	protected Map<String, Integer> sensorsAssociated;
	private double bandwidthUpperThreshold;
	private double bandwidthLowerThreshold;
	private double bandwidthUsage;
	private int cloudId;
	private Configuration reconfigure;
	private Graph graph;
	private SOO_Exaustive algorithmique; 
	private Map<Integer, Double> streamsSet = null;
	private Map<Integer, Graph> graphMapping = null;
	
	public ModulePlacementSOO_Exaustive(List<FogDevice> fogDevices, List<Sensor> sensors, List<Actuator> actuators,
			Application application, double bandwidthUpperThreshold, double bandwidthLowerThreshold){
		
		this.setUsedAlgorithm(ALGORITHM.SOO);
		this.setFogDevices(fogDevices);
		this.setApplication(application);
		this.setSensors(sensors);
		this.setActuators(actuators);
		this.setModuleToDeviceMap(new HashMap<String, List<Integer>>());
		this.setDeviceToModuleMap(new HashMap<Integer, List<AppModule>>());
		this.setModuleInstanceCountMap(new HashMap<Integer, Map<String, Integer>>());
		this.setModulesScheduled(new HashMap<Integer, List<String>>());
		this.bandwidthUpperThreshold = bandwidthUpperThreshold;
		sensorsAssociated = new HashMap<String, Integer>();
		currentCpuLoad = new HashMap<Integer, Double>();
		this.cloudId = CloudSim.getEntityId("cloud");
		
		reconfigure = new Configuration(getFogDevices(), getSensors(), getActuators());
		reconfigure.setApplication(getApplication());
		reconfigure.convertApplicationToGraph(getApplication());
		graph = reconfigure.getInitialGraph();
		algorithmique = new SOO_Exaustive(graph, reconfigure.getOperatorsType(), reconfigure.getOperatorsSelectivity(), reconfigure.getOperatorCosts(),reconfigure.getOperatorsReplicability());
		mapModules();
		
	}
	
	@Override
	protected void mapModules(){
		// TODO Auto-generated method stub
		dynamicMapModules(Triggers.MIGRATE_TOFOG, 1.0, 1.0, reconfigure.getInitialDataStreams()); 		
	}

	
	protected void dynamicMapModules(Triggers triggers, double fog_cloud_netw_delay, double edge_fog_netw_delay, Map<Integer, Double> streamsSet) {
		// TODO Auto-generated method stub
		
		switch (triggers){
		
		case MIGRATE_TOFOG:
			
			 if(StaticVariables.jsonObject == null) {
				 
				 StaticVariables.jsonObject = new JSONObject();
			 }
			 
			 
			algorithmique.setSourcesToResourceNodes(reconfigure.getSourcesToResourceNodes());
			algorithmique.setW_c(1.0);
			algorithmique.setW_n(1.0);
			algorithmique.initialConfiguration(fog_cloud_netw_delay, edge_fog_netw_delay, bandwidthUpperThreshold, streamsSet);
			JSONArray intitialConfiguration = new JSONArray();
			for(int S_j : algorithmique.getInitialMapping().keySet()) {
		    	
				System.out.println("Initial mapping ["+S_j+"] "+algorithmique.getInitialMapping().get(S_j).getAdjVertices());
		    	CloudSim.getEntityName(S_j);
		    	JSONObject initial = new JSONObject();
		    	initial.put(CloudSim.getEntityName(S_j), algorithmique.getInitialMapping().get(S_j).getAdjVertices().toString());
		    	intitialConfiguration.add(initial);
		    }
			StaticVariables.jsonObject.put("initial_configuration", intitialConfiguration);
		    
		    algorithmique.adjustConfiguration(fog_cloud_netw_delay, edge_fog_netw_delay, bandwidthUpperThreshold, streamsSet);
		    JSONArray adjustConfiguration = new JSONArray(); 
		    for(int S_j : algorithmique.getInitialMapping().keySet()) {
		    	
		    	System.out.println("Adjusted mapping ["+S_j+"] "+algorithmique.getInitialMapping().get(S_j).getAdjVertices());
		    	CloudSim.getEntityName(S_j);
		    	 JSONObject adjust = new JSONObject();
		    	 adjust.put(CloudSim.getEntityName(S_j), algorithmique.getInitialMapping().get(S_j).getAdjVertices().toString());
		    	 adjustConfiguration.add(adjust);
		    }
		    StaticVariables.jsonObject.put("adjusted_configuration", adjustConfiguration);
		    
		  
		 
			for(AppModule module : getApplication().getModules()){
				
				reconfigure.addOperatorToMappingModel(cloudId, module.getName());
				
			}
			
			reconfigure.reconfigureApplicationTodeployOnFog(algorithmique.getInitialMapping());
			implementMappingModel(reconfigure.getMappingModel());
			
		break;
		
		}
		
		
	}

	public void implementMappingModel(Map<Integer, List<String>> mappingModel) {
		
		System.out.println("mappingModel "+mappingModel);
		
		for (int device_id : mappingModel.keySet()) {

			for (String moduleName : mappingModel.get(device_id)) {

				AppModule module = getApplication().getModuleByName(moduleName);
				FogDevice device = getDeviceById(device_id);
				
				// Todo Compute CPU load and memory
				createModuleInstanceOnDevice(module, device);
				
			}
		}
		
		
	}
	
	public void implementRemovingModel(Map<Integer, List<String>> removingModel) {
		
		for (int device_id : removingModel.keySet()) {
			
			System.out.println("Removing operators on "+CloudSim.getEntity(device_id).getName()+ ":"+ removingModel.get(device_id));
			for (String moduleName : removingModel.get(device_id)) {

				AppModule module = getApplication().getModuleByName(moduleName);
				FogDevice device = getDeviceById(device_id);
				removeModuleInstanceOnDevice(module, device);
				reconfigure.removeAppModule(moduleName);
				
			}
		}
		setRemovingOperatorsModel(removingModel);
	}
	/**
	 * Gets all sensors associated with fog-device <b>device</b>
	 * 
	 * @param device
	 * @return map from sensor type to number of such sensors
	 */
	private void setAssociatedSensors(FogDevice device) {
		Map<String, Integer> endpoints = new HashMap<String, Integer>();
		for (Sensor sensor : getSensors()){
			if (sensor.getGatewayDeviceId() == device.getId()) {
				if (!sensorsAssociated.containsKey(sensor.getTupleType()))
					sensorsAssociated.put(sensor.getTupleType(), 0);
				sensorsAssociated.put(sensor.getTupleType(), sensorsAssociated.get(sensor.getTupleType()) + 1);
			}
		}
	}

	protected double getRateOfSensor(String sensorType) {

		for (Sensor sensor : getSensors()) {
			if (sensor.getTupleType().equals(sensorType))
				return 1 / sensor.getTransmitDistribution().getMeanInterTransmitTime();
		}
		return 0;
	}

	private Map<Integer, Double> mapDataStreamSourceToGraph(Map<Integer, Double> input_streamsSet){
		System.out.println("reconfigure.getApplicationToGraph() "+reconfigure.getApplicationToGraph());
		Map<Integer, Double> streamsSet = new ConcurrentHashMap<Integer, Double>();
		for(Sensor sensor: getSensors()) {
			
			if(input_streamsSet.containsKey(sensor.getGatewayDeviceId())) {
				
				
				int S_j = reconfigure.getApplicationToGraph().get(sensor.getName());
				double data_stream = input_streamsSet.get(sensor.getGatewayDeviceId());
				streamsSet.put(S_j, data_stream);
			}
			
		}
		return streamsSet;
	}

	public List<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(List<Sensor> sensors) {
		this.sensors = sensors;
	}

	public List<Actuator> getActuators() {
		return actuators;
	}

	public void setActuators(List<Actuator> actuators) {
		this.actuators = actuators;
	}

	public double getBandwidthUsage() {
		return bandwidthUsage;
	}

	public void setBandwidthUsage(double bandwidthUsage) {
		this.bandwidthUsage = bandwidthUsage;
	}

	public double getBandwidthLowerThreshold() {
		return bandwidthLowerThreshold;
	}

	public void setBandwidthLowerThreshold(double bandwidthLowerThreshold) {
		this.bandwidthLowerThreshold = bandwidthLowerThreshold;
	}

	public Map<Integer, List<String>> getRemovingOperatorsModel() {
		return removingOperatorsModel;
	}

	public void setRemovingOperatorsModel(Map<Integer, List<String>> removingOperatorsModel) {
		this.removingOperatorsModel = removingOperatorsModel;
	}

	
}
