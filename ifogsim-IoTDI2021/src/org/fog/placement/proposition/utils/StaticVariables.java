package org.fog.placement.proposition.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONObject;

public class StaticVariables {


	/////////////////////////////////////
	public static int operator_number=20; 
	public static int source_number=20; 
	public static double min_data_rate=1.0;
	public static double max_data_rate=256.0; 
	public static double min_selectivity=0.3; 
	public static double max_selectivity=1.5;
	public static double min_operator_cost=1.0; 
	public static double max_operator_cost=1.5;
	public static double Buthr=7250.0;
	public static double Blthr=4000.0;
	//public static String problem_size = "";
	public static String problem_size = "_scale_"+source_number;
	
	///////////////////////////////////
	
	public static String input_directory = "src/org/fog/placement/proposition/experiments/input/";
	public static String dataPointFile = "src/org/fog/placement/proposition/experiments/input/datapoints"+problem_size+".json";
	public static String physical_topology = "src/org/fog/placement/proposition/experiments/topology/UR_topology"+problem_size+".json";
	public static String application_topology = "src/org/fog/placement/proposition/experiments/topology/application_topology.json";
	public static String custom_application = "src/org/fog/placement/proposition/experiments/input/custom_application.json";
	public static String executionLogFile = "src/org/fog/placement/proposition/experiments/output/executionLogFile.json";
	
	
	public static String input_graph = "src/org/fog/placement/proposition/experiments/input/graph";
	public static String output_result = "src/org/fog/placement/proposition/experiments/output/Experiment";
	public static String previous_application_topology = "src/org/fog/placement/proposition/experiments/topology/previous";
	public static String experiment = null;
	public static double data_stream_size = 0.0;
	///////////////////////////////////
	
	public static double cru = 0.0;
	public static double nru = 0.0;
	public static double B = 0.0;
	public static double CRU = 0.0;
	public static double NRU = 0.0;
	public static double RU = 0.0;
	public static double RT = 0.0;
	public static int counter = 0;
	
	public static Map<Integer, Double> fog_comp_resource_usage_cost = new ConcurrentHashMap<Integer, Double>();
	public static Map<Integer, Double> exeedAvailableComputationalResources = new ConcurrentHashMap<Integer, Double>();
	public static boolean triggerByAdjustEdgeCut = false;
	
	public static long reconfigurationTime = 0l;
	
	
	public static JSONObject jsonObject = null;
	
	
	
}
