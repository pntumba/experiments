package org.fog.placement.proposition.utils;

public enum ModuleType {

	JOIN,
	UNION,
	SINGLE_INPUT_EDGE, /* Modules of type filter, aggregation, etc.*/
	MULTIPLE_OUTPUT_EDGES /* Modules of type split, etc.*/
}
