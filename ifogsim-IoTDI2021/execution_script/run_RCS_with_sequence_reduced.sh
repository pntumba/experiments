#!/bin/bash

export MAVEN_OPTS="-Xmx12G"
#rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json
total_data_rates=(2256 700 512 256 1792 1500 2400 965 1221 2000 512 2256 2400 256 1792 1500 700 2000 1221 965 256 2256 2000 2400 1792 512 700 1221 965 1500 256 2400 965 1221 700 1500 2256 1792 512 2000 2256 512 1792 2000 1500 256 2400 700 1221 965 1792 2000 256 2400 700 965 1221 512 1500 2256 965 1500 512 2256 1792 256 2400 1221 700 2000 1500 1221 2400 700 2000 1792 965 256 2256 512 965 700 1221 1792 256 512 1500 2256 2000 2400 1500 700 512 2000 1792 1221 2400 2256 256 965 256 2400 2000 2256 700 1792 965 1221 1500 512 965 700 2256 512 1500 2000 1792 1221 256 2400 965 1500 256 2256 700 1221 512 2400 1792 2000 256 700 965 512 2256 1792 2000 1500 1221 2400 2000 2256 1221 256 1792 2400 700 965 512 1500)
execution_counter=136
for i in {136..150..1}
do	
			
				total_data_rate=${total_data_rates[$i]}
				let execution_counter=execution_counter+1
				if [[ $i -gt 0 ]]
				then

					mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS_UR 8 $total_data_rate $execution_counter y"
					
				else
					mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS_UR 8 $total_data_rate $execution_counter n"
				fi
				echo "Looping for RCS[$execution_counter]: data stream=$total_data_rate, iteration=$i has finished!" 
				sleep 10
done

