package org.fog.placement.proposition.utils;

public class MinMax {

	private double min;
	private double max;
	
	public MinMax(double min, double max) {
		
		this.setMin(min);
		this.setMax(max);
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}
	
	public String toString() {
		
		return String.valueOf("min="+min+", max="+max);
	}
}
