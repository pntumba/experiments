package org.fog.placement.proposition.configuration;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.entities.FogDevice;
import org.fog.entities.Sensor;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.placement.proposition.utils.UtilFunctions;
import org.fog.utils.JsonToApplication;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ApplicationState {

	private static Configuration config;

	public static void setConfig(Configuration config) {

		ApplicationState.config = config;
	}

	public static void backupStaticGraph(Application application) {

		JSONObject app = JsonToApplication.applicationToJson(application);

		JSONObject appDoc = JsonToApplication.convertJsonFileToObject(StaticVariables.application_topology);

		appDoc.put("application_state", app);

		JSONObject staticGraphObj = new JSONObject();
		JSONArray verticesObject = new JSONArray();
		JSONArray edgesObject = new JSONArray();

		for (int vertex : config.getStaticGraph().getAdjVertices().keySet()) {

			JSONObject edgeObject = new JSONObject();
			JSONObject vertexObject = new JSONObject();
			String vertex_name = config.getGraphToApplication().get(vertex);
			vertexObject.put(vertex_name, vertex);
			verticesObject.add(vertexObject);

			for (int next_vertex : config.getStaticGraph().getAdjVertices().get(vertex).keySet()) {

				edgeObject.put("source", vertex);
				edgeObject.put("destination", next_vertex);
				double weight = config.getStaticGraph().getAdjVertices().get(vertex).get(next_vertex);
				edgeObject.put("weight", weight);
				edgesObject.add(edgeObject);
			}
		}
		staticGraphObj.put("vertices", verticesObject);
		staticGraphObj.put("edges", edgesObject);
		appDoc.put("static_graph", staticGraphObj);
		JsonToApplication.generateJsonFile(appDoc, StaticVariables.application_topology);
	}

	public static void backupEvolvingGraph() {

		JSONObject appDoc = JsonToApplication.convertJsonFileToObject(StaticVariables.application_topology);
		JSONObject evolvingGraphObj = new JSONObject();
		JSONArray verticesObject = new JSONArray();
		JSONArray edgesObject = new JSONArray();
		
		
		System.out.println("Evolving graph on Cloud: "+config.getInitialGraph().getAdjVertices());
		for (int vertex : config.getInitialGraph().getAdjVertices().keySet()) {

			JSONObject edgeObject = new JSONObject();
			JSONObject vertexObject = new JSONObject();
			String vertex_name = config.getGraphToApplication().get(vertex);
			vertexObject.put(vertex_name, vertex);
			verticesObject.add(vertexObject);

			for (int next_vertex : config.getInitialGraph().getAdjVertices().get(vertex).keySet()) {

				edgeObject.put("source", vertex);
				edgeObject.put("destination", next_vertex);
				double weight = config.getInitialGraph().getAdjVertices().get(vertex).get(next_vertex);
				edgeObject.put("weight", weight);
				edgesObject.add(edgeObject);
			}

		}
		evolvingGraphObj.put("vertices", verticesObject);
		evolvingGraphObj.put("edges", edgesObject);
		appDoc.put("evolving_graph", evolvingGraphObj);

		JSONArray replicatedGraphPerConnector = new JSONArray();
		for (int vertex : config.getCountConnectedReplicatedGraphs().keySet()) {

			JSONObject connector = new JSONObject();
			int count = config.getCountConnectedReplicatedGraphs().get(vertex);
			connector.put(vertex, count);
			replicatedGraphPerConnector.add(connector);
		}
		appDoc.put("replicated_graph_per_connector", replicatedGraphPerConnector);

		JSONObject replicatedGraphToApp = new JSONObject();
		if (appDoc.containsKey("replicated_graph_to_application")){

			appDoc.remove("replicated_graph_to_application");
		}
		for (int F_j : config.getReplicatedGraphToApplication().keySet()) {

			JSONArray vertices_to_application = new JSONArray();
			for (int vertex : config.getReplicatedGraphToApplication().get(F_j).keySet()) {

				JSONObject obj = new JSONObject();
				String vertex_name = config.getReplicatedGraphToApplication().get(F_j).get(vertex);
				obj.put(vertex_name, vertex);
				vertices_to_application.add(obj);
			}
			String fog_node_name = CloudSim.getEntityName(F_j);
			replicatedGraphToApp.put(fog_node_name, vertices_to_application);
		}
		appDoc.put("replicated_graph_to_application", replicatedGraphToApp);

		JSONArray csel_for_migrated_streams = new JSONArray();
		if (appDoc.containsKey("csel_for_migrated_streams")) {

			appDoc.remove("csel_for_migrated_streams");
		}
		for (int S_j : config.getCumulatedSelectivitiesForMigratedSreams().keySet()) {

			String source_name = config.getGraphToApplication().get(S_j);
			double csel = config.getCumulatedSelectivitiesForMigratedSreams().get(S_j);
			JSONObject cselObject = new JSONObject();
			cselObject.put(source_name, csel);
			csel_for_migrated_streams.add(cselObject);
		}
		appDoc.put("csel_for_migrated_streams", csel_for_migrated_streams);

		

			for (Sensor sensor : config.getSensors()) {

				String fog_name = CloudSim.getEntityName(sensor.getGatewayDeviceId());
				if (appDoc.containsKey("replicated_graph_" + fog_name)) {

					appDoc.remove("replicated_graph_" + fog_name);
				}
			}

			for (int F_j : config.getEffectiveMapping().keySet()) {

				Graph replicatedGraph = config.getEffectiveMapping().get(F_j);
				String fog_name = CloudSim.getEntityName(F_j);
				System.out.println("Graph replicated on "+fog_name+": "+ replicatedGraph.getAdjVertices());
				JSONObject replicatedGraphObj = new JSONObject();
				JSONArray replicatedVerticesObject = new JSONArray();
				JSONArray replicatedEdgesObject = new JSONArray();
				
				for (int vertex : replicatedGraph.getAdjVertices().keySet()){

					JSONObject replicatedvertexObject = new JSONObject();
					String vertex_name = null;
					
					if(config.getReplicatedGraphToApplication().get(F_j).containsKey(vertex)){

						vertex_name = config.getReplicatedGraphToApplication().get(F_j).get(vertex);

					} else if (config.getGraphToApplication().containsKey(vertex)) {

						vertex_name = config.getGraphToApplication().get(vertex);
					}
					replicatedvertexObject.put(vertex_name, vertex);
					replicatedVerticesObject.add(replicatedvertexObject);

					for (int next_vertex : replicatedGraph.getAdjVertices().get(vertex).keySet()) {

						JSONObject replicatededgeObject = new JSONObject();
						replicatededgeObject.put("source", vertex);
						replicatededgeObject.put("destination", next_vertex);
						double weight = replicatedGraph.getAdjVertices().get(vertex).get(next_vertex);
						replicatededgeObject.put("weight", weight);
						replicatedEdgesObject.add(replicatededgeObject);
					}
				}
				replicatedGraphObj.put("vertices", replicatedVerticesObject);
				replicatedGraphObj.put("edges", replicatedEdgesObject);
				appDoc.put("replicated_graph_" + fog_name, replicatedGraphObj);
			}

		JsonToApplication.generateJsonFile(appDoc, StaticVariables.application_topology);
	}

	public static void loadEvolvingGraphState() {

		JSONObject appDoc = JsonToApplication.convertJsonFileToObject(StaticVariables.application_topology);
		Graph graph = loadGraphObjectState(appDoc, "evolving_graph");
		config.setInitialGraph(graph);
		
		/*
		for (Sensor sensor : config.getSensors()) {

			int F_j = sensor.getGatewayDeviceId();
			String fog_name = CloudSim.getEntityName(F_j);
			String key = "replicated_graph_" + fog_name;
			if (appDoc.containsKey(key)) {

				Graph replicated_graph = loadGraphObjectState(appDoc, key);
				config.getEffectiveMapping().put(F_j, replicated_graph);
			}
		}*/

		JSONArray replicatedGraphPerConnector = (JSONArray) appDoc.get("replicated_graph_per_connector");
		if (!replicatedGraphPerConnector.isEmpty()) {

			for (int i = 0; i < replicatedGraphPerConnector.size(); i++) {

				JSONObject connector = (JSONObject) replicatedGraphPerConnector.get(i);
				for (int vertex : graph.getAdjVertices().keySet()) {

					if (connector.containsKey("" + vertex)) {

						int count = (int) (new BigDecimal((long) (connector.get("" + vertex))).longValue());
						config.getCountConnectedReplicatedGraphs().put(vertex, count);

					}
				}

			}
		}

		JSONObject replicatedGraphToApp = (JSONObject) appDoc.get("replicated_graph_to_application");
		if (!replicatedGraphToApp.isEmpty()) {

			for (Sensor sensor : config.getSensors()) {

				String fog_node_name = CloudSim.getEntityName(sensor.getGatewayDeviceId());
				if (replicatedGraphToApp.containsKey(fog_node_name)) {

					JSONArray vertices_to_application = (JSONArray) replicatedGraphToApp.get(fog_node_name);
					Map<Integer, String> vertex_to_application = new ConcurrentHashMap<Integer, String>();
					for (int i = 0; i < vertices_to_application.size(); i++) {

						JSONObject obj = (JSONObject) vertices_to_application.get(i);
						for (AppModule module : config.getApplication().getModules()) {

							if (obj.containsKey(module.getName())) {

								String rep_vertex_name = module.getName();
								int rep_vertex = (int) (new BigDecimal((long) (obj.get(rep_vertex_name))).longValue());
								vertex_to_application.put(rep_vertex, rep_vertex_name);
							}
						}
					}
					config.getReplicatedGraphToApplication().put(sensor.getGatewayDeviceId(), vertex_to_application);
				}

			}
		}

		for (AppModule module : config.getApplication().getModules()) {

			if (config.getApplicationToGraph().containsKey(module.getName())) {

				int vertex = config.getApplicationToGraph().get(module.getName());
				config.getOperatorsReplicability().put(vertex, module.isLocallyReplicable());
				config.getOperatorsSelectivity().put(vertex, module.getSelectivity());
				config.getOperatorCosts().put(vertex, module.getOperatorCost());
				config.getOperatorsType().put(vertex, module.getType());
			}

			for (int F_j : config.getReplicatedGraphToApplication().keySet()) {

				for (int vertex : config.getReplicatedGraphToApplication().get(F_j).keySet()) {

					if (module.getName().equals(config.getReplicatedGraphToApplication().get(F_j).get(vertex))) {

						config.getOperatorsReplicability().put(vertex, module.isLocallyReplicable());
						config.getOperatorsSelectivity().put(vertex, module.getSelectivity());
						config.getOperatorCosts().put(vertex, module.getOperatorCost());
						config.getOperatorsType().put(vertex, module.getType());
					}
				}
			}

		}

		for (Sensor sensor : config.getSensors()) {

			if (config.getApplicationToGraph().containsKey(sensor.getName())) {

				int sourceVertex = config.getApplicationToGraph().get(sensor.getName());
				config.getOperatorsSelectivity().put(sourceVertex, 1.0);
				config.getOperatorsReplicability().put(sourceVertex, true);
				config.getSourcesToResourceNodes().put(sourceVertex, sensor.getGatewayDeviceId());
				config.getResourceNodeToSource().put(sensor.getGatewayDeviceId(), sourceVertex);
			}

		}
		
		JSONArray csel_for_migrated_streams = (JSONArray) appDoc.get("csel_for_migrated_streams");
		if (!csel_for_migrated_streams.isEmpty()) {
			for (int i = 0; i < csel_for_migrated_streams.size(); i++){

				JSONObject cselObject = (JSONObject) csel_for_migrated_streams.get(i);

				for (Sensor sensor : config.getSensors()) {

					if (cselObject.containsKey(sensor.getName())) {

						double csel = (double) (new BigDecimal((double) (cselObject.get(sensor.getName()))).doubleValue());
						int S_j = config.getApplicationToGraph().get(sensor.getName());
						config.getCumulatedSelectivitiesForMigratedSreams().put(S_j, csel);
						
						int F_j = sensor.getGatewayDeviceId();
						double data_stream_rate = config.getApplication().getEdgeMap().get(sensor.getName()).getTupleNwLength();
						String fog_name = CloudSim.getEntityName(F_j);
						String key = "replicated_graph_" + fog_name;
						if (appDoc.containsKey(key)) {
							
							FogDevice fog_node = (FogDevice) CloudSim.getEntity(F_j);
							Graph replicated_graph = loadGraphObjectState(appDoc, key);
							int cm_F_j = fog_node.getHost().getRamProvisioner().getAvailableRam();
							UtilFunctions.setAvailableMemory(cm_F_j*10);
							
							UtilFunctions.identifyGraphSatisfyingFogNodeResource(replicated_graph, config.getOperatorsType(), config.getOperatorsSelectivity(), config.getOperatorCosts(), S_j, data_stream_rate);
							config.getEffectiveMapping().put(F_j, UtilFunctions.getGraphSatRequirement());
							System.out.println("Checking "+UtilFunctions.getRequiredMemory()+" > "+cm_F_j);
							if(UtilFunctions.getRequiredMemory() > cm_F_j) {
								
								StaticVariables.exeedAvailableComputationalResources.put(S_j, UtilFunctions.getRequiredMemory());
							}
						}
					}
				}
			}
		}
		//System.exit(0);

	}

	public static void loadStaticGraphState() {

		JSONObject appDoc = JsonToApplication.convertJsonFileToObject(StaticVariables.application_topology);
		Graph graph = loadGraphObjectState(appDoc, "static_graph");
		config.setStaticGraph(graph);
	}

	private static Graph loadGraphObjectState(JSONObject appDoc, String type) {

		JSONObject graphObject = (JSONObject) appDoc.get(type);
		JSONArray verticesObject = (JSONArray) graphObject.get("vertices");
		JSONArray edgesObject = (JSONArray) graphObject.get("edges");
		Graph graph = new Graph();
		
		for (int i = 0; i < verticesObject.size(); i++){

			JSONObject vertexObject = (JSONObject) verticesObject.get(i);
			for (AppModule module : config.getApplication().getModules()) {
                
				
				if (vertexObject.containsKey(module.getName())) {
					
					int vertex = (int) (new BigDecimal((long) (vertexObject.get(module.getName()))).longValue());
					graph.addVertex(vertex);
					if (type.equals("evolving_graph")) {

						config.getApplicationToGraph().put(module.getName(), vertex);
						config.getGraphToApplication().put(vertex, module.getName());
					}
				}
			}

			for (Sensor sensor : config.getSensors()) {

				if (vertexObject.containsKey(sensor.getName())) {

					int vertex = (int) (new BigDecimal((long) (vertexObject.get(sensor.getName()))).longValue());
					graph.addVertex(vertex);
					if (type.equals("evolving_graph")) {

						config.getApplicationToGraph().put(sensor.getName(), vertex);
						config.getGraphToApplication().put(vertex, sensor.getName());
					}
				}
			}

		}
		
		

		for (int j = 0; j < edgesObject.size(); j++) {

			JSONObject edgeObject = (JSONObject) edgesObject.get(j);
			int vertex1 = (int) (new BigDecimal((long) (edgeObject.get("source"))).longValue());
			int vertex2 = (int) (new BigDecimal((long) (edgeObject.get("destination"))).longValue());
			double weight = (double) (new BigDecimal((double) (edgeObject.get("weight"))).doubleValue());
			if (graph.getAdjVertices().containsKey(vertex1)) {

				graph.addEdge(vertex1, vertex2, weight);
			}
		}
		return graph;
	}
}
