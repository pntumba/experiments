#!/bin/bash
export MAVEN_OPTS="-Xmx12G"

rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json
total_data_rate_sequence=(965 700 1500 512 1221 2000 2256 1792 2100 1792 2100 965 1221 1500 2000 700 512 2256 965 512 2256 700 2000 1221 1500 2100 1792 512 2000 1221 965 2100 1792 700 2256 1500 2100 512 2256 1500 1221 1792 2000 700 965 1792 2100 1500 2000 1221 512 965 700 2256 512 1500 700 965 1792 2100 2256 2000 1221 2100 1792 965 1221 2256 1500 512 700 2000 2000 2100 2256 700 512 1792 1500 1221 965 1221 700 965 2256 1792 2000 512 2100 1500 2256 512 2000 2100 965 700 1792 1221 1500 2100 1221 1792 965 2256 2000 512 700 1500 965 1500 700 1792 2256 2000 2100 512 1221 965 1792 2256 2000 2100 512 1221 700 1500 512 1500 965 700 2000 1792 2256 2100 1221)
execution_counter=0
for i in {0..134..1}
do	
			
				total_data_rate=${total_data_rate_sequence[$i]}
				let execution_counter=execution_counter+1
				if [[ $i -gt 0 ]]
				then

					mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS 10 $total_data_rate $execution_counter y"
					
				else
					
					mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS 10 $total_data_rate $execution_counter s"
				fi
				echo "Looping for RCS[$execution_counter]: data stream=$total_data_rate, iteration=$i has finished!" 
				sleep 5
done

total_data_rates=(512 700 965 1221 1500 1792 2000 2100 2256)
k=0
for j in {0..8..1}
do
	total_data_rate=${total_data_rates[$j]}
	rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json
	execution_counter=1
	for i in {1..15..1}
	do	
			
				mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_SOO_CPLEX" -Dexec.args="SOO_CPLEX 10 $total_data_rate $execution_counter n"
				echo "Looping for SOO_CPLEX[$execution_counter]: data stream=$total_data_rate, iteration=$i has finished!"
				let execution_counter=execution_counter+1 
				sleep 1
	done

	rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json
	execution_counter=1
	k=0
	for i in {1..15..1}
	do	
			
				mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_SOO_DeltaRU" -Dexec.args="SOO_DeltaRU_UR 10 $total_data_rate $execution_counter $k n"
				echo "Looping for SOO_DeltaRU[$execution_counter]: k=$k, data stream=$total_data_rate, iteration=$i has finished!" 
				let execution_counter=execution_counter+1 
				sleep 1
	done
done

