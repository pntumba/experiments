import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [512,700,965,1221,1500,1792,2000,2100,2256]
CRU_SOOCPLEX = [0.2459814453,0.3356770833,0.4526147461,0.5259985352,0.5984928385,0.6839160156,0.7250406901,0.7805314128,0.7629288737]
CRU_RCS = [0,0,0.0006241861979,0.05452067057,0.2195499674,0.3116748047,0.3121638997,0.4823649089,0.5471818034]

fig, ax = plt.subplots()
ax.plot(data_stream_rates, CRU_SOOCPLEX, "--^b", label="SOO-CPLEX", markersize=8, markevery=1)
ax.plot(data_stream_rates, CRU_RCS, "-ok", label="RCS", markersize=8)
ax.legend()
plt.ylabel('Computational resource usage costs- CRU')
plt.xlabel('Data stream rate produced at the Edge (KB/sec)')
plt.show()


