import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [512,700,965,1221,1500,1792,2000,2100,2256]
NRU_SOOCPLEX = [0.01025287356,0.01687356322,0.03940229885,0.119816092,0.2262528736,0.320091954,0.4105287356,0.3987586207,0.5552643678]
NRU_RCS = [0.3531034483,0.4827586207,0.6646436782,0.7725057471,0.756,0.8335172414,0.8424367816,0.8045057471,0.8332413793]

fig, ax = plt.subplots()
ax.plot(data_stream_rates, NRU_SOOCPLEX, "--^b", label="SOO-CPLEX", markersize=8, markevery=1)
ax.plot(data_stream_rates, NRU_RCS, "-ok", label="RCS", markersize=8)
ax.legend()
plt.ylabel('Network resource usage costs- NRU')
plt.xlabel('Data stream rate produced at the Edge (KB/sec)')
plt.show()


