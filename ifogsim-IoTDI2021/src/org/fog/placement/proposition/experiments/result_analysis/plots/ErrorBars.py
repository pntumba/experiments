import matplotlib.pyplot as plt
import numpy as np

data_point_965 = [0,0,0,0,0,0,0,0,0,0]
data_point_1221 = [0,0,0,0,0,0,0,0,0,0]
data_point_1500 = [0,0.02823005722,0,0.05903976545,0.04513127301,0,0,0,0,0,0,0,0.01977444833,0,0]
data_point_1792 = [0.04018458475,0.04785878469,0.04709847192,0,0.04028258064,0,0,0,0,0,0,0,0.04710262284,0,0.05490585441]
data_point_2000 = [0.008361737133,0,0.001922990427,0,0,0.04610744225,0.02322691102,0,0.01801038927,0,0.00544738639,0,0,0,0]
data_point_2256 = [0,0,0,0,0.01588670036,0,0.0341216838,0,0.04123730029,0,0,0.04453757511,0.03404845913,0,0.04409568307]
data_point_2400 = [0,0.03396136171,0,0,0.02031157454,0.05000838416,0,0,0.00544738639,0,0,0,0.02807742836,0,0]
data_point_2656 = [0,0,0.04206503398,0,0,0.0387413365,0,0,0.04453757511,0,0,0,0,0,0]
data_point_2800 = [0,0,0,0,0.03661764759,0,0,0,0,0.05675172576,0.01860725832,0,0,0.02148906376,0]


# Calculate the average
data_point_965_mean = np.mean(data_point_965)
data_point_1221_mean = np.mean(data_point_1221)
data_point_1500_mean = np.mean(data_point_1500)
data_point_1792_mean = np.mean(data_point_1792)
data_point_2000_mean = np.mean(data_point_2000)
data_point_2256_mean = np.mean(data_point_2256)
data_point_2400_mean = np.mean(data_point_2400)
data_point_2656_mean = np.mean(data_point_2656)
data_point_2800_mean = np.mean(data_point_2800)

# Calculate the standard deviation
data_point_965_std = np.std(data_point_965)
data_point_1221_std = np.std(data_point_1221)
data_point_1500_std = np.std(data_point_1500)
data_point_1792_std = np.std(data_point_1792)
data_point_2000_std = np.std(data_point_2000)
data_point_2256_std = np.std(data_point_2256)
data_point_2400_std = np.std(data_point_2400)
data_point_2656_std = np.std(data_point_2656)
data_point_2800_std = np.std(data_point_2800)


# Define labels, positions, bar heights and error bar heights
labels = ['965', '1221', '1500','1792','2000','2256','2400','2656','2800']
x_pos = np.arange(len(labels))
CTEs = [data_point_965_mean, data_point_1221_mean, data_point_1500_mean,data_point_1792_mean, data_point_2000_mean, data_point_2256_mean,data_point_2400_mean, data_point_2656_mean, data_point_2800_mean]
error = [data_point_965_std, data_point_1221_std, data_point_1500_std,data_point_1792_std, data_point_2000_std, data_point_2256_std,data_point_2400_std, data_point_2656_std, data_point_2800_std]


# Build the plot
fig, ax = plt.subplots()
ax.bar(x_pos, CTEs,
       yerr=error,
       align='center',
       alpha=0.5,
       ecolor='black',
       capsize=10)

ax.set_ylabel('Difference of resource usage costs between SOO and SOO CPLEX')
ax.set_xlabel('Total data stream rates (KB/sec)')
ax.set_xticks(x_pos)
ax.set_xticklabels(labels)
ax.set_title('Error bars')
ax.yaxis.grid(True)


# Save the figure and show
plt.tight_layout()
plt.savefig('bar_plot_with_error_bars.png')
plt.show()



fig, ax = plt.subplots()
ax.plot(count, sequence, "-*b", label="data point", markersize=4)
ax.plot(count, Bmin, "->r", label="Bmin",markersize=4)
ax.plot(count, Bmax, "-<g", label="Bmax",markersize=4)
ax.legend()
plt.ylabel('Data stream rates produced at the Edge (KB/sec)')
plt.xlabel('Experiment number')
plt.show()


