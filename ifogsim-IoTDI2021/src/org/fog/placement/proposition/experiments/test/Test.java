package org.fog.placement.proposition.experiments.test;

import org.apache.commons.math3.util.Pair;
import org.fog.application.AppEdge;
import org.fog.application.AppLoop;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.utils.JsonToApplication;
import org.json.simple.JSONObject;

public class Test {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		  JSONObject object = JsonToApplication.getApplicationJson(50, 3, 1, 256, 0.2, 1.8, 1.0, 1.0, 1 );
		  Application application = JsonToApplication.getApplication(1, "test", object);
		  applicationToString(application);
	}
	
	
public static void applicationToString(Application application){
		
		System.out.println("===================");
		System.out.println("Application modules");
		
		for(AppModule module : application.getModules()){
			
			System.out.println(module.getName()+":ram="+module.getRam()+", type="+module.getType()+", replicability="+module.isLocallyReplicable()+", cost="+module.getOperatorCost()+", selectivity="+module.getSelectivity());
		}
		
		System.out.println("\nApplication edges");
		for(AppEdge appEdge : application.getEdges()){
		
			System.out.println(appEdge.toString()+" tupleNwLength="+appEdge.getTupleNwLength());
		}
		
		System.out.println("\nApplication tupple mapping");
		for(AppModule module : application.getModules()){
			
			for(Pair<String, String> pair : module.getSelectivityMap().keySet()) {
				
				System.out.println(module.getName()+" ["+pair.getFirst()+", "+pair.getSecond()+"] Selectivity="+module.getSelectivityMap().get(pair).getMeanRate());
			}
			
		}
		System.out.println("\nApplication loop");
		for(AppLoop appLoop : application.getLoops()) {
			
			System.out.println(appLoop.getModules());
		}
		System.out.println("===================");
		
	}
}
