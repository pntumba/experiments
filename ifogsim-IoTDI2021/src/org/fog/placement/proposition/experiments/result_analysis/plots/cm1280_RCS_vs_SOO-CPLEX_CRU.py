import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [512,700,965,1221,1500,1792,2000,2100,2256]
CRU_SOOCPLEX = [0,0,0,0,0.07324348958,0.3087942708,0.5028333333,0.5972213542,0.7418164063]
CRU_RCS = [0,0,0.02384635417,0.2002122396,0.2808919271,0.3666184896,0.4975351563,0.5680755208,0.7035143229]

fig, ax = plt.subplots()
ax.plot(data_stream_rates, CRU_SOOCPLEX, "--^b", label="SOO-CPLEX", markersize=8, markevery=1)
ax.plot(data_stream_rates, CRU_RCS, "-ok", label="RCS", markersize=8)
ax.legend()
plt.ylabel('Computational resource usage costs- CRU')
plt.xlabel('Data stream rate produced at the Edge (KB/sec)')
plt.show()


