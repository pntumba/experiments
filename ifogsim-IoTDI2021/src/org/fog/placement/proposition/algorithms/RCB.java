package org.fog.placement.proposition.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.entities.FogDevice;
import org.fog.placement.proposition.monitoring.CostModel;
import org.fog.placement.proposition.utils.EdgeCut;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.MinMax;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.placement.proposition.utils.UtilFunctions;

public class RCB {

	private Graph graph;
	private Graph graph_rep;
	private double B;
	private double Bmax;
	private Map<Integer, Double> streamsSet;
	private Map<Integer, Double> cumulatedSelectivitiesForMigratedStreams;
	private Map<Integer, Graph> initialMapping = new HashMap<Integer, Graph>();
	private Map<Integer, Graph> mappingGmigSat = new HashMap<Integer, Graph>();
	private Map<Integer, Integer> sourcesToResourceNodes;
	private Map<Integer, ModuleType> operatorsType;
	private Map<Integer, Double> operatorsSelectivity;
	private Map<Integer, Boolean> operatorsReplicability;
	private Map<Integer, Integer> replicationAndMigrationPoints = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Map<Integer, EdgeCut>> allEdgeCuts = null;
	private Map<Integer, Map<Integer, Double>> allEdgeCutValues = null;
	private int edgeCutHopParameter = 0;
    
	private double w_c;
	private double w_n;
	private double cmu_F_j = 0.0;
	private double nbu_F_jC = 0.0;
	private double nd_FC;
	private double nd_EF;
	private MinMax compMinMax = UtilFunctions.compMinMax;
	private MinMax netwMinMax = UtilFunctions.netwMinMax;
	private Map<Integer, Double> processedDataStreams = new ConcurrentHashMap<Integer, Double>();

	private Map<Integer, Double> adjustedNetw_resource_usage_cost = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> adjustedComp_resource_usage_cost = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> deltaRU_set = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> operatorCosts = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Queue<Integer>> datastream_deltaRU_index = new ConcurrentHashMap<Integer, Queue<Integer>>();
	
	private Map<Integer, EdgeCut> acceptedEdgeCutHops = new ConcurrentHashMap<Integer, EdgeCut>();
	private Map<Integer, Double> contributiontoCRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> contributiontoNRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Integer> edgeCutIndex_per_deltaRU = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Double> nbu_per_deltaRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> cmu_per_deltaRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Graph> graph_mig_per_deltaRU = new ConcurrentHashMap<Integer, Graph>();
	private ArrayList<Integer> stream_to_update = new ArrayList<Integer>();
	private int maxFogNumber = 0;

	public RCB(Graph graph, Map<Integer, ModuleType> operatorsType, Map<Integer, Double> operatorsSelectivity,
			Map<Integer, Double> operatorCosts, Map<Integer, Boolean> operatorsReplicability) {

		this.graph = graph;
		this.operatorsReplicability = operatorsReplicability;
		this.operatorsSelectivity = operatorsSelectivity;
		this.operatorsType = operatorsType;
		this.cumulatedSelectivitiesForMigratedStreams = new ConcurrentHashMap<Integer, Double>();
		this.operatorCosts = operatorCosts;
		compMinMax = UtilFunctions.compMinMax;
		netwMinMax = UtilFunctions.netwMinMax;
		
	}

	public void initialConfiguration(double nd_FC, double nd_EF, double nb_F_jC, Map<Integer, Double> streamsSet) {

		System.out.println("\nStart identifying edge-cut that minimize NRU_j - (1/N * CRU_j)\n");
		allEdgeCuts = new ConcurrentHashMap<Integer, Map<Integer, EdgeCut>>();
		allEdgeCutValues = new ConcurrentHashMap<Integer, Map<Integer, Double>>();
		this.nd_FC = nd_FC;
		this.nd_EF = nd_EF;
		this.Bmax = nb_F_jC;
		this.streamsSet = streamsSet;
		this.graph_rep = UtilFunctions.replicableGraph(graph, operatorsReplicability);
		maxFogNumber = streamsSet.size();
		Queue<Integer> streamQueue = queue(streamsSet);

		double cm_F_j = 0.0;
		double cru_F_j_p = 0.0;
		double nru_F_j_p = 0.0;
		double CRU_j = 0.0;
		double NRU_j = 0.0;

		while (!streamQueue.isEmpty()){

			int S_j = streamQueue.remove();
			System.out.println("\n\t**Search edge-cut that minimizes (NRU_j - 1/N * CRU_j) for Gmig_" + S_j);
			int F_j = getSourcesToResourceNodes().get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			int memory_j = fogDevice.getHost().getRamProvisioner().getAvailableRam();
			UtilFunctions.setAvailableMemory(memory_j);
			int sourceVertex = S_j;
			int sinkVertex = graph_rep.getSink();
			Graph graph_candidat = UtilFunctions.candidatSubGraph(graph_rep, sourceVertex, sinkVertex,
					operatorsReplicability);
			double stream = streamsSet.get(S_j);
			UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_candidat, operatorsType, operatorsSelectivity,
					operatorCosts, sourceVertex, stream);
			Graph graph_sat_j = UtilFunctions.getGraphSatRequirement();
			cmu_F_j = 0.0;
			cm_F_j = UtilFunctions.getAvailableMemory();
			nbu_F_jC = stream;
			cru_F_j_p = Double.MAX_VALUE;
			nru_F_j_p = Double.MAX_VALUE;
			CRU_j = cru_F_j_p / maxFogNumber;
			//CRU_j = cru_F_j_p;
			NRU_j = nru_F_j_p;
			
			contributiontoCRU.put(S_j, cru_F_j_p);
			contributiontoNRU.put(S_j, NRU_j);
			
			CostModel.getFog_cpu_memory_usage().put(F_j, cmu_F_j);
			CostModel.getFog_max_cpu_memory().put(F_j, cm_F_j);
			CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
			CostModel.getFog_to_cloud_netw_delay().put(F_j, nd_FC);
			CostModel.getEdge_to_fog_netw_delay().put(F_j, nd_EF);
			processedDataStreams.put(S_j, nbu_F_jC);
			double decision_variable = Double.MAX_VALUE;
			if (!graph_sat_j.getAdjVertices().isEmpty()){
				
				
				// Search data min cut 
				UtilFunctions.findAllEdgeCuts(graph_sat_j, operatorsSelectivity);
				Graph graph_mig_j = UtilFunctions.getGraphSatRequirement();
				mappingGmigSat.put(S_j, graph_mig_j);
				getCumulatedSelectivitiesForMigratedStreams().put(S_j, UtilFunctions.getCumulatedSelectivity());
				
				Map<Integer, EdgeCut> edgeCuts = new HashMap<Integer, EdgeCut>(UtilFunctions.getAllEdgeCuts());
				allEdgeCuts.put(S_j, edgeCuts);

				Map<Integer, Double> edgeCutValues = new HashMap<Integer, Double>(UtilFunctions.getAllEdgeCutValues());
				getAllEdgeCutValues().put(S_j, edgeCutValues);
				for(int edgeCutIndex : allEdgeCuts.get(S_j).keySet()) {
					
					
					if(!getAllEdgeCutValues().get(S_j).containsKey(edgeCutIndex)) {
						continue;
					}
					Graph new_graph_mig_j = computeIndividualeResourceUsage(S_j, graph_mig_j, edgeCuts.get(edgeCutIndex), edgeCutIndex);
					cru_F_j_p = cmu_F_j / cm_F_j;
					nru_F_j_p = (nbu_F_jC / nb_F_jC) * (nd_FC / nd_EF);
					CRU_j = cru_F_j_p / maxFogNumber;
					NRU_j = nru_F_j_p;
					double new_decision_variable = Math.abs(NRU_j - CRU_j);
					System.out.print("\tcurrent dv=" + decision_variable);
					System.out.print("\tnew dv =" + new_decision_variable);
					System.out.print("\tNRU_j=" + NRU_j);
					System.out.print("\tCRU_j=" + CRU_j);
					System.out.println("\tcru_F_"+S_j+"_"+edgeCutIndex+"=" + cru_F_j_p);	
					if(new_decision_variable < decision_variable){
						
						decision_variable = new_decision_variable;
						contributiontoCRU.put(S_j, cru_F_j_p);
						contributiontoNRU.put(S_j, NRU_j);
						initialMapping.put(F_j, new_graph_mig_j);
						CostModel.getFog_cpu_memory_usage().put(F_j, cmu_F_j);
						CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
						System.out.println("\tAccepted edge-cut with new dv="+decision_variable);
						replicationAndMigrationPoints.put(S_j, edgeCutIndex);
					}
					
				  }	
				
				}	
				System.out.print("\n\tcontribution to CRU =" +contributiontoCRU.get(S_j));
				System.out.print("\tcontribution to NRU =" + contributiontoNRU.get(S_j));
				System.out.println("\tGmig_" + S_j + "= " + initialMapping.get(F_j).getAdjVertices());
			}
		
		showSolution("Initial");
		
		diverge();
		
		System.exit(0);
	}
    
	public void diverge(){
		
		System.out.println("**Start diverging to improve soluion");
		double cru_F_j_p = 0.0;
		double nru_F_j_p = 0.0;
		double CRU_j = 0.0;
		double NRU_j = 0.0;
		double deltaGap = 0;
		int selectedS_j = -1;
		int selectedEdgeCutIndex = 0;
		
		double acceptedCru_F_j_p = 0.0;
		double acceptedNRU_j = 0.0;
		double acceptedcmu_F_j = 0.0;
		double acceptednbu_FjC = 0.0;
		Graph acceptedGraph_mig_j = null;
		
		int maxS_j=0;
		int minS_j=0;
		
		double cru_F_j_max = 0;
		double cru_F_j_min = Double.MAX_VALUE;
		for (int S_j : replicationAndMigrationPoints.keySet()){
			
			
			double cru_F_j = contributiontoCRU.get(S_j);
			if(cru_F_j_max < cru_F_j){
				
				maxS_j = S_j;
				cru_F_j_max = cru_F_j;
			}
			
			if(cru_F_j_min > cru_F_j){
				
				minS_j = S_j;
				cru_F_j_min = cru_F_j;
			}
		}
		
		System.out.print("\tFind: S_"+minS_j+" with min cru_F_j="+cru_F_j_min);
		System.out.println("\tS_"+maxS_j+" with max cru_F_j="+cru_F_j_max);
		Queue<Integer> selectedStreams = new LinkedList<Integer>();
		selectedStreams.add(minS_j);selectedStreams.add(maxS_j);
		EdgeCut edgeCut = new EdgeCut();
		
		while(!selectedStreams.isEmpty()){
			int S_j = selectedStreams.remove();
			int F_j = getSourcesToResourceNodes().get(S_j);
			System.out.println("\t**Check if S_"+S_j+" can improve the initial soluion");
			double cm_F_j = CostModel.getFog_max_cpu_memory().get(F_j);
			
			double curCmu_Fj = CostModel.getFog_comp_resource_usage_cost().get(F_j);
			double curNbu_Fj = CostModel.getFog_to_cloud_netw_bwd_usage().get(F_j);
			double curCru_Fj = curCmu_Fj / cm_F_j;
			double curNru_Fj = (curNbu_Fj / Bmax) * (nd_FC / nd_EF);
			double curCRU_j = curCru_Fj/maxFogNumber;
			double curNRU_j = curNru_Fj;
			
			int current_edgeCutIndex = replicationAndMigrationPoints.get(S_j);
			int preceding_edgeCutIndex = current_edgeCutIndex-1;
			Graph graph_mig_j = mappingGmigSat.get(S_j);
			if(getAllEdgeCutValues().get(S_j).containsKey(preceding_edgeCutIndex)) {
				
				edgeCut = allEdgeCuts.get(S_j).get(preceding_edgeCutIndex);
				Graph new_graph_mig_j = computeIndividualeResourceUsage(S_j, graph_mig_j, edgeCut, preceding_edgeCutIndex);
				cru_F_j_p = cmu_F_j / cm_F_j;
				nru_F_j_p = (nbu_F_jC / Bmax) * (nd_FC / nd_EF);
				CRU_j = cru_F_j_p / maxFogNumber;
				NRU_j = nru_F_j_p;
				
				double deltaCRU = CRU_j - curCRU_j;
				double deltaNRU = NRU_j - curNRU_j;
				System.out.print("\t\tdeltaCRU="+deltaCRU);
				System.out.print("\tdeltaNRU="+deltaNRU);
				System.out.println("\tdeltaGap="+(deltaCRU - deltaNRU));
				if(deltaGap < Math.abs(deltaCRU - deltaNRU)) {
					
					deltaGap = Math.abs(deltaCRU - deltaNRU);
					selectedEdgeCutIndex = preceding_edgeCutIndex;
					selectedS_j = S_j;
					acceptedGraph_mig_j = new_graph_mig_j;
					System.out.println("\t\tAccept preceding edgeCutIndex="+selectedEdgeCutIndex+" with deltaGap="+deltaGap);
					
					acceptedCru_F_j_p = cru_F_j_p;
					acceptedNRU_j = NRU_j;
					acceptedcmu_F_j = cmu_F_j;
					acceptednbu_FjC = nbu_F_jC;
					
				}
				
			}
			
			int next_edgeCutIndex = current_edgeCutIndex+1;
			if(getAllEdgeCutValues().get(S_j).containsKey(next_edgeCutIndex)) {
				
				edgeCut = allEdgeCuts.get(S_j).get(next_edgeCutIndex);
				Graph new_graph_mig_j = computeIndividualeResourceUsage(S_j, graph_mig_j, edgeCut, next_edgeCutIndex);
				cru_F_j_p = cmu_F_j / cm_F_j;
				nru_F_j_p = (nbu_F_jC / Bmax) * (nd_FC / nd_EF);
				CRU_j = cru_F_j_p / maxFogNumber;
				NRU_j = nru_F_j_p;
				double deltaCRU = CRU_j - curCRU_j;
				double deltaNRU = NRU_j - curNRU_j;
				System.out.print("\t\tdeltaCRU="+deltaCRU);
				System.out.print("\tdeltaNRU="+deltaNRU);
				System.out.println("\tdeltaGap="+(deltaCRU - deltaNRU));
				if(deltaGap < Math.abs(deltaCRU - deltaNRU)) {
					
					deltaGap = Math.abs(deltaCRU - deltaNRU);
					selectedEdgeCutIndex = next_edgeCutIndex;
					selectedS_j = S_j;
					acceptedGraph_mig_j = new_graph_mig_j;
					System.out.println("\t\\tAccept succeding edgeCutIndex="+selectedEdgeCutIndex+" with deltaGap="+deltaGap);
					
					acceptedCru_F_j_p = cru_F_j_p;
					acceptedNRU_j = NRU_j;
					acceptedcmu_F_j = cmu_F_j;
					acceptednbu_FjC = nbu_F_jC;
				}
			}
		}
		
		
		System.out.print("\n\tSelect S_"+selectedS_j);
		System.out.println("\tEdgeCutIndex="+selectedEdgeCutIndex);
		
		if(selectedS_j >= 0) {
			
			contributiontoCRU.put(selectedS_j, acceptedCru_F_j_p);
			contributiontoNRU.put(selectedS_j, acceptedNRU_j);
			int F_j = getSourcesToResourceNodes().get(selectedS_j);
			initialMapping.put(F_j, acceptedGraph_mig_j);
			CostModel.getFog_cpu_memory_usage().put(F_j, acceptedcmu_F_j);
			CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, acceptednbu_FjC);
			showSolution("Improved");
		}
		
	}
	
	private void showSolution(String label) {
		
		
		CostModel.calculateComp_resource_usage_cost();
		CostModel.calculateNetw_resource_usage_cost();
		CostModel.calculateResource_usage_cost();
		CostModel.calculateStdev();
		System.out.println("\n"+label+" Mapping");
		B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
		System.out.println("\t"+label+" B=" + CostModel.getFog_to_cloud_overall_netw_bwd_usage());
		System.out.print("\t"+label+" NRU=" + CostModel.getNorm_netw_resource_usage_cost());
		for(int S_j : contributiontoCRU.keySet()){
			
			System.out.print("\tcru_"+S_j+"=" + contributiontoCRU.get(S_j));
		}
		System.out.println("\n\t"+label+" CRU=" + CostModel.getNorm_comp_resource_usage_cost());
		double gap = Math.abs(CostModel.getNorm_comp_resource_usage_cost()-CostModel.getNorm_netw_resource_usage_cost());
		System.out.println("\tgap="+gap+" does NRU =? CRU");
		System.out.println("\tInitial RU=" + CostModel.getResource_usage_cost());
		System.out.println("\tInitial stdev=" + CostModel.getStdev()+"\n");
		
	}
    private Graph computeIndividualeResourceUsage(int S_j, Graph graph, EdgeCut edgeCut, int edgeCutIndex) {
    	
    	Graph graph_to_replicate = UtilFunctions.candidatGraphToReplicate(graph, S_j, edgeCut.getEdgeCut());
		UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_to_replicate, operatorsType, operatorsSelectivity,operatorCosts, S_j, this.streamsSet.get(S_j));
		Graph new_graph_mig_j = UtilFunctions.getGraphSatRequirement();
		cmu_F_j = UtilFunctions.getRequiredMemory();
		nbu_F_jC = getAllEdgeCutValues().get(S_j).get(edgeCutIndex);
		return new_graph_mig_j;
    } 

	private Queue<Integer> queue(Map<Integer, Double> streamsSet) {

		Queue<Integer> queue = new LinkedList<Integer>();
		for (int F_j : streamsSet.keySet()) {

			queue.add(F_j);
		}

		return queue;

	}

	public double getW_c() {
		return w_c;
	}

	public void setW_c(double w_c) {
		this.w_c = w_c;
	}

	public double getW_n() {
		return w_n;
	}

	public void setW_n(double w_n) {
		this.w_n = w_n;
	}

	public Map<Integer, Integer> getSourcesToResourceNodes() {
		return sourcesToResourceNodes;
	}

	public void setSourcesToResourceNodes(Map<Integer, Integer> sourcesToResourceNodes) {
		this.sourcesToResourceNodes = sourcesToResourceNodes;
	}

	public Map<Integer, Double> getStreamsSet() {
		return streamsSet;
	}

	public void setStreamsSet(Map<Integer, Double> streamsSet) {

		this.streamsSet = streamsSet;
	}

	public Map<Integer, Double> getCumulatedSelectivitiesForMigratedStreams() {
		return cumulatedSelectivitiesForMigratedStreams;
	}

	public void setCumulatedSelectivitiesForMigratedStreams(
			Map<Integer, Double> cumulatedSelectivitiesForMigratedSreams) {
		this.cumulatedSelectivitiesForMigratedStreams = cumulatedSelectivitiesForMigratedSreams;
	}

	public Map<Integer, Graph> getInitialMapping() {
		return initialMapping;
	}

	public void setInitialMapping(Map<Integer, Graph> initialMapping) {
		this.initialMapping = initialMapping;
	}

	public Map<Integer, Map<Integer, Double>> getAllEdgeCutValues() {
		return allEdgeCutValues;
	}

	public void setAllEdgeCutValues(Map<Integer, Map<Integer, Double>> allEdgeCutValues) {
		this.allEdgeCutValues = allEdgeCutValues;
	}

	public int getEdgeCutHopParameter() {
		return edgeCutHopParameter;
	}

	public void setEdgeCutHopParameter(int edgeCutHopParameter) {
		this.edgeCutHopParameter = edgeCutHopParameter;
	}
}
