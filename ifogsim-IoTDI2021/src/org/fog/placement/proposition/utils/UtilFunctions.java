package org.fog.placement.proposition.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.math3.util.Pair;
import org.fog.application.AppEdge;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.placement.proposition.monitoring.CostModel;

import static java.util.stream.Collectors.*;
import static org.junit.Assert.assertFalse;

public class UtilFunctions {

	private static double availableMemory;
	private static double availableCpu;

	private static double requiredMemory;
	private static double requiredCpu;

	private static Map<Integer, ArrayList<Integer>> minCut = null;
	private static Map<Integer, ArrayList<Integer>> edgeCutSet = null;
	private static Map<Integer, ArrayList<Double>> edgeCutSetValues = null;
	
	private static double minCutValue = 0;
	private static int minCutCountIndex = 0;
	private static Graph GraphSatRequirement = null;
	private static double cumulatedSelectivity = 0.0;

	private static Map<Integer, EdgeCut> allEdgeCuts = new ConcurrentHashMap<Integer, EdgeCut>();
	private static Map<Integer, Double> allEdgeCutValues = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Double> cmuPerEdgeCut = new ConcurrentHashMap<Integer, Double>();
	public static MinMax compMinMax = new MinMax(0,CostModel.getSource_number());
	public static MinMax netwMinMax  = new MinMax(0,1.0);
	private static Map<Integer, Double> operatorCosts;
	private static boolean watchDog = false;
	private static String ordering = null;
	private static HashMap<Integer, Double> edgeDataRates = null;
	

	public static Graph candidatSubGraph(Graph graphG, int source, int sink,
			Map<Integer, Boolean> operatorsReplicability) {

		ArrayList<ArrayList<Integer>> Setpaths = new ArrayList<ArrayList<Integer>>();

		// Since the sink is not replicable, it can not appear in the replicable graph
		// as a
		// vertex
		// We consider as the sink vertex, the upstream vertex of the edge connecting to
		// the actual sink
		int newSink = sink;
		if (!graphG.getAdjVertices().containsKey(sink)) {

			for (int vertex1 : graphG.getAdjVertices().keySet()) {

				if (graphG.getAdjVertices().get(vertex1).containsKey(sink)) {

					newSink = vertex1;
				}
			}
		}
		ArrayList<Integer> pathList = new ArrayList<Integer>();
		Set<Integer> visitedVertices = new LinkedHashSet<Integer>();
		pathList.add(source);
		printAllPathsUtil(graphG, source, newSink, visitedVertices, pathList, Setpaths);
		Graph candidatGraph = new Graph();

		for (ArrayList<Integer> path : Setpaths) {

			int path_size = path.size();
			for (int index = 0; index < path_size; index++) {

				int vertex1 = path.get(index);
				candidatGraph.addVertex(vertex1);
				if (index + 1 < path_size) {

					int vertex2 = path.get(index + 1);
					candidatGraph.addEdge(vertex1, vertex2);

				} else {

					for (int vertex2 : graphG.getSuccessors(vertex1)) {

						if (!operatorsReplicability.get(vertex2)) {

							candidatGraph.addEdge(vertex1, vertex2);
						}
					}
				}
			}
		}
		return candidatGraph;
	}

	public static ArrayList<Graph> printAllPaths(Graph graphG, int source, int sink) {

		ArrayList<ArrayList<Integer>> Setpaths = new ArrayList<ArrayList<Integer>>();

		// Since the sink is not replicable, it can not appear in the replicable graph
		// as a
		// vertex
		// We consider as the sink vertex, the source vertex of the edge connecting to
		// the actual sink
		int validSink = sink;
		if (!graphG.getAdjVertices().containsKey(sink)) {

			for (int vertex1 : graphG.getAdjVertices().keySet()) {

				if (graphG.getAdjVertices().get(vertex1).containsKey(sink)) {

					validSink = vertex1;
				}

			}

		}

		ArrayList<Integer> pathList = new ArrayList<Integer>();
		Set<Integer> visitedVertices = new LinkedHashSet<Integer>();
		pathList.add(source);
		printAllPathsUtil(graphG, source, validSink, visitedVertices, pathList, Setpaths);

		ArrayList<Graph> graphs = new ArrayList<Graph>();

		Graph candidatGraph = new Graph();
		for (ArrayList<Integer> path : Setpaths) {

			for (int vertex1 : graphG.getAdjVertices().keySet()) {

				for (int vertex2 : graphG.getAdjVertices(vertex1).keySet()) {

					if (path.contains(vertex1) && path.contains(vertex2)) {

						candidatGraph.addVertex(vertex1);
						candidatGraph.addEdge(vertex1, vertex2);

					} else if (path.contains(vertex1) && vertex2 == sink) {

						candidatGraph.addVertex(vertex1);
						candidatGraph.addEdge(vertex1, vertex2);
					}

				}

			}
			graphs.add(candidatGraph);

		}
		return graphs;
	}

	private static void printAllPathsUtil(Graph graphG, int source, int sink, Set<Integer> visitedVertices,
			ArrayList<Integer> localPathList, ArrayList<ArrayList<Integer>> Setpaths) {

		visitedVertices.add(source);
		if (source == sink) {

			ArrayList<Integer> pathFound = new ArrayList<Integer>();
			for (int vertex : localPathList) {

				pathFound.add(vertex);
			}
			Setpaths.add(pathFound);
			visitedVertices.remove(source);
			return;
		}
		if (graphG.getAdjVertices().containsKey(source)) {
			for (Integer i : graphG.getAdjVertices(source).keySet()) {
				if (!visitedVertices.contains(i)) {

					localPathList.add(i);
					printAllPathsUtil(graphG, i, sink, visitedVertices, localPathList, Setpaths);

					localPathList.remove(i);
				}
			}
		}

		visitedVertices.remove(source);
	}

	/**
	 * 
	 * 
	 * Enable to find a subgraph that are replicable on a specific fog node To do
	 * so, the search is performed on the initial graph starting from the source to
	 * the sink
	 * 
	 * @param graph
	 * @param source
	 * @param operatorsReplicability
	 * @return Graph graph
	 */
	public static Graph replicableGraph(Graph graph, Map<Integer, Boolean> operatorsReplicability) {
		Graph replicableGraph = new Graph();
		for (int source : graph.getAdjVertices().keySet()) {

			if (graph.isSource(source)) {

				Set<Integer> visited = new LinkedHashSet<Integer>();
				Queue<Integer> queue = new LinkedList<Integer>();
				queue.add(source);
				visited.add(source);
				while (!queue.isEmpty()) {

					int vertex1 = queue.poll();
					if (replicableGraph.getAdjVertices().containsKey(vertex1)) {

						continue;
					}

					if (!operatorsReplicability.get(vertex1)) {
						continue;
					}

					replicableGraph.addVertex(vertex1);
					HashMap<Integer, Double> adjVertices = graph.getAdjVertices(vertex1);
					Set setAdjVertices = adjVertices.entrySet();
					Iterator iterator = setAdjVertices.iterator();
					while (iterator.hasNext()) {
						Map.Entry mentry = (Map.Entry) iterator.next();
						Integer vertex2 = (Integer) mentry.getKey();

						if (operatorsReplicability.get(vertex1)) {

							double edgeWeight = graph.getEdgeWeight(vertex1, vertex2);
							replicableGraph.addEdge(vertex1, vertex2, edgeWeight);
							if (!visited.contains(vertex2)) {

								visited.add(vertex2);
								queue.add(vertex2);
							}
						}

					}

				}

			}

		}
		return replicableGraph;
	}

	public static Graph candidatGraphToReplicate(Graph graph, int sourceVertex,
			Map<Integer, ArrayList<Integer>> minCut) {

		Graph graphToReplicate = new Graph();

		for (int inVertex : minCut.keySet()){

			Set<Integer> visited = new LinkedHashSet<Integer>();
			Queue<Integer> queue = new LinkedList<Integer>();
			queue.add(sourceVertex);
			visited.add(sourceVertex);
			while (!queue.isEmpty()) {
				int vertex1 = queue.poll();

				if (!graphToReplicate.getAdjVertices().containsKey(vertex1)) {

					graphToReplicate.addVertex(vertex1);
				}

				HashMap<Integer, Double> adjVertices = graph.getAdjVertices(vertex1);
				Set setAdjVertices = adjVertices.entrySet();
				Iterator iterator = setAdjVertices.iterator();
				while (iterator.hasNext()) {
					Map.Entry mentry = (Map.Entry) iterator.next();
					Integer vertex2 = (Integer) mentry.getKey();
					if (vertex1 != inVertex) {

						graphToReplicate.addEdge(vertex1, vertex2, graph.getEdgeWeight(vertex1, vertex2));
						if (!visited.contains(vertex2) && graph.getAdjVertices().containsKey(vertex2)) {

							visited.add(vertex2);
							queue.add(vertex2);
						}
					} else {

						graphToReplicate.addEdge(vertex1, vertex2, graph.getEdgeWeight(vertex1, vertex2));
					}
				}

			}

		}

		return graphToReplicate;
	}

	public static void findAllEdgeCuts(Graph graphG, Map<Integer, Double> operatorsSelectivity){
		
		int countEdgeCut = 0;
		setMinCutValue(Double.MAX_VALUE);
		double processedStream;
		minCut = new ConcurrentHashMap<Integer, ArrayList<Integer>>();
		edgeCutSet = new ConcurrentHashMap<Integer, ArrayList<Integer>>();
		edgeCutSetValues = new ConcurrentHashMap<Integer, ArrayList<Double>>();
		allEdgeCutValues = new ConcurrentHashMap<Integer, Double>();
		edgeDataRates = new HashMap<Integer, Double>();
		double cumulatedOperatorSelectivity = 1.0;
		
		for (int vertex1 : graphG.getAdjVertices().keySet()) {

			cumulatedOperatorSelectivity = cumulatedOperatorSelectivity * operatorsSelectivity.get(vertex1);
			processedStream = 0;
			ArrayList<Integer> nextVertices = new ArrayList<Integer>();
			ArrayList<Double> edge = new ArrayList<Double>();
			for (int vertex2 : graphG.getAdjVertices(vertex1).keySet()) {

				processedStream += graphG.getEdgeWeight(vertex1, vertex2);
				nextVertices.add(vertex2);
				edge.add(cumulatedOperatorSelectivity);
			}

			edgeDataRates.put(vertex1, processedStream);
			edgeCutSet.put(vertex1, nextVertices);
			edgeCutSetValues.put(vertex1, edge);

			EdgeCut edgeCut = new EdgeCut();
			edgeCut.getEdgeCut().put(vertex1, nextVertices);
			countEdgeCut += 1;
			allEdgeCutValues.put(countEdgeCut, processedStream);
			allEdgeCuts.put(countEdgeCut, edgeCut);
			
			if (processedStream < getMinCutValue()){

				setMinCutCountIndex(countEdgeCut);
				setMinCutValue(processedStream);
				minCut.clear();
				minCut.put(vertex1, nextVertices);
				setCumulatedSelectivity(cumulatedOperatorSelectivity);
			}
		}
	}
	
	
	public static Queue<Integer> descendantsortMap(Map<Integer, Double> streamsList) {
		
		UtilFunctions.ordering = "highest";
		Queue<Integer> topStreams = new LinkedList<Integer>();

		Map<Integer, Double> tmpStreamsList = streamsList.entrySet().stream()
				.sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

		for(int key : tmpStreamsList.keySet()){

			topStreams.add(key);
		}
		return topStreams;
	}

	public static Queue<Integer> ascendantSortMap(Map<Integer, Double> streamsList) {
		
		UtilFunctions.ordering = "lowest";
		Queue<Integer> topStreams = new LinkedList<Integer>();
		LinkedHashMap<Integer, Double> ascendantSortedMap = new LinkedHashMap<>();

		streamsList.entrySet().stream().sorted(Map.Entry.comparingByValue())
				.forEachOrdered(x -> ascendantSortedMap.put(x.getKey(), x.getValue()));

		for (int key : ascendantSortedMap.keySet()) {

			topStreams.add(key);
		}
		return topStreams;
	}

	public static Queue<Double> sortMap(Map<Double, Double> mapObject) {

		Queue<Double> topList = new LinkedList<Double>();

		Map<Double, Double> sortedMapObject = mapObject.entrySet().stream()
				.sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

		for (double key : sortedMapObject.keySet()) {

			topList.add(key);
		}
		return topList;

	}

	public static double sumUpStreamRate(Map<Integer, Double> streamsList) {

		double sum = 0.0;
		for (int source : streamsList.keySet()) {

			sum += streamsList.get(source);
		}
		return sum;
	}

	public static void identifyGraphSatisfyingFogNodeResource(Graph graph, Map<Integer, ModuleType> operatorsType,
			Map<Integer, Double> operatorsSelectivity, Map<Integer, Double> operatorCost, int sourceVertex, double streamRate) {
		setRequiredMemory(0.0);
		UtilFunctions.operatorCosts = operatorCost;
		setGraphSatRequirements(new Graph());
		if (streamRate > availableMemory) {
			System.out.println("\tInput streamRate="+streamRate+" < Available Fog computational resources "+availableMemory);
			return;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		Map<Integer, Double> visited = new LinkedHashMap<Integer, Double>();
		Map<Integer, Double> dataloads = new LinkedHashMap<Integer, Double>();
		visited.put(sourceVertex, streamRate);
		dataloads.put(sourceVertex, 0.0);
		queue.add(sourceVertex);
		while (queue.size() != 0) {

			int vertex1 = queue.pop();
			double vertex1_input_data = 0.0;
			double vertex1_output_data = 0.0;
			if (graph.isSource(vertex1)) {

				vertex1_input_data = streamRate;

			} else {

				vertex1_input_data = intput_data(operatorsType, graph, vertex1);
			}
			if(checkCmu(vertex1, vertex1_input_data)){
				
				if (graph.getAdjVertices().containsKey(vertex1)){

					GraphSatRequirement.addVertex(vertex1);
					dataloads.put(vertex1, vertex1_input_data);
					cmu(vertex1,dataloads, streamRate);
					Set entryset = graph.getAdjVertices(vertex1).entrySet();
					Iterator it = entryset.iterator();
					while (it.hasNext()) {

						vertex1_output_data = vertex1_input_data * operatorsSelectivity.get(vertex1);
						Map.Entry mentry = (Map.Entry) it.next();
						Integer vertex2 = (Integer) mentry.getKey();
						GraphSatRequirement.addEdge(vertex1, vertex2, vertex1_output_data);

						if (!visited.containsKey(vertex2) && graph.getAdjVertices().containsKey(vertex2)) {

							queue.add(vertex2);
							visited.put(vertex2, vertex1_output_data);
						}
					}
				}
			}	
		}
		getGraphSatRequirement().sortGraph();
	}

	public static void cmuPerEdgeCut(Graph graph, Map<Integer, ModuleType> operatorsType,
			Map<Integer, Double> operatorsSelectivity, Map<Integer, Double> operatorCost, int sourceVertex, double streamRate) {
		
		
		setRequiredMemory(0.0);
		cmuPerEdgeCut = new ConcurrentHashMap<Integer, Double>();
		UtilFunctions.operatorCosts = operatorCost;
		setGraphSatRequirements(new Graph());
		if (streamRate > availableMemory) {
			System.out.println("streamRate="+streamRate+" < "+availableMemory);
			//return;
		}
		LinkedList<Integer> queue = new LinkedList<Integer>();
		Map<Integer, Double> visited = new LinkedHashMap<Integer, Double>();
		Map<Integer, Double> dataloads = new LinkedHashMap<Integer, Double>();
		visited.put(sourceVertex, streamRate);
		dataloads.put(sourceVertex, 0.0);
		queue.add(sourceVertex);
		while (queue.size() != 0) {

			int vertex1 = queue.pop();
			double vertex1_input_data = 0.0;
			double vertex1_output_data = 0.0;
			if (graph.isSource(vertex1)) {

				vertex1_input_data = streamRate;

			} else {

				vertex1_input_data = intput_data(operatorsType, graph, vertex1);
			}
			cmu(vertex1, dataloads, vertex1_input_data, streamRate);
			
				if (graph.getAdjVertices().containsKey(vertex1)){

					GraphSatRequirement.addVertex(vertex1);
					dataloads.put(vertex1, vertex1_input_data);
					cmu(vertex1,dataloads, streamRate);
					Set entryset = graph.getAdjVertices(vertex1).entrySet();
					Iterator it = entryset.iterator();
					while (it.hasNext()) {

						vertex1_output_data = vertex1_input_data * operatorsSelectivity.get(vertex1);
						Map.Entry mentry = (Map.Entry) it.next();
						Integer vertex2 = (Integer) mentry.getKey();
						GraphSatRequirement.addEdge(vertex1, vertex2, vertex1_output_data);

						if (!visited.containsKey(vertex2) && graph.getAdjVertices().containsKey(vertex2)) {

							queue.add(vertex2);
							visited.put(vertex2, vertex1_output_data);
						}
					}
			}
			
			if(getRequiredMemory() >= 0) {
				
					int count = cmuPerEdgeCut.size();
					cmuPerEdgeCut.put(count, getRequiredMemory());
			}
		}
		
		Map<Integer, Double> cmuPerEdgeCutTmp = new ConcurrentHashMap<Integer, Double>();
		for(int key : cmuPerEdgeCut.keySet()) {
			
			if(key > 0) {
				
				cmuPerEdgeCutTmp.put(key-1, cmuPerEdgeCut.get(key));
			}
		}
		cmuPerEdgeCut = new ConcurrentHashMap<Integer, Double>(cmuPerEdgeCutTmp);
		
	}
	
	private static double intput_data(Map<Integer, ModuleType> operatorsType, Graph graph, int vertex) {

		double input_data = 0;
		switch (operatorsType.get(vertex)) {

		case UNION:
			for (int vertexIn : GraphSatRequirement.getPredecessors(vertex)) {

				if (GraphSatRequirement.getAdjVertices().containsKey(vertexIn))
					input_data += GraphSatRequirement.getEdgeWeight(vertexIn, vertex);

			}
			break;

		case JOIN:

			input_data = 1.0;
			for (int vertexIn : GraphSatRequirement.getPredecessors(vertex)) {
				if (GraphSatRequirement.getAdjVertices().containsKey(vertexIn))
					input_data *= GraphSatRequirement.getEdgeWeight(vertexIn, vertex);
			}
			break;

		case SINGLE_INPUT_EDGE:

			for (int vertexIn : GraphSatRequirement.getPredecessors(vertex)) {

				input_data = GraphSatRequirement.getEdgeWeight(vertexIn, vertex);
			}
			break;

		case MULTIPLE_OUTPUT_EDGES:

			break;

		default:
			break;
		}
		return input_data;
	}

	private static boolean checkCmu(int vertex, double dataload) {

		double resources = dataload;
		double operatorCost = 1.0;
		if(operatorCosts.containsKey(vertex)) {
			
			operatorCost = operatorCosts.get(vertex);
		}
		double memory = getRequiredMemory() + resources * operatorCost;
		if( memory <= getAvailableMemory()) {
			
			return true;
			
		}else {
			
			return false;
		}
	}

	private static void cmu(int vertex, Map<Integer, Double> dataloads, double dataload, double dataloadToExlude) {

		double resources = dataload;
		for (int vertexIn : dataloads.keySet()) {

			resources += dataloads.get(vertexIn);
		}
		resources = resources - (dataloadToExlude * 2);
		double operatorCost = 1.0;
		if(operatorCosts.containsKey(vertex)) {
			
			operatorCost = operatorCosts.get(vertex);
		}
		setRequiredMemory(resources * operatorCost);
		setRequiredCpu(resources * operatorCost);
	}
	
	private static void cmu(int vertex, Map<Integer, Double> dataloads, double dataloadToExlude) {

		double resources = 0.0;
		double operatorCost = 0.0;
		for (int vertexIn : dataloads.keySet()) {
			
			if(operatorCosts.containsKey(vertexIn) && vertexIn != 1){
				
				operatorCost = operatorCosts.get(vertexIn);
			}
			resources += dataloads.get(vertexIn) * operatorCost;
			
		}
		setRequiredMemory(resources);
		setRequiredCpu(resources);
		
	}
	
	private static boolean satisfies(int vertex, Map<Integer, Double> dataloads, double dataloadToExlude) {

		double resources = 0.0;
		double operatorCost = 0.0;
		for (int vertexIn : dataloads.keySet()) {
			
			if(operatorCosts.containsKey(vertexIn) && vertexIn != 1){
				
				operatorCost = operatorCosts.get(vertexIn);
			}
			resources += dataloads.get(vertexIn) * operatorCost;
			
		}
		if(resources > getAvailableMemory()) {
			
			return false;
		}
		setRequiredMemory(resources);
		setRequiredCpu(resources);
		return true;
	}

	private static void setEdgeWeigth(int vertex, double weight, Graph graph) {

		for (int vertex2 : graph.getSuccessors(vertex)) {

			GraphSatRequirement.addEdge(vertex, vertex2, weight);
		}

	}

	public static void setMinMax() {

		compMinMax = new MinMax(0,CostModel.getSource_number());
		netwMinMax  = new MinMax(0,1.0);
	}

	
	public static boolean decideToReplicateOnFog(Graph graph_mig_j, EdgeCut edgeCut_p){
		
		
		boolean decideToNotReplicateOnFog=true;
		for (int vertex : edgeCut_p.getEdgeCut().keySet()) {

			if(graph_mig_j.isSource(vertex)){

				return false;
				
			}else {
				
				for(int vertexIn : graph_mig_j.getPredecessors(vertex)) {
					
					if (graph_mig_j.isSource(vertexIn)){
						
						return false;
						
					}
				}
			}
		}
		return decideToNotReplicateOnFog;
	}

	public static Map<Integer, EdgeCut> getAllEdgeCuts() {
		return allEdgeCuts;
	}

	public static void applicationToString(Application application) {

		System.out.println("===================");
		System.out.println("Application modules");
		for (AppModule module : application.getModules()) {

			System.out.println(module.getName()+":ram="+module.getRam()+", type="+module.getType()+", replicability="+module.isLocallyReplicable()+", cost="+module.getOperatorCost()+", selectivity="+module.getSelectivity());
		}

		System.out.println("\nApplication edges");
		for (AppEdge appEdge : application.getEdges()) {

			System.out.println(appEdge.toString() + " tupleNwLength=" + appEdge.getTupleNwLength());
		}

		System.out.println("\nApplication tupple mapping");
		for (AppModule module : application.getModules()){

			for (Pair<String, String> pair : module.getSelectivityMap().keySet()) {

				System.out.println(module.getName() + " [" + pair.getFirst() + ", " + pair.getSecond()+ "] Selectivity=" + module.getSelectivityMap().get(pair).getMeanRate());
			}
		}
		System.out.println("===================");

	}

	public static Map<Integer, Double> getAllEdgeCutValues() {
		return allEdgeCutValues;
	}

	public static Map<Integer, ArrayList<Integer>> getMinCut() {
		return minCut;
	}

	public static double getAvailableMemory() {
		return availableMemory;
	}

	public static void setAvailableMemory(double memory) {
		availableMemory = memory;
	}

	public static double getAvailableCpu() {
		return availableCpu;
	}

	public static void setAvailableCpu(double cpu) {
		availableCpu = cpu;
	}

	public static Graph getGraphSatRequirement() {
		return GraphSatRequirement;
	}

	public static void setGraphSatRequirements(Graph graph) {
		GraphSatRequirement = graph;
	}

	public static double getCumulatedSelectivity() {
		return cumulatedSelectivity;
	}

	public static void setCumulatedSelectivity(double cumulatedSelectivity) {
		UtilFunctions.cumulatedSelectivity = cumulatedSelectivity;
	}

	public static double getMinCutValue() {
		return minCutValue;
	}

	public static void setMinCutValue(double minCutValue) {
		UtilFunctions.minCutValue = minCutValue;
	}

	public static double getRequiredMemory() {
		return requiredMemory;
	}

	public static void setRequiredMemory(double requiredMemory) {
		UtilFunctions.requiredMemory = requiredMemory;
	}

	public static double getRequiredCpu() {
		return requiredCpu;
	}

	public static void setRequiredCpu(double requiredCpu) {
		UtilFunctions.requiredCpu = requiredCpu;
	}

	public static int getMinCutCountIndex() {
		return minCutCountIndex;
	}

	public static void setMinCutCountIndex(int minCutCountIndex) {
		UtilFunctions.minCutCountIndex = minCutCountIndex;
	}

	public static Map<Integer, Double> getCmuPerEdgeCut() {
		return cmuPerEdgeCut;
	}

	public static void setCmuPerEdgeCut(Map<Integer, Double> cmuPerEdgeCut) {
		UtilFunctions.cmuPerEdgeCut = cmuPerEdgeCut;
	}

	public static void setWatchDog(boolean watchDog) {
		// TODO Auto-generated method stub
		UtilFunctions.watchDog = watchDog;
	}
	
	public static boolean getWatchDog() {
		// TODO Auto-generated method stub
		return watchDog;
	}

	public static String getOrdering() {
		return ordering;
	}

	public static void setOrdering(String ordering) {
		UtilFunctions.ordering = ordering;
	}

	

}
