1. Experiments

We run 10 times each single expriemts, then we report the average value for each corresponding metric, we perform the experiments as described bellow.

1.1. Uniform Resource capacity,Dynamic Graph and Static data Stream rate

 - Number of operators in the application graph: 1 - 14 
 - 4 Edge nodes producing data streams: 4
 - 4 Fog nodes receiving, processing the prodcudes data stream and transmitting processed data stream to the Cloud
 - Each fog node has the resource capacity of 1024000 Kbytes.

 - 256000 Kbytes for all the data stream rates arriving to the distributed as follows: 64000 Kbytes for the 1st Fog node, 48000 Kbytes for the 2nd Fog node, 36000 Kbytes for the 3rd Fog node and 108000 Kbytes for the 4th Fog node.  
 - The selectivity of operator: 0.2 - 1.8 follwing the uniform distribution
 - The upper threshold for the cloud bandwidth usage: 260000.0
 - The lower threshold for the cloud bandwidth usage: 130000.0

 It remains to run the Experiment for the ILP approach.

1.2. Heterogeneous Resource capacity,Dynamic Graph and Static data Stream rate

 - Number of operators in the application graph: 1 - 14 
 - 4 Edge nodes producing data streams: 4
 - 4 Fog nodes receiving, processing the prodcudes data stream and transmitting processed data stream to the Cloud
 - The resource capacity of the Fog nodes is distributed as following the 1st has 1024000 Kbytes, 768000 Kbytes for the 2nd, 576000 Kbytes for the 3rd and 1728000 Kbytes for the 4th.
 - 256000 Kbytes for all the data stream rates arriving to the distributed as follows: 64000 Kbytes for the 1st Fog node, 48000 Kbytes for the 2nd Fog node, 36000 Kbytes for the 3rd Fog node and 108000 Kbytes for the 4th Fog node.  
 - The selectivity of operator: 0.2 - 1.8 follwing the uniform distribution
 - The upper threshold for the cloud bandwidth usage: 260000.0
 - The lower threshold for the cloud bandwidth usage: 130000.0

 It remains to run the Experiment for the ILP approach.


1.3. Uniform Resource capacity,Static Graph and Dynamic data Stream rate

 - 8 operators in the application graph: 1 - 14 
 - 4 Edge nodes producing data streams: 4
 - 4 Fog nodes receiving, processing the prodcudes data stream and transmitting processed data stream to the Cloud
 - Each fog node has the resource capacity of 1024000 Kbytes.

 - 256000 Kbytes for all the data stream rates arriving to the distributed as follows: 64000 Kbytes for the 1st Fog node, 48000 Kbytes for the 2nd Fog node, 36000 Kbytes for the 3rd Fog node and 108000 Kbytes for the 4th Fog node.  

 - The selectivity of operator: 0.2 - 1.8 follwing the uniform distribution
 - The upper threshold for the cloud bandwidth usage: 260000.0
 - The lower threshold for the cloud bandwidth usage: 130000.0

The data stream, upper and lower thresholds are is increaing from 1 to 10.

 It remains to run the Experiment for the ILP approach.


1.4. Heterogeneous Resource capacity,Static Graph and Dynamic data Stream rate
