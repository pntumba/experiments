import matplotlib.pyplot as plt
import numpy as np

operators = ['Source S_j','O_1','O_2','O_3','O_4','O_5','O_6','O_7', 'O_8']
CONTRIBUTION_CRU = [0,0.1327586207,0.2486206897,0.3364827586,0.3932551724,0.4419172414,0.4678703448,0.4789004138,0.4859077517]
CONTRIBUTION_NRU = [0.4827586207,0.3862068966,0.2703448276,0.1622068966,0.1297655172,0.06488275862,0.02595310345,0.01557186207,0.01090030345]
CONTRIBUTION_RU = [0.4827586207,0.5189655172,0.5189655172,0.4986896552,0.5230206897,0.5068,0.4938234483,0.4944722759,0.4968080552]

fig, ax = plt.subplots()
ax.plot(operators, CONTRIBUTION_CRU, "-*b", label="CRU_j", markersize=8)
ax.plot(operators, CONTRIBUTION_NRU, "-+r", label="NRU_j",markersize=8)
ax.plot(operators, CONTRIBUTION_RU, "-y", label="RU_j",markersize=8)
ax.legend()
plt.ylabel('Resource usage costs')
plt.xlabel('Gim_j includes operators up to this one')
plt.show()


