import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [512,700,965,1221,1500,1792,2000,2100,2256]
RU_SOOCPLEX = [0.2562343189,0.3525506466,0.4920170449,0.6458146271,0.8247457121,1.00400797,1.135569426,1.179290033,1.318193242]
RU_RCS = [0.3531034483,0.4827586207,0.6652678644,0.8270264177,0.9755499674,1.145192046,1.154600681,1.286870656,1.380423183]

fig, ax = plt.subplots()
ax.plot(data_stream_rates, RU_SOOCPLEX, "--^b", label="SOO-CPLEX", markersize=8, markevery=1)
ax.plot(data_stream_rates, RU_RCS, "-ok", label="RCS", markersize=8)
ax.legend()
plt.ylabel('Resource usage costs- RU')
plt.xlabel('Data stream rate produced at the Edge (KB/sec)')
plt.show()


