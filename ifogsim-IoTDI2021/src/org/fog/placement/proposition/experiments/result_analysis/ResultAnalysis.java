package org.fog.placement.proposition.experiments.result_analysis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class ResultAnalysis{

	private final static String path = "/home/pntumba/inria_code/repositories/Experiments/IoTDI2021/ifogsim-IoTDI2021/src/org/fog/placement/proposition/experiments/";

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
	   String directory ="output/";
	  //String directory ="Results/RCS_2/"; 
	  
	  String  pattern = "Experiment_RCS_10_2256.0";
	  pattern = "Experiment_SOO_CPLEX_10_10000.0";
	  //pattern  = "Experiment_SOO_10_10000.0";
      
      
       String file_pattern = directory+pattern;
		readJsonFile(file_pattern);
	}

	public static void readJsonFile(String pattern) throws IOException {
		
		for (int i = 1; i <= 15; i++){

			JSONObject metric_object = new JSONObject();
			ArrayList<String> header = new ArrayList<String>();
			int countFiles = 0;
			String jsonFile = path+pattern + "_" + i + ".json";

			try {

				File isFile = new File(jsonFile);
				if (!isFile.exists()) {
					System.out.println("Can not find "+pattern + "_" + i + ".json");
					continue;
				}
				JSONObject object = (JSONObject) JSONValue.parse(new FileReader(jsonFile));
				JSONArray metrics = (JSONArray) object.get("metrics");
				for (int index = 0; index < metrics.size(); index++) {

					JSONObject obj = (JSONObject) metrics.get(index);
					metric_object.putAll(obj);
				}
				double _B = new BigDecimal((double) (metric_object.get("B"))).doubleValue();
				double _nru = new BigDecimal((double) (metric_object.get("nru"))).doubleValue();
				double _NRU = new BigDecimal((double) (metric_object.get("NRU"))).doubleValue();
				double _cru = new BigDecimal((double) (metric_object.get("cru"))).doubleValue();
				double _CRU = new BigDecimal((double) (metric_object.get("CRU"))).doubleValue();
				double _RU = new BigDecimal((double) (metric_object.get("RU"))).doubleValue();
				
				long RecTime = new BigDecimal((long) (metric_object.get("RecTime"))).longValue();

				JSONObject cmu = (JSONObject) object.get("cmu");

				double cmu_1 = new BigDecimal((double) (cmu.get("Fog_1"))).doubleValue();
				double cmu_2 = new BigDecimal((double) (cmu.get("Fog_2"))).doubleValue();
				double cmu_3 = new BigDecimal((double) (cmu.get("Fog_3"))).doubleValue();
				double cmu_4 = new BigDecimal((double) (cmu.get("Fog_4"))).doubleValue();
				int fog_count = 4;
				if (cmu_1 == 0.0) {
					fog_count = fog_count - 1;
				}
				if (cmu_2 == 0.0) {
					fog_count = fog_count - 1;
				}
				if (cmu_3 == 0.0) {
					fog_count = fog_count - 1;
				}
				if (cmu_4 == 0.0) {
					fog_count = fog_count - 1;
				}
				System.out.println(_B + "\t" + _RU + "\t" + _CRU + "\t" + _NRU+"\t"+fog_count+"\t"+RecTime);
				//System.out.println("B="+_B + "\t" +"RU="+_RU + "\t" +"CRU="+_CRU + "\t" +"NRU="+_NRU);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
