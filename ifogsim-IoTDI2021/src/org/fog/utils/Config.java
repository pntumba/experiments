package org.fog.utils;

public class Config {

	public static final double RESOURCE_MGMT_INTERVAL = 100;
	public static  double HANDLE_DATASTREAM_LOAD_INTERVAL = 0;
	public static int MAX_SIMULATION_TIME = 100000000;
	//public static int MAX_SIMULATION_TIME = 10000000;
	public static int RESOURCE_MANAGE_INTERVAL = 100;
	public static int RESOURCE_MONITORING_INTERVAL = 1000000;
	public static String FOG_DEVICE_ARCH = "x86";
	public static String FOG_DEVICE_OS = "Linux";
	public static String FOG_DEVICE_VMM = "Xen";
	public static double FOG_DEVICE_TIMEZONE = 10.0;
	public static double FOG_DEVICE_COST = 3.0;
	public static double FOG_DEVICE_COST_PER_MEMORY = 0.05;
	public static double FOG_DEVICE_COST_PER_STORAGE = 0.001;
	public static double FOG_DEVICE_COST_PER_BW = 0.0;
	public static boolean USE_APPLICATION_STATE = false;
	public static boolean APPLICATION_STATE_FILE_EXISTS = false;
	public static boolean STATIC_OPERATOR_PARAMETER = false;
	public static boolean REUSE_DEPLOYMENT = false;
}
