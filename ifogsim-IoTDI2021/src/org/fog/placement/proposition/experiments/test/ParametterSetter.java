package org.fog.placement.proposition.experiments.test;

import java.util.ArrayList;

import org.fog.placement.proposition.utils.StaticVariables;

public class ParametterSetter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double total_data_stream_ratio = getRatio(StaticVariables.source_number); 
		double newBmax = getNewBmax(total_data_stream_ratio);
		double newBmin = getNewBmin(newBmax);
		System.out.println("ratio="+total_data_stream_ratio);
		System.out.println("newBmax="+newBmax);
		System.out.println("newBmin="+newBmin+"\n");
		totalDataStreamRates(total_data_stream_ratio);
	}
	
	public static double getRatio(double value) {
		
		return value/4;
		
	}
	
	public static double getNewBmax(double total_data_stream_ratio) {
		
		double Bmax = 1450;
		double max_csel_cost = 4.03;
		double max_csel = 0.8;
		double fog_capacity = 1280;
		//double new_Bmax =  (max_csel *  fog_capacity * fog_node_number)/max_csel_cost;
		double new_Bmax = Bmax * total_data_stream_ratio;
		return Math.ceil(new_Bmax);
	}
	
	public static double getNewBmin(double new_Bmax) {
		
		double Bmax = 1450;
		double Bmin = 800;
		double new_Bmin =  Bmin/Bmax * new_Bmax;
		return Math.ceil(new_Bmin);
	}
	
	
	public static void totalDataStreamRates(double ratio){
		// TODO Auto-generated method stub
	
		ArrayList<Double> totalDataStreamRates = new ArrayList<Double>();
		totalDataStreamRates.add(512.0 * ratio);
		totalDataStreamRates.add(700.0 * ratio);
	    totalDataStreamRates.add(965.0 * ratio);
	   
		totalDataStreamRates.add(1221.0 * ratio);
		
		totalDataStreamRates.add(1500.0 * ratio);
		totalDataStreamRates.add(1792.0 * ratio);
		
		
		totalDataStreamRates.add(2000.0 * ratio);
		totalDataStreamRates.add(2100.0 * ratio);
		
		totalDataStreamRates.add(2256.0 * ratio);
		
		
		for(double total_data_rate : totalDataStreamRates) {
			
			System.out.print("add(\""+(int)Math.ceil(total_data_rate)+"\");");
			
		}
		
	}

}
