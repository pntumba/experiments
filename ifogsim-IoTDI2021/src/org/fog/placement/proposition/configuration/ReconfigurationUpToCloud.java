package org.fog.placement.proposition.configuration;

import javax.lang.model.type.UnionType;

import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppEdge;
import org.fog.application.selectivity.FractionalSelectivity;
import org.fog.entities.Tuple;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.utils.DynamicTupleSize;

public class ReconfigurationUpToCloud {
	
	private Configuration rec = null;
	public ReconfigurationUpToCloud(Configuration reconfigure) {
		// TODO Auto-generated constructor stub
		this.rec = reconfigure;
	}
	
	public void redirectSource(Graph graphToReplicate, int S_j) {
		// TODO Auto-generated method stub
		
		Graph graphOnCloud = rec.getInitialGraph();
		String source_name = rec.getGraphToApplication().get(S_j);
		int vertex_source = S_j;
		int F_j = rec.getSourcesToResourceNodes().get(S_j);
		
		//Update the graph model representing the application
		graphOnCloud.addVertex(vertex_source);
		System.out.println("vertex_source="+vertex_source);
		System.out.println(graphToReplicate.getAdjVertices());
		for(int vertex_succ: graphToReplicate.getSuccessors(vertex_source)){
			
			//double datastream_rate = graphToReplicate.getEdgeWeight(vertex_source, vertex_succ);
			double datastream_rate = rec.getApplication().getEdgeMap().get(source_name).getTupleNwLength();
			graphOnCloud.addEdge(vertex_source, vertex_succ, datastream_rate);
			
			//Update the application by removing/adding edge for the data source
			String vertex_succ_name = rec.getGraphToApplication().get(vertex_succ);
			rec.removeAppEdge(source_name);
			for(int succ : graphToReplicate.getSuccessors(vertex_succ)){
				
				String rep_succ_name = rec.getReplicatedGraphToApplication().get(F_j).get(succ);
				rec.removeTuppleMapping(source_name, rep_succ_name);
			}
			rec.getApplication().addAppEdge(source_name, vertex_succ_name, 2, datastream_rate, source_name, Tuple.UP, AppEdge.SENSOR);
			rec.getApplication().addTupleMapping(vertex_succ_name, source_name, vertex_succ_name, new FractionalSelectivity(1.0));
		}
		
	}
	
	public void deleteReplicatedOperators(Graph graphToReplicate, int S_j){
		
		int F_j = rec.getSourcesToResourceNodes().get(S_j);
		Graph graphOnCloud = rec.getInitialGraph();
		for(int vertex1: graphToReplicate.getAdjVertices().keySet()){
				
				if(graphToReplicate.isSource(vertex1) || rec.getOperatorsType().get(vertex1).equals(ModuleType.UNION)) {
					continue;
				}
				
				String rep_vertex1_name = rec.getReplicatedGraphToApplication().get(F_j).get(vertex1);
				for(int vertex2 : graphToReplicate.getSuccessors(vertex1)) {
					
					if(graphToReplicate.getAdjVertices().containsKey(vertex2)){
						
						String rep_vertex2_name = rec.getReplicatedGraphToApplication().get(F_j).get(vertex2);
						if(graphOnCloud.getAdjVertices(vertex1).containsKey(vertex2)){
							
							rec.removeTuppleMapping(rep_vertex1_name, rep_vertex2_name);
							
							AppEdge appEdge = rec.removeAppEdge(rep_vertex1_name);
							if(appEdge != null){
								
								String vertex1_name = rec.getGraphToApplication().get(vertex1);
								AppEdge appEdgeInCloud = rec.getApplication().getEdgeMap().get(vertex1_name);
								rec.getApplication().getEdgeMap().get(vertex1_name).setTupleNwLength(appEdgeInCloud.getTupleNwLength()+appEdge.getTupleNwLength());
								
							}
						}
						else{
							
							int vertex1_succ_cloud = -1;
							for(int succ : graphOnCloud.getSuccessors(vertex1)) {
								
								if(graphOnCloud.getSuccessors(succ).contains(vertex2)) {
									
									vertex1_succ_cloud = succ;
								}	
							}
							
							rec.removeTuppleMapping(rep_vertex1_name, rep_vertex2_name);
							
							AppEdge appEdge = rec.removeAppEdge(rep_vertex1_name);
							
							String vertex1_name = rec.getGraphToApplication().get(vertex1);
							String vertex2_name = rec.getGraphToApplication().get(vertex2);
							String vertex1_succ_cloud_name = rec.getGraphToApplication().get(vertex1_succ_cloud);
							
							AppEdge appEdgeInCloud1 = rec.getApplication().getEdgeMap().get(vertex1_name);
							AppEdge appEdgeInCloud2 = rec.getApplication().getEdgeMap().get(vertex1_succ_cloud_name);
							
							
							if(vertex1_succ_cloud_name == null) {
								
								vertex1_succ_cloud_name = appEdgeInCloud1.getDestination();
								appEdgeInCloud2 = rec.getApplication().getEdgeMap().get(vertex1_succ_cloud_name);
							}
							rec.getApplication().getEdgeMap().get(vertex1_name).setTupleNwLength(appEdgeInCloud1.getTupleNwLength()+appEdge.getTupleNwLength());
							rec.getApplication().getEdgeMap().get(vertex1_succ_cloud_name).setTupleNwLength(appEdgeInCloud2.getTupleNwLength()+appEdge.getTupleNwLength());
						}
						rec.addOperatorToRemovingModel(F_j, rep_vertex1_name);
					}
					else{
						
						System.out.println("rec.getCountConnectedReplicatedGraphs="+rec.getCountConnectedReplicatedGraphs());
						if(rec.getCountConnectedReplicatedGraphs().get(vertex2) > 1){
						
						
						int count = rec.getCountConnectedReplicatedGraphs().remove(vertex2)-1;
						rec.getCountConnectedReplicatedGraphs().put(vertex2, count);
						String vertex2_name = rec.getGraphToApplication().get(vertex2);
						AppEdge rep_appEdge = rec.removeAppEdge(rep_vertex1_name);
						rec.removeTuppleMapping(rep_vertex1_name, vertex2_name);
						
						
						String vertex2_pred_name = rec.getGraphToApplication().get(vertex1);
						AppEdge appEdge = rec.getApplication().getEdgeMap().get(vertex2_pred_name);
						rec.getApplication().getEdgeMap().get(vertex2_pred_name).setTupleNwLength(appEdge.getTupleNwLength()+rep_appEdge.getTupleNwLength());
						
						appEdge = rec.getApplication().getEdgeMap().get(vertex2_name);
						rec.getApplication().getEdgeMap().get(vertex2_name).setTupleNwLength(appEdge.getTupleNwLength()+rep_appEdge.getTupleNwLength());
						
					}else {
								
							rec.getCountConnectedReplicatedGraphs().remove(vertex2);
							graphOnCloud.removeEdge(vertex1, vertex2);
							int vertex2_pred = vertex1;
							String vertex2_pred_name = rec.getGraphToApplication().get(vertex1);
							int vertex2_succ = -1;
							for(int succ : graphOnCloud.getSuccessors(vertex2)) {
								
								vertex2_succ = succ;
							}
							String vertex2_succ_name = rec.getGraphToApplication().get(vertex2_succ);
							String vertex2_name = rec.getGraphToApplication().get(vertex2);
							
							AppEdge rep_appEdge = rec.removeAppEdge(rep_vertex1_name);
							rec.removeTuppleMapping(rep_vertex1_name, vertex2_name);
							
							AppEdge appEdge = rec.removeAppEdge(vertex2_pred_name);
							rec.removeAppEdge(vertex2_name);
							if(appEdge != null) {
								
								rec.getApplication().addAppEdge(vertex2_pred_name,vertex2_succ_name, 2, appEdge.getTupleNwLength()+rep_appEdge.getTupleNwLength(), vertex2_pred_name, Tuple.UP, AppEdge.SENSOR);
								
							}
							rec.removeTuppleMapping(vertex2_pred_name, vertex2_name);
							rec.removeTuppleMapping(vertex2_name, vertex2_succ_name);
							
							double selectivity = rec.getOperatorsSelectivity().get(vertex2);
							rec.getApplication().addTupleMapping(vertex2_succ_name, vertex2_pred_name, vertex2_succ_name, new FractionalSelectivity(selectivity));
							rec.addOperatorToRemovingModel(CloudSim.getEntityId("cloud"), vertex2_name);
							graphOnCloud.removeEdge(vertex2_pred, vertex2);
							graphOnCloud.removeEdge(vertex2, vertex2_succ);
							graphOnCloud.removeVertex(vertex2);
							graphOnCloud.addEdge(vertex2_pred, vertex2_succ, 0.0);
					
					}
					rec.addOperatorToRemovingModel(F_j, rep_vertex1_name);	
				}
			}
			
			}
		rec.getReplicatedGraphToApplication().remove(F_j);
		}

}
