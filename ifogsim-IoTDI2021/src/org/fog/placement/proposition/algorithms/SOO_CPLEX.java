package org.fog.placement.proposition.algorithms;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.entities.FogDevice;
import org.fog.placement.proposition.utils.EdgeCut;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.MinMax;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.placement.proposition.utils.UtilFunctions;

import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloNumVarType;
import ilog.cplex.IloCplex;
import ilog.concert.*;
import ilog.cplex.*;

public class SOO_CPLEX {

	private Graph graph;
	private Graph graph_rep;
	private double B;
	private double Bmax;
	private Map<Integer, Double> streamsSet;
	private Map<Integer, Double> cumulatedSelectivitiesForMigratedStreams;
	private Map<Integer, Double> operatorCosts;
	private Map<Integer, Graph> initialMapping = new HashMap<Integer, Graph>();
	private Map<Integer, Integer> sourcesToResourceNodes;
	private Map<Integer, ModuleType> operatorsType;
	private Map<Integer, Double> operatorsSelectivity;
	private Map<Integer, Boolean> operatorsReplicability;
	private Map<Integer, Integer> replicationAndMigrationPoints = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, Map<Integer, EdgeCut>> allEdgeCuts = null;
	private Map<Integer, Map<Integer, Double>> allEdgeCutValues = null;

	private double RU = 0.0;
	private double w_c;
	private double w_n;
	private double cru = 0.0;
	private double nru = 0.0;
	private double CRU = 0.0;
	private double NRU = 0.0;
	private MinMax compMinMax = UtilFunctions.compMinMax;
	private MinMax netwMinMax = UtilFunctions.netwMinMax;

	public SOO_CPLEX(Graph graph, Map<Integer, ModuleType> operatorsType, Map<Integer, Double> operatorsSelectivity,
			Map<Integer, Double> operatorCosts, Map<Integer, Boolean> operatorsReplicability) {

		this.graph = graph;
		this.operatorsReplicability = operatorsReplicability;
		this.operatorsSelectivity = operatorsSelectivity;
		this.operatorsType = operatorsType;
		this.cumulatedSelectivitiesForMigratedStreams = new ConcurrentHashMap<Integer, Double>();
		this.operatorCosts = operatorCosts;
		compMinMax = UtilFunctions.compMinMax;
		netwMinMax = UtilFunctions.netwMinMax;
	}

	private int[] modelAndsolveWithCplex(double[] source_data_stream_rate, double[] max_fog_comp_resources,
			double[] csel_edge_cut, Map<Integer, Map<Integer, Double>> cmuPerEdgeCutPerGraph) {

		int N = source_data_stream_rate.length;
		int K = csel_edge_cut.length;
		int[] rep_mig_points = new int[N];
		double[][] fog_cpumemory_usage = new double[N][K];
		double[][] fog_to_cloud_bwd_usage = new double[N][K];
        System.out.println("csel_edge_cut="+Arrays.toString(csel_edge_cut));
        System.out.println("source_data_stream_rate="+Arrays.toString(source_data_stream_rate));
        System.out.println("max_fog_comp_resources="+Arrays.toString(max_fog_comp_resources));
        System.out.println("K="+K);
        System.out.println("cmuPerEdgeCutPerGraph="+cmuPerEdgeCutPerGraph);
        System.out.println("operatorCosts="+operatorCosts);
        System.out.println("operatorsSelectivity="+operatorsSelectivity);
		try{
			
			IloCplex cplex = new IloCplex();
			  // Turn of presolve to make the output more interesting
			//cplex.setParam(IloCplex.Param.Preprocessing.Presolve, false);
		
			for (int j = 0; j < N; j++){

				double cmu = 0.0;
				for (int k = 0; k < K; k++) {
					double nbu = source_data_stream_rate[j] * csel_edge_cut[k];
					fog_to_cloud_bwd_usage[j][k] = nbu;

					if (k == 0) {

						fog_cpumemory_usage[j][k] = cmuPerEdgeCutPerGraph.get(j).get(k);
						fog_to_cloud_bwd_usage[j][k] = source_data_stream_rate[j] * csel_edge_cut[k];
						System.out.print("\n|S"+(j+1)+"|= (nbu="+source_data_stream_rate[j]+", cmu="+fog_cpumemory_usage[j][k]+"\t");

					}else{

						fog_cpumemory_usage[j][k] = cmuPerEdgeCutPerGraph.get(j).get(k);
						fog_to_cloud_bwd_usage[j][k] = source_data_stream_rate[j] * csel_edge_cut[k];
						System.out.print("(nbu="+fog_to_cloud_bwd_usage[j][k]+",cmu="+fog_cpumemory_usage[j][k]+")\t");
					}
					
				}
			}
			System.out.println("\n");
			IloNumVar[][] X = new IloNumVar[N][];
			for (int j = 0; j < N; j++) {
								
				X[j] = cplex.numVarArray(K, 0, 1, IloNumVarType.Int);
				//X[j] = cplex.boolVarArray(K);

				// Constraint for the decision variable
				//cplex.addEq(cplex.sum(X[j]), 1);
				cplex.addEq(cplex.sum(X[j]), 1);

			}

			IloLinearNumExpr[] expr_cmu = new IloLinearNumExpr[N];
			IloLinearNumExpr[] expr_nbu = new IloLinearNumExpr[N];
			for (int j = 0; j < N; j++) {

				expr_cmu[j] = cplex.linearNumExpr();
				expr_nbu[j] = cplex.linearNumExpr();
				for (int k = 0; k < K; k++) {

					expr_cmu[j].addTerm(fog_cpumemory_usage[j][k], X[j][k]);
					expr_nbu[j].addTerm(fog_to_cloud_bwd_usage[j][k], X[j][k]);
				}

			}

			// Overall computational resource usage
			IloNumExpr cru = cplex.numExpr();

			// Overall network resource usage
			IloNumExpr nru = cplex.numExpr();

			for (int j = 0; j < N; j++) {

				cru = cplex.sum(cru, cplex.prod(1 / (max_fog_comp_resources[j] * N), expr_cmu[j]));
				nru = cplex.sum(nru, cplex.prod(1 / Bmax, expr_nbu[j]));

				// Constraint for the computational resource usage

				cplex.addLe(expr_cmu[j], max_fog_comp_resources[j]);
			}
			// Constraint for the network resource usage
		
			cplex.addLe(cplex.sum(expr_nbu), Bmax);
			
			// Normalization of cru
			IloNumExpr CRU = cplex.numExpr();
			CRU = cplex.prod(1, cru);

			// Normalization of nru
			IloNumExpr NRU = cplex.numExpr();
			NRU = cplex.prod(1, nru);

			// Objective: minimize w_c * CRU + w_n * NRU
			cplex.addMinimize(cplex.sum(cplex.prod(w_n, NRU), cplex.prod(w_c, CRU)));
			
			//https://www.ibm.com/support/pages/deciding-which-cplexs-numerous-linear-programming-algorithms-fastest-performance 
		
			
			  /* Set the solution pool relative gap parameter to obtain solutions
            of objective value within 10% of the optimal: reading https://www.ibm.com/support/pages/using-cplex-examine-alternate-optimal-solutions */
		
			
            cplex.setParam(IloCplex.Param.MIP.Pool.Capacity, 2100000000);
			cplex.setParam(IloCplex.Param.MIP.Pool.Intensity, 4);
			cplex.setParam(IloCplex.Param.MIP.Pool.Replace, 1);
			cplex.setParam(IloCplex.Param.MIP.Limits.Populate, 2100000000);
            
			//cplex.setParam(IloCplex.Param.Emphasis.MIP, 1);
			// cplex.setParam(IloCplex.Param.Feasopt.Tolerance, 0.0001);
			// http://www-eio.upc.es/lceio/manuals/cplex-11/html/usrcplex/solveMIP10.html
            cplex.setParam(IloCplex.Param.MIP.Tolerances.MIPGap, 0.5);
            //cplex.setParam(IloCplex.Param.Simplex.Tolerances.Optimality,  1e-6);
            
			if(cplex.solve()){
				
				System.out.println("Solution status: " + cplex.getStatus());
				System.out.println("NRU=" + cplex.getValue(NRU) + "\tB=" + cplex.getValue(cplex.sum(expr_nbu)));
				System.out.print("CRU=" + cplex.getValue(CRU));
				for (int j = 0; j < N; j++) {

					System.out.print("\tcmu_F" + (j + 1) + "=" + cplex.getValue(expr_cmu[j]));
				}
				System.out.println("\nRU " + cplex.getObjValue());
				for (int j = 0; j < N; j++) {
					System.out.print("   S_" + (j + 1) + ": ");
					int edgeCutIndex = 1;
					for (int k = 0; k < K; k++) {

						if (cplex.getValue(X[j][k]) >= 0.6) {

							rep_mig_points[j] = edgeCutIndex;
						//rep_mig_points[j] = (int) cplex.getValue(X[j][k]);
						}
						edgeCutIndex++;
						System.out.print(cplex.getValue(X[j][k]) + "\t");

					}
					System.out.println();
				}
				System.out.println("Algorithm "+cplex.getAlgorithm());
				cplex.getAlgorithm();
			}
			//cplex.exportModel("file_name.lp");
			System.out.println("Maximum bound violation = " + cplex.getQuality(IloCplex.QualityType.MaxPrimalInfeas));
			//cplex.writeSolutions("file_name.xml");

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			// System.err.println("Concert exception caught: " + );
		}
		 
		return rep_mig_points;
	}

	public void initialConfiguration(double nd_FC, double nd_EF, double nb_F_jC, Map<Integer, Double> streamsSet) {

		allEdgeCuts = new ConcurrentHashMap<Integer, Map<Integer, EdgeCut>>();
		allEdgeCutValues = new ConcurrentHashMap<Integer, Map<Integer, Double>>();
		this.Bmax = nb_F_jC;
		this.streamsSet = streamsSet;

		double[] source_data_stream_rate = new double[streamsSet.size()];
		double[] max_fog_comp_resources = new double[streamsSet.size()];
		double[] csel_edge_cut;
		Map<Integer, Integer> indexToStreamId = new ConcurrentHashMap<Integer, Integer>();
		Map<Integer, Map<Integer, Double>> cmuPerEdgeCutPerGraph = new ConcurrentHashMap<Integer, Map<Integer,Double>>();
		int i = 0;
		for (int S_j : streamsSet.keySet()){

			int F_j = getSourcesToResourceNodes().get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			int memory_j = fogDevice.getHost().getRamProvisioner().getAvailableRam();

			indexToStreamId.put(i, S_j);
			source_data_stream_rate[i] = streamsSet.get(S_j);
			max_fog_comp_resources[i] = memory_j;
			
			this.graph_rep = UtilFunctions.replicableGraph(graph, operatorsReplicability);
			int sink = graph_rep.getSink();
			int source = S_j;
			UtilFunctions.setAvailableMemory(memory_j);
			Graph graph_candidat = UtilFunctions.candidatSubGraph(graph_rep, source, sink, operatorsReplicability);
			UtilFunctions.cmuPerEdgeCut(graph_candidat, operatorsType, operatorsSelectivity,operatorCosts, source, streamsSet.get(S_j));
			Map<Integer, Double> cmuPerEdgeCut = new ConcurrentHashMap<Integer, Double>(UtilFunctions.getCmuPerEdgeCut());
			cmuPerEdgeCutPerGraph.put(i, cmuPerEdgeCut);
			i++;
		}

		int source = indexToStreamId.get(0);
		this.graph_rep = UtilFunctions.replicableGraph(graph, operatorsReplicability);
		int sink = graph_rep.getSink();
		UtilFunctions.setAvailableMemory(max_fog_comp_resources[0]);
		Graph graph_candidat = UtilFunctions.candidatSubGraph(graph_rep, source, sink, operatorsReplicability);
		double stream = 1;
		UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_candidat, operatorsType, operatorsSelectivity,operatorCosts, source, stream);
		Graph graph_sat_j = UtilFunctions.getGraphSatRequirement();
		UtilFunctions.findAllEdgeCuts(graph_sat_j, operatorsSelectivity);
		Map<Integer, EdgeCut> edgeCuts = new ConcurrentHashMap<Integer, EdgeCut>();
		Map<Integer, Double> edgeCutValues = new ConcurrentHashMap<Integer, Double>();
		
		Map<Integer, Double> cumulatedOperatorCosts = new ConcurrentHashMap<Integer, Double>();
		csel_edge_cut = new double[UtilFunctions.getAllEdgeCutValues().size() - 1];
		i=0;
		for (int count : UtilFunctions.getAllEdgeCutValues().keySet()){
			
			if (count > 1){

				edgeCutValues.put(count - 1, UtilFunctions.getAllEdgeCutValues().get(count));
				edgeCuts.put(count - 1, UtilFunctions.getAllEdgeCuts().get(count));
				csel_edge_cut[i] = UtilFunctions.getAllEdgeCutValues().get(count);	
				i++;
			}
		}

		int[] rep_mig_points = modelAndsolveWithCplex(source_data_stream_rate, max_fog_comp_resources, csel_edge_cut, cmuPerEdgeCutPerGraph);
		System.out.println("rep_mig_points="+Arrays.toString(rep_mig_points));
		for (i = 0; i < rep_mig_points.length; i++){
			
			int edgeCutIndex = rep_mig_points[i];
			if(edgeCutIndex==1) {
				continue;
			}
			int S_j = indexToStreamId.get(i);
			stream = streamsSet.get(S_j);
			int F_j = getSourcesToResourceNodes().get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			UtilFunctions.setAvailableMemory(fogDevice.getHost().getRamProvisioner().getAvailableRam());
			graph_candidat = UtilFunctions.candidatSubGraph(graph_rep, S_j, sink, operatorsReplicability);
			UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_candidat, operatorsType, operatorsSelectivity,operatorCosts,S_j, stream);
			graph_sat_j = UtilFunctions.getGraphSatRequirement();
			
			EdgeCut edgeCut = edgeCuts.get(edgeCutIndex);
			Graph graph_to_replicate = UtilFunctions.candidatGraphToReplicate(graph_sat_j, S_j, edgeCut.getEdgeCut());
			UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_to_replicate, operatorsType,operatorsSelectivity,operatorCosts, S_j, stream);
			Graph graph_mig_j = UtilFunctions.getGraphSatRequirement();
			getCumulatedSelectivitiesForMigratedStreams().put(S_j, UtilFunctions.getCumulatedSelectivity());
			initialMapping.put(F_j, graph_mig_j);
		}

	}

	public double getW_c() {
		return w_c;
	}

	public void setW_c(double w_c) {
		this.w_c = w_c;
	}

	public double getW_n() {
		return w_n;
	}

	public void setW_n(double w_n) {
		this.w_n = w_n;
	}

	public Map<Integer, Integer> getSourcesToResourceNodes() {
		return sourcesToResourceNodes;
	}

	public void setSourcesToResourceNodes(Map<Integer, Integer> sourcesToResourceNodes) {
		this.sourcesToResourceNodes = sourcesToResourceNodes;
	}

	public Map<Integer, Double> getStreamsSet() {
		return streamsSet;
	}

	public void setStreamsSet(Map<Integer, Double> streamsSet) {

		this.streamsSet = streamsSet;
	}

	public Map<Integer, Double> getCumulatedSelectivitiesForMigratedStreams() {
		return cumulatedSelectivitiesForMigratedStreams;
	}

	public void setCumulatedSelectivitiesForMigratedStreams(
			Map<Integer, Double> cumulatedSelectivitiesForMigratedSreams) {
		this.cumulatedSelectivitiesForMigratedStreams = cumulatedSelectivitiesForMigratedSreams;
	}

	public Map<Integer, Graph> getInitialMapping() {
		return initialMapping;
	}

	public void setInitialMapping(Map<Integer, Graph> initialMapping) {
		this.initialMapping = initialMapping;
	}

	public Map<Integer, Map<Integer, Double>> getAllEdgeCutValues() {
		return allEdgeCutValues;
	}

	public void setAllEdgeCutValues(Map<Integer, Map<Integer, Double>> allEdgeCutValues) {
		this.allEdgeCutValues = allEdgeCutValues;
	}
}
