package org.fog.placement;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;
import org.fog.application.AppEdge;
import org.fog.application.AppLoop;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.entities.Actuator;
import org.fog.entities.FogBroker;
import org.fog.entities.FogDevice;
import org.fog.entities.Sensor;
import org.fog.placement.proposition.configuration.ApplicationState;
import org.fog.placement.proposition.monitoring.CostModel;
import org.fog.placement.proposition.utils.ALGORITHM;
import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.placement.proposition.utils.Triggers;
import org.fog.placement.proposition.utils.UtilFunctions;
import org.fog.utils.Config;
import org.fog.utils.DynamicTupleSize;
import org.fog.utils.FogEvents;
import org.fog.utils.FogUtils;
import org.fog.utils.JsonToApplication;
import org.fog.utils.NetworkUsageMonitor;
import org.fog.utils.TimeKeeper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Controller extends SimEntity {

	public static boolean ONLY_CLOUD = false;

	private List<FogDevice> fogDevices;
	private List<Sensor> sensors;
	private List<Actuator> actuators;

	private Map<String, Application> applications;
	private Map<String, Integer> appLaunchDelays;

	private Map<String, ModulePlacement> appModulePlacementPolicy;
	private Map<Integer, List<String>> modulesScheduled;

	public Controller(String name, List<FogDevice> fogDevices, List<Sensor> sensors, List<Actuator> actuators) {
		super(name);
		this.applications = new HashMap<String, Application>();
		setAppLaunchDelays(new HashMap<String, Integer>());
		setAppModulePlacementPolicy(new HashMap<String, ModulePlacement>());
		for (FogDevice fogDevice : fogDevices) {

			fogDevice.setControllerId(getId());
		}
		setFogDevices(fogDevices);
		setActuators(actuators);
		setSensors(sensors);
		connectWithLatencies();
	}

	private FogDevice getFogDeviceById(int id) {
		for (FogDevice fogDevice : getFogDevices()) {
			if (id == fogDevice.getId())
				return fogDevice;
		}
		return null;
	}

	private void connectWithLatencies() {
		for (FogDevice fogDevice : getFogDevices()) {
			FogDevice parent = getFogDeviceById(fogDevice.getParentId());
			if (parent == null)
				continue;
			double latency = fogDevice.getUplinkLatency();
			parent.getChildToLatencyMap().put(fogDevice.getId(), latency);
			parent.getChildrenIds().add(fogDevice.getId());
		}
	}

	@Override
	public void startEntity() {
		for (String appId : applications.keySet()) {
			if (getAppLaunchDelays().get(appId) == 0)
				processAppSubmit(applications.get(appId));
			else
				send(getId(), getAppLaunchDelays().get(appId), FogEvents.APP_SUBMIT, applications.get(appId));
		}

		send(getId(), Config.RESOURCE_MANAGE_INTERVAL, FogEvents.CONTROLLER_RESOURCE_MANAGE);

		send(getId(), Config.MAX_SIMULATION_TIME, FogEvents.STOP_SIMULATION);

		send(getId(), Config.RESOURCE_MONITORING_INTERVAL, FogEvents.HANDLE_RESOURCE_MONITORING);

		for (FogDevice dev : getFogDevices())
			sendNow(dev.getId(), FogEvents.RESOURCE_MGMT);

	}

	@Override
	public void processEvent(SimEvent ev) {
		switch (ev.getTag()) {
		case FogEvents.APP_SUBMIT:
			processAppSubmit(ev);
			break;
		case FogEvents.TUPLE_FINISHED:
			processTupleFinished(ev);
			break;
		case FogEvents.CONTROLLER_RESOURCE_MANAGE:
			manageResources();
			break;
		case FogEvents.HANDLE_RESOURCE_MONITORING:

			if (getAppModulePlacementPolicy().get("IoTDI2021").getUsedAlgorithm().equals(ALGORITHM.RCS)) {

				resourceUsageMonitoringRCS();

			} else if (getAppModulePlacementPolicy().get("IoTDI2021").getUsedAlgorithm().equals(ALGORITHM.SOO)) {

				resourceUsageMonitoringSOO();

			}

			else if (getAppModulePlacementPolicy().get("IoTDI2021").getUsedAlgorithm().equals(ALGORITHM.SOOV1)) {

				resourceUsageMonitoringSOOV1();
			}

			else if (getAppModulePlacementPolicy().get("IoTDI2021").getUsedAlgorithm().equals(ALGORITHM.ILP)) {

				resourceUsageMonitoringILP();
			}

			break;
		case FogEvents.STOP_SIMULATION:
			CloudSim.stopSimulation();
			printTimeDetails();
			printPowerDetails();
			printCostDetails();
			printNetworkUsageDetails();
			System.exit(0);
			break;
		case FogEvents.HANDLE_DATASTREAM_LOAD:
			// updateDataStreamLoad();
			break;

		}
	}

	private void resourceUsageMonitoringILP() {
		// TODO Auto-generated method stub
		currentResourceUsageCosts();

		send(getId(), Config.RESOURCE_MONITORING_INTERVAL, FogEvents.HANDLE_RESOURCE_MONITORING);
	}

	private void resourceUsageMonitoringSOO(){
		// TODO Auto-generated method stub
		currentResourceUsageCosts();
		send(getId(), Config.RESOURCE_MONITORING_INTERVAL, FogEvents.HANDLE_RESOURCE_MONITORING);

	}
	
	
	private void resourceUsageMonitoringSOOV1() {
		// TODO Auto-generated method stub
		currentResourceUsageCosts();
		send(getId(), Config.RESOURCE_MONITORING_INTERVAL, FogEvents.HANDLE_RESOURCE_MONITORING);

	}
	
	private void resourceUsageMonitoringRCS() {

		currentResourceUsageCosts();

		ModulePlacementRCS modulePlacement = (ModulePlacementRCS) getAppModulePlacementPolicy().get("IoTDI2021");
		double fog_cloud_netw_bwd_usage = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
		double fog_cloud_upper_threshold_netw_bwd = CostModel.getFog_to_cloud_upper_threshold_netw_bwd();

		Map<Integer, Double> streamsSet = CostModel.getFog_to_cloud_netw_bwd_usage();

		CloudSim.pauseSimulation();

		if (CostModel.getFog_to_cloud_overall_netw_bwd_usage() > CostModel.getFog_to_cloud_upper_threshold_netw_bwd()) {

			System.out.println(
					"Bandwidth usage get upper than Bmax=" + CostModel.getFog_to_cloud_upper_threshold_netw_bwd());
			if (!UtilFunctions.getWatchDog()) {

				modulePlacement.dynamicMapModules(Triggers.MIGRATE_TOFOG, 1.0, 1.0, fog_cloud_netw_bwd_usage,
						fog_cloud_upper_threshold_netw_bwd, streamsSet);
				mappingApplicationGRaphToResource("IoTDI2021");
				StaticVariables.counter = 0;
				StaticVariables.RU = 0;
				StaticVariables.NRU = 0;
				StaticVariables.CRU = 0;
				StaticVariables.B = 0;
				StaticVariables.nru = 0;
				StaticVariables.cru = 0;
			}

		}

		if (CostModel.getFog_to_cloud_overall_netw_bwd_usage() < CostModel.getFog_to_cloud_lower_threshold_netw_bwd()) {
			System.out.println(
					"Bandwidth usage get lower than Bmin=" + CostModel.getFog_to_cloud_lower_threshold_netw_bwd());
			modulePlacement.dynamicMapModules(Triggers.MIGRATE_BACK_INCLOUD, 1.0, 1.0, fog_cloud_netw_bwd_usage,
					fog_cloud_upper_threshold_netw_bwd, streamsSet);
			if (!modulePlacement.isEmptyGraphToRemove()) {

				removingApplicationGRaphToResource("IoTDI2021");
				StaticVariables.counter = 0;
				StaticVariables.RU = 0;
				StaticVariables.NRU = 0;
				StaticVariables.CRU = 0;
				StaticVariables.B = 0;
				StaticVariables.nru = 0;
				StaticVariables.cru = 0;
			}

		}

		if (!StaticVariables.exeedAvailableComputationalResources.isEmpty()) {

			modulePlacement.dynamicMapModules(Triggers.ADJUST_EDGECUT, 1.0, 1.0, fog_cloud_netw_bwd_usage,
					fog_cloud_upper_threshold_netw_bwd, StaticVariables.exeedAvailableComputationalResources);
			StaticVariables.counter = 0;
			StaticVariables.RU = 0;
			StaticVariables.NRU = 0;
			StaticVariables.CRU = 0;
			StaticVariables.B = 0;
			StaticVariables.nru = 0;
			StaticVariables.cru = 0;
		}

		// CostModel.getFog_cpu_memory_usage().clear();
		CostModel.getFog_comp_resource_usage_cost().clear();
		CostModel.getFog_to_cloud_netw_bwd_usage().clear();
		CostModel.getFog_to_cloud_netw_resource_usage_cost().clear();

		if (CloudSim.isPaused()){

			CloudSim.resumeSimulation();
		}
		send(getId(), Config.RESOURCE_MONITORING_INTERVAL, FogEvents.HANDLE_RESOURCE_MONITORING);
	}

	private void currentResourceUsageCosts() {

		CostModel.calculateComp_resource_usage_cost();
		CostModel.calculateNetw_resource_usage_cost();
		CostModel.calculateResource_usage_cost();
		StaticVariables.counter++;
		StaticVariables.RU = CostModel.getResource_usage_cost();
		StaticVariables.NRU = CostModel.getNorm_netw_resource_usage_cost();
		StaticVariables.CRU = CostModel.getNorm_comp_resource_usage_cost();
		StaticVariables.B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
		StaticVariables.nru = CostModel.getNetw_resource_usage_cost();
		StaticVariables.cru = CostModel.getComp_resource_usage_cost();

		System.out.print("cru=" + CostModel.getComp_resource_usage_cost() + " ");
		System.out.print("nru=" + CostModel.getNetw_resource_usage_cost() + " ");
		System.out.println("B=" + CostModel.getFog_to_cloud_overall_netw_bwd_usage());
		System.out.print("CRU=" + CostModel.getNorm_comp_resource_usage_cost() + " ");
		System.out.print("NRU=" + CostModel.getNorm_netw_resource_usage_cost() + " ");
		System.out.println("RU=" + CostModel.getResource_usage_cost());

	}

	public void mappingApplicationGRaphToResource(String appId) {

		ModulePlacementRCS modulePlacement = (ModulePlacementRCS) getAppModulePlacementPolicy().get(appId);
		Map<Integer, List<AppModule>> deviceToModuleMap = modulePlacement.getDeviceToModuleMap();

		for (Integer deviceId : deviceToModuleMap.keySet()) {

			if (!modulePlacement.getModulesScheduled().containsKey(deviceId)) {

				modulePlacement.getModulesScheduled().put(deviceId, new ArrayList<String>());
			}
			// System.out.print("\n\nMapping on fog-" + deviceId + ":");
			for (AppModule module : deviceToModuleMap.get(deviceId)) {

				// System.out.print(module.getName() + "\t");
				if (!modulePlacement.getModulesScheduled().get(deviceId).contains(module.getName())) {

					send(deviceId, FogEvents.APP_SUBMIT, modulePlacement.getApplication());
					send(deviceId, FogEvents.LAUNCH_MODULE, module);
					modulePlacement.getModulesScheduled().get(deviceId).add(module.getName());
				}
			}

		}
		

	}

	public void removingApplicationGRaphToResource(String appId) {

		ModulePlacementRCS modulePlacement = (ModulePlacementRCS) getAppModulePlacementPolicy().get(appId);

		for (int deviceId : modulePlacement.getRemovingOperatorsModel().keySet()) {

			for (String moduleName : modulePlacement.getRemovingOperatorsModel().get(deviceId)) {

				send(deviceId, FogEvents.APP_SUBMIT, modulePlacement.getApplication());
				// send(deviceId,
				// FogEvents.OPERATOR_REMOVAL,modulePlacement.getApplication().getModuleByName(moduleName));
			}
		}
	}

	private void printNetworkUsageDetails() {

		if (getAppModulePlacementPolicy().get("IoTDI2021").getUsedAlgorithm().equals(ALGORITHM.RCS)) {
			Application application = getAppModulePlacementPolicy().get("IoTDI2021").getApplication();
			// UtilFunctions.applicationToString(application);
			ApplicationState.backupStaticGraph(application);
			ApplicationState.backupEvolvingGraph();

		}

		if (StaticVariables.jsonObject == null) {

			StaticVariables.jsonObject = new JSONObject();
		}

		JSONArray metrics = new JSONArray();
		JSONObject metric = new JSONObject();
		
		metric.put("RecTime", StaticVariables.reconfigurationTime);
		metrics.add(metric);
		metric = new JSONObject();
		metric.put("B", StaticVariables.B);
		metrics.add(metric);
		metric = new JSONObject();
		metric.put("cru", StaticVariables.cru);
		metrics.add(metric);
		metric = new JSONObject();
		metric.put("nru", StaticVariables.nru);
		metrics.add(metric);
		metric = new JSONObject();
		metric.put("CRU", StaticVariables.CRU);
		metrics.add(metric);
		metric = new JSONObject();
		metric.put("NRU", StaticVariables.NRU);
		metrics.add(metric);
		metric = new JSONObject();
		metric.put("RU", StaticVariables.RU);
		metrics.add(metric);
		StaticVariables.jsonObject.put("metrics", metrics);
		JSONObject cmu = new JSONObject();
		for (int F_j : CostModel.getFog_cpu_memory_usage().keySet()) {

			double cmu_j = CostModel.getFog_cpu_memory_usage().get(F_j);
			cmu.put(CloudSim.getEntityName(F_j), cmu_j);
		}
		StaticVariables.jsonObject.put("cmu", cmu);

		JSONObject json_previous_app_state = JsonToApplication
				.convertJsonFileToObject(StaticVariables.application_topology);
		String file_path = StaticVariables.previous_application_topology + "/application_topology_"
				+ StaticVariables.data_stream_size + ".json";
		JsonToApplication.generateJsonFile(json_previous_app_state, file_path);
		jsonToFile(StaticVariables.jsonObject);
		System.out
				.println("Total network usage = " + NetworkUsageMonitor.getNetworkUsage() / Config.MAX_SIMULATION_TIME);
		System.out.println("Average values B=" + StaticVariables.B + "\tcru=" + StaticVariables.cru + "\tnru="
				+ StaticVariables.nru + "\tCRU=" + StaticVariables.CRU + "\tNRU=" + StaticVariables.NRU + "\tRU="
				+ StaticVariables.RU);
	}

	private FogDevice getCloud() {
		for (FogDevice dev : getFogDevices())
			if (dev.getName().equals("cloud"))
				return dev;
		return null;
	}

	private void printCostDetails() {
		System.out.println("Cost of execution in cloud = " + getCloud().getTotalCost());
	}

	private void printPowerDetails() {

		for (FogDevice fogDevice : getFogDevices()) {
			System.out.println(fogDevice.getName() + " : Energy Consumed = " + fogDevice.getEnergyConsumption());
		}
	}

	private String getStringForLoopId(int loopId) {
		for (String appId : getApplications().keySet()) {
			Application app = getApplications().get(appId);
			for (AppLoop loop : app.getLoops()) {
				if (loop.getLoopId() == loopId)
					return loop.getModules().toString();
			}
		}
		return null;
	}

	private void printTimeDetails() {
		System.out.println("=========================================");
		System.out.println("============== RESULTS ==================");
		System.out.println("=========================================");
		System.out.println("EXECUTION TIME : "
				+ (Calendar.getInstance().getTimeInMillis() - TimeKeeper.getInstance().getSimulationStartTime()));
		System.out.println("=========================================");
		System.out.println("APPLICATION LOOP DELAYS");
		System.out.println("=========================================");
		for (Integer loopId : TimeKeeper.getInstance().getLoopIdToTupleIds().keySet()) {
			/*
			 * double average = 0, count = 0; for(int tupleId :
			 * TimeKeeper.getInstance().getLoopIdToTupleIds().get(loopId)){ Double startTime
			 * = TimeKeeper.getInstance().getEmitTimes().get(tupleId); Double endTime =
			 * TimeKeeper.getInstance().getEndTimes().get(tupleId); if(startTime == null ||
			 * endTime == null) break; average += endTime-startTime; count += 1; }
			 * System.out.println(getStringForLoopId(loopId) + " ---> "+(average/count));
			 */
			System.out.println(getStringForLoopId(loopId) + " ---> "
					+ TimeKeeper.getInstance().getLoopIdToCurrentAverage().get(loopId));
		}
		System.out.println("=========================================");
		System.out.println("TUPLE CPU EXECUTION DELAY");
		System.out.println("=========================================");

		for (String tupleType : TimeKeeper.getInstance().getTupleTypeToAverageCpuTime().keySet()) {
			System.out.println(
					tupleType + " ---> " + TimeKeeper.getInstance().getTupleTypeToAverageCpuTime().get(tupleType));
		}

		System.out.println("=========================================");
	}

	protected void manageResources() {

		send(getId(), Config.RESOURCE_MANAGE_INTERVAL, FogEvents.CONTROLLER_RESOURCE_MANAGE);
	}

	private void processTupleFinished(SimEvent ev) {
	}

	@Override
	public void shutdownEntity() {
	}

	public void submitApplication(Application application, int delay, ModulePlacement modulePlacement) {
		FogUtils.appIdToGeoCoverageMap.put(application.getAppId(), application.getGeoCoverage());
		getApplications().put(application.getAppId(), application);
		getAppLaunchDelays().put(application.getAppId(), delay);
		getAppModulePlacementPolicy().put(application.getAppId(), modulePlacement);

		for (Sensor sensor : sensors) {
			sensor.setApp(getApplications().get(sensor.getAppId()));
		}
		for (Actuator ac : actuators) {
			ac.setApp(getApplications().get(ac.getAppId()));
		}

		for (AppEdge edge : application.getEdges()) {
			if (edge.getEdgeType() == AppEdge.ACTUATOR) {
				String moduleName = edge.getSource();
				for (Actuator actuator : getActuators()) {
					if (actuator.getActuatorType().equalsIgnoreCase(edge.getDestination()))
						application.getModuleByName(moduleName).subscribeActuator(actuator.getId(),
								edge.getTupleType());
				}
			}
		}
	}

	public void submitApplication(Application application, ModulePlacement modulePlacement) {
		submitApplication(application, 0, modulePlacement);
	}

	private void processAppSubmit(SimEvent ev) {
		Application app = (Application) ev.getData();
		processAppSubmit(app);
	}

	private void processAppSubmit(Application application) {
		System.out.println(CloudSim.clock() + " Submitted application " + application.getAppId());
		FogUtils.appIdToGeoCoverageMap.put(application.getAppId(), application.getGeoCoverage());
		getApplications().put(application.getAppId(), application);

		ModulePlacement modulePlacement = getAppModulePlacementPolicy().get(application.getAppId());
		for (FogDevice fogDevice : fogDevices) {

			sendNow(fogDevice.getId(), FogEvents.ACTIVE_APP_UPDATE, application);
		}

		Map<Integer, List<AppModule>> deviceToModuleMap = modulePlacement.getDeviceToModuleMap();
		for (Integer deviceId : deviceToModuleMap.keySet()) {
			for (AppModule module : deviceToModuleMap.get(deviceId)) {
				sendNow(deviceId, FogEvents.APP_SUBMIT, application);
				sendNow(deviceId, FogEvents.LAUNCH_MODULE, module);
			}
		}
		
	}

	public void send(int entityId, int tag, Object data) {

		sendNow(entityId, tag, data);
	}

	private static void jsonToFile(JSONObject json) {

		FileWriter file = null;
		try {

			// Constructs a FileWriter given a file name, using the platform's default
			// charset
			StaticVariables.output_result = StaticVariables.output_result + "_" + StaticVariables.experiment + ".json";
			file = new FileWriter(StaticVariables.output_result, false);
			file.write(json.toString());

		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			try {
				file.flush();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public List<FogDevice> getFogDevices() {
		return fogDevices;
	}

	public void setFogDevices(List<FogDevice> fogDevices) {
		this.fogDevices = fogDevices;
	}

	public Map<String, Integer> getAppLaunchDelays() {
		return appLaunchDelays;
	}

	public void setAppLaunchDelays(Map<String, Integer> appLaunchDelays) {
		this.appLaunchDelays = appLaunchDelays;
	}

	public Map<String, Application> getApplications() {
		return applications;
	}

	public void setApplications(Map<String, Application> applications) {
		this.applications = applications;
	}

	public List<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(List<Sensor> sensors) {
		for (Sensor sensor : sensors)
			sensor.setControllerId(getId());
		this.sensors = sensors;
	}

	public List<Actuator> getActuators() {
		return actuators;
	}

	public void setActuators(List<Actuator> actuators) {
		this.actuators = actuators;
	}

	public Map<String, ModulePlacement> getAppModulePlacementPolicy() {
		return appModulePlacementPolicy;
	}

	public void setAppModulePlacementPolicy(Map<String, ModulePlacement> appModulePlacementPolicy) {
		this.appModulePlacementPolicy = appModulePlacementPolicy;
	}
}