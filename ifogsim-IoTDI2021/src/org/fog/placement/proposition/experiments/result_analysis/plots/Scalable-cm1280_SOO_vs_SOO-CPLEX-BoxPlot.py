import matplotlib.pyplot as plt
import numpy as np

data_point_2000 = [0,0,0.001649380388,0.006868938578,0,0,0,0,0,0,0,0.008505522629,0,0,0.009229525862]
data_point_3000 = [0.002178071121,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_4000 = [0,0.0009540005388,0,0,0,0,0.01904229526,0,0,0,0,0.005537782866,0,0,0]
data_point_5000 = [0,0,0.01196282328,0.01050350216,0,0.01184617457,0.0004369612069,0,0.01055711207,0.002161099138,0.005950969828,0.0093515625,0,0,0.0189606681]
data_point_6000 = [0,0.006397270115,0.003446030891,0.00006914511495,0.00005657327587,0,0.009253322557,0,0.003609240302,0.008377783764,0.005509832974,0,0,0.007240481322,0.0005466505029]
data_point_7000 = [0.004321313116,0.01524822968,0.008082088978,0.0006469365763,0.0009873383621,0,0.003104410406,0.008048606835,0.01190424877,0,0.0003515625,0.0002026246921,0.0002193657636,0,0.008294142549]
data_point_8000 = [0.003111866918,0.01072905442,0.002978515625,0.02393403152,0.0007005994073,0.0001626481681,0.01178071121,0.0001171875,0,0,0.01172514817,0.0002092874461,0.008323679957,0,0.005382711476]
data_point_9000 = [0.004534692289,0.000114194205,0,0.009674928161,0.005901430795,0.00006944444444,0.000782447318,0.008072916667,0.0004877574234,0.009396701389,0.00002035440613,0.001794181034,0.006574173851,0.0005064655172,0.007442229406]
data_point_10000 = [0.001676993534,0.01133203125,0,0.003541487069,0.009578125,0.007106681034,0.001372306034,0.008651131466,0.003483162716,0.009932381466,0.0003933189655,0.0004543372845,0.009965517241,0.01368871228,0]

labels = ['2000', '3000', '4000','5000','6000','7000','8000','9000','10000']
x_pos = np.arange(len(labels))

## combine these different collections into a list    
data_to_plot = [data_point_2000, data_point_3000, data_point_4000, data_point_5000,data_point_6000,data_point_7000,data_point_8000,data_point_9000,data_point_10000]

# Create a figure instance
fig = plt.figure(1, figsize=(9, 6))


# Create an axes instance
ax = fig.add_subplot(111)

ax.set_ylabel('Difference of RU between SOO and SOO-CPLEX')
ax.set_xlabel('Total data stream rate produced at the Edge (KB/sec)')
ax.set_xticks(x_pos)
#ax.set_xticklabels(labels)

colors = ['blue', 'blue', 'blue', 'blue','blue','blue', 'blue','blue','blue','blue']

# Create the boxplot
bp = ax.boxplot(data_to_plot, showfliers=True,patch_artist = True)

for patch, color in zip(bp['boxes'], colors): 
    patch.set_facecolor(color) 
    
for median in bp['medians']: 
    median.set(color ='red', 
               linewidth = 2,
               linestyle ="--") 


# changing color and linewidth of 
# whiskers 
for whisker in bp['whiskers']: 
    whisker.set(color ='black', 
                linewidth = 1.5) 
  
# changing color and linewidth of 
# caps 
for cap in bp['caps']: 
    cap.set(color ='black', 
            linewidth = 2) 
    
# x-axis labels 
ax.set_xticklabels(labels) 

ax.yaxis.grid(True)
    
# Save the figure
fig.savefig('fig1.png', bbox_inches='tight')
plt.show()





