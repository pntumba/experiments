package org.fog.placement.proposition.algorithms;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.entities.FogDevice;
import org.fog.placement.proposition.monitoring.CostModel;
import org.fog.placement.proposition.utils.Graph;
import org.fog.placement.proposition.utils.ModuleType;
import org.fog.placement.proposition.utils.UtilFunctions;

public class RCS{

	private Graph graph;
	private Graph graph_rep;
	private double B;
	private double Buthr;
	private Map<Integer, Double> streamsSet;
	private Map<Integer, Double> cumulatedSelectivitiesForMigratedSreams;
	private Map<Integer, Graph> effectiveMapping = new HashMap<Integer, Graph>();
	private Map<Integer, Integer> sourcesToResourceNodes;
	private Map<Integer, Graph> removing = null;
	private Map<Integer, ModuleType> operatorsType;
	private Map<Integer, Double> operatorsSelectivity;
	private Map<Integer, Boolean> operatorsReplicability;
	private Map<Integer, Double> operatorCosts;
	
	private Map<Integer, Double> contributiontoRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> contributiontoCRU = new ConcurrentHashMap<Integer, Double>();
	private Map<Integer, Double> contributiontoNRU = new ConcurrentHashMap<Integer, Double>();
	private double nd_FC;
	private double nd_EF;
	public RCS(Graph graph, Map<Integer, ModuleType> operatorsType, Map<Integer, Double> operatorsSelectivity,
			Map<Integer, Double> operatorCosts, Map<Integer, Boolean> operatorsReplicability){

		this.graph = graph;
		this.operatorsReplicability = operatorsReplicability;
		this.operatorsSelectivity = operatorsSelectivity;
		this.operatorsType = operatorsType;
		this.cumulatedSelectivitiesForMigratedSreams = new ConcurrentHashMap<Integer, Double>();
		this.operatorCosts = operatorCosts;

	}
	
	public Map<Integer, Graph> replicateAndMigratePartOfApplicationOnFog(double B, double Buthr,
			Map<Integer, Double> streamsSet) {

		System.out.println("operatorsSelectivity="+operatorsSelectivity);
		System.out.println("operatorCosts="+operatorCosts);
		Map<Integer, Graph> mapping = new HashMap<Integer, Graph>();
		this.B = B;
		this.Buthr = Buthr;
		this.streamsSet = streamsSet;
		this.graph_rep = UtilFunctions.replicableGraph(graph, operatorsReplicability);
		this.nd_EF = nd_EF;
		this.nd_FC = nd_FC;

		Queue<Integer> sortedStreams = sortByStreamDecreasing(streamsSet);
		Queue<Integer> selected = new LinkedList<Integer>();
		double cm_F_j = 0;

		selected.add(sortedStreams.remove());
		while (!selected.isEmpty()) {

			int S_j = selected.remove();
			if (!getCumulatedSelectivitiesForMigratedSreams().containsKey(S_j)) {

				int F_j = getSourcesToResourceNodes().get(S_j);
				FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
				System.out.println("Search subgraph to migrate on "+fogDevice.getName());
				cm_F_j = fogDevice.getHost().getRamProvisioner().getRam();
				double cpu_j = fogDevice.getHost().getAvailableMips();
				UtilFunctions.setAvailableCpu(cpu_j);
				UtilFunctions.setAvailableMemory(cm_F_j);
				int sourceVertex = S_j;
				int sinkVertex = graph_rep.getSink();
				Graph graph_candidat = UtilFunctions.candidatSubGraph(graph_rep, sourceVertex, sinkVertex,
						operatorsReplicability);
				System.out.println("\tgraph_candidat " + graph_candidat.getAdjVertices());
				double stream = streamsSet.get(S_j);
				UtilFunctions.identifyGraphSatisfyingFogNodeResource(graph_candidat, operatorsType,
						operatorsSelectivity, operatorCosts, sourceVertex, stream);
				Graph graph_sat_j = UtilFunctions.getGraphSatRequirement();
				System.out.println("\tgraph_sat " + graph_sat_j.getAdjVertices());
				UtilFunctions.findAllEdgeCuts(graph_sat_j, operatorsSelectivity);
				double processed_S_j = UtilFunctions.getMinCutValue();
				Graph graph_mig_j = UtilFunctions.candidatGraphToReplicate(graph_sat_j, sourceVertex,
						UtilFunctions.getMinCut());
				System.out.println("\tgraph_mig " + graph_mig_j.getAdjVertices());
	
				double cmu_F_j = UtilFunctions.getRequiredMemory();
				double nbu_F_jC = UtilFunctions.getMinCutValue();
				if (!graph_mig_j.getAdjVertices().isEmpty()) {

					
					getCumulatedSelectivitiesForMigratedSreams().put(S_j, UtilFunctions.getCumulatedSelectivity());
					mapping.put(F_j, graph_mig_j);
					B = B - stream + processed_S_j;
					System.out.println("\tAccepted subgraph");
					System.out.println("\tRequired cmu " + UtilFunctions.getRequiredMemory() + " on available cm " + cm_F_j);
					System.out.println("\tOutput data stream " + processed_S_j);
					System.out.println("\tCloud bandwidth usage "+B+" < ? "+Buthr+"\n");
					
					double cru_F_j_p = cmu_F_j / cm_F_j;
					double nru_F_j_p = (nbu_F_jC / Buthr) * (nd_FC / nd_EF);
					System.out.print("\tcmu=" + cmu_F_j);
					System.out.println("\tnbu=" + nbu_F_jC);
					double CRU_j = cru_F_j_p / CostModel.getSource_number();
					double NRU_j = nru_F_j_p;
					contributiontoCRU.put(S_j,  CRU_j);
					contributiontoNRU.put(S_j, NRU_j);
					contributiontoRU.put(S_j, CRU_j + NRU_j);
					System.out.print("\tcontribution to CRU =" +contributiontoCRU.get(S_j));
					System.out.print("\tcontribution to NRU =" + contributiontoNRU.get(S_j));
					System.out.println("\tcontribution to RU =" + contributiontoRU.get(S_j));
					
					CostModel.getFog_cpu_memory_usage().put(F_j, cmu_F_j);
					CostModel.getFog_max_cpu_memory().put(F_j, cm_F_j);
					CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
					CostModel.getFog_to_cloud_netw_delay().put(F_j, nd_FC);
					CostModel.getEdge_to_fog_netw_delay().put(F_j, nd_EF);
				}
			}
			if (B > Buthr) {

				if (sortedStreams.isEmpty()){

					System.out.println("\tNo available raw data stream to migrate on the Fog");
					System.out.println("\tCloud bandwidth usage "+B+" < ? "+Buthr);
					UtilFunctions.setWatchDog(true);
					break;
				}
				selected.add(sortedStreams.remove());
			}

		}
		
		for(int S_j : streamsSet.keySet()) {
			
			double nbu_F_jC = streamsSet.get(S_j);
			int F_j = getSourcesToResourceNodes().get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			cm_F_j = fogDevice.getHost().getRamProvisioner().getRam();
			if(mapping.containsKey(F_j)) {
				
				effectiveMapping.put(F_j, mapping.get(F_j));
				
			}else{
				
				if(effectiveMapping.containsKey(F_j)){
					
					continue;
				}
				double CRU_j = 0;
				double NRU_j = 0;
				contributiontoCRU.put(S_j,  CRU_j);
				contributiontoNRU.put(S_j, NRU_j);
				contributiontoRU.put(S_j, CRU_j + NRU_j);
				System.out.print("\tcontribution to CRU =" +contributiontoCRU.get(S_j));
				System.out.print("\tcontribution to NRU =" + contributiontoNRU.get(S_j));
				System.out.println("\tcontribution to RU =" + contributiontoRU.get(S_j));
				
				CostModel.getFog_cpu_memory_usage().put(F_j, 0.0);
				CostModel.getFog_max_cpu_memory().put(F_j, cm_F_j);
				CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
				CostModel.getFog_to_cloud_netw_delay().put(F_j, nd_FC);
				CostModel.getEdge_to_fog_netw_delay().put(F_j, nd_EF);
			}
			
		}
		
		System.out.println("\nMapping to deploy part of application on the Fog");
		B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
		CostModel.printOut();
		for(int F_j : mapping.keySet()) {
			
			CostModel.clear(F_j);
		}
		return mapping;

	}

	public Map<Integer, Graph> migratePartOfApplicationBackToCloud(double B, double Buthr,
			Map<Integer, Double> streamsSet) {
		
		
		removing = new HashMap<Integer, Graph>();
		if(getCumulatedSelectivitiesForMigratedSreams().isEmpty()) {
			
			System.out.println("All Fog nodes are released");
			return removing;
		}
		this.B = B;
		this.Buthr = Buthr;
		this.streamsSet = streamsSet;
		Queue<Integer> sortedStreams = sortByStreamIncreasing(streamsSet);
		Queue<Integer> selected = new LinkedList<Integer>();
		selected.add(sortedStreams.remove());
		double cm_F_j;
		while (!selected.isEmpty()) {
			
			int S_j = selected.remove();
			int F_j = sourcesToResourceNodes.get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			cm_F_j = fogDevice.getHost().getRamProvisioner().getRam();
			System.out.println("Search subgraph to migrate on "+CloudSim.getEntityName(F_j));
			if (getCumulatedSelectivitiesForMigratedSreams().containsKey(S_j)) {
				
				Graph Gmig_j = effectiveMapping.get(F_j);
				
				double processedStream = streamsSet.get(S_j);
				double rawStream = Gmig_j.getRawStream(S_j);
				double deltaStream = rawStream - processedStream;
				System.out.print("\tTry to remove "+effectiveMapping.get(F_j).getAdjVertices());
				System.out.print("\tcurrent nbu="+processedStream);
				System.out.println("\tnew nbu="+rawStream);
				System.out.println("\tdelta nbu="+deltaStream);
				if (B + deltaStream < Buthr){
					
					double nbu_F_jC = rawStream;
					B += deltaStream;
					System.out.println("\tRemoval accepted");
					System.out.println("\tCloud bandwidth usage "+B+" < ? "+Buthr);
					Graph graph_mig_j = effectiveMapping.remove(F_j);
					removing.put(F_j, graph_mig_j);
					getCumulatedSelectivitiesForMigratedSreams().remove(S_j);
					double CRU_j = 0;
					double NRU_j = nbu_F_jC/Buthr;
					contributiontoCRU.put(S_j,  CRU_j);
					contributiontoNRU.put(S_j, NRU_j);
					contributiontoRU.put(S_j, CRU_j + NRU_j);
					System.out.print("\tcontribution to CRU =" +contributiontoCRU.get(S_j));
					System.out.print("\tcontribution to NRU =" + contributiontoNRU.get(S_j));
					System.out.println("\tcontribution to RU =" + contributiontoRU.get(S_j));
					
					CostModel.getFog_cpu_memory_usage().put(F_j, 0.0);
					CostModel.getFog_max_cpu_memory().put(F_j, cm_F_j);
					CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
					CostModel.getFog_to_cloud_netw_delay().put(F_j, nd_FC);
					CostModel.getEdge_to_fog_netw_delay().put(F_j, nd_EF);
					
					if (!sortedStreams.isEmpty()){

						selected.add(sortedStreams.remove());
						
					}else{
						
						System.out.println("\tNo more subgraph to remove from the Fog");
					}
				
				}
			} else {

				if (!sortedStreams.isEmpty()){

					selected.add(sortedStreams.remove());
				}
			}
		}
		System.out.println("\nMapping after migrating part of application back to Cloud");
		B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
		CostModel.printOut();
		return removing;
	}

	public Map<Integer, Graph> adjustEdgeCutToSatfisfyFogNodeResources(Map<Integer, Double> streamsSet_, double Buthr){
		
		removing = new HashMap<Integer, Graph>();
		System.out.println("Search subgraph to adjust the edge-cut");
		for(int S_j: streamsSet_.keySet()) {
			
			int F_j = getSourcesToResourceNodes().get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			Graph graph_mig_j = effectiveMapping.remove(F_j);
			System.out.println("\tAdjust edge-cut on " +fogDevice.getName());
			System.out.println("\tGmig_" +S_j+"= "+graph_mig_j.getAdjVertices());
			removing.put(F_j, graph_mig_j);
			getCumulatedSelectivitiesForMigratedSreams().remove(S_j);
			
			double nbu_F_jC = graph_mig_j.getRawStream(S_j);
			removing.put(F_j, graph_mig_j);
			getCumulatedSelectivitiesForMigratedSreams().remove(S_j);
			double CRU_j = 0;
			double NRU_j = nbu_F_jC/Buthr;
			contributiontoCRU.put(S_j,  CRU_j);
			contributiontoNRU.put(S_j, NRU_j);
			contributiontoRU.put(S_j, CRU_j + NRU_j);
			System.out.print("\tcontribution to CRU =" +contributiontoCRU.get(S_j));
			System.out.print("\tcontribution to NRU =" + contributiontoNRU.get(S_j));
			System.out.println("\tcontribution to RU =" + contributiontoRU.get(S_j)+"\n");
			CostModel.getFog_cpu_memory_usage().put(F_j, 0.0);
			CostModel.getFog_to_cloud_netw_bwd_usage().put(F_j, nbu_F_jC);
			CostModel.getFog_to_cloud_netw_delay().put(F_j, nd_FC);
			CostModel.getEdge_to_fog_netw_delay().put(F_j, nd_EF);
		}
		System.out.println("\nPrepare mapping to adjust Edge cut");
		CostModel.printOut();
		B = CostModel.getFog_to_cloud_overall_netw_bwd_usage();
		if(!removing.isEmpty()) {
			
			UtilFunctions.setWatchDog(false);
		}
		return removing;
	}

	private Queue<Integer> sortByStreamDecreasing(Map<Integer, Double> streamsSet) {

		return UtilFunctions.descendantsortMap(streamsSet);
	}

	private Queue<Integer> sortByStreamIncreasing(Map<Integer, Double> streamsSet) {

		return UtilFunctions.ascendantSortMap(streamsSet);
	}

	private Queue<Integer> sortByFactorIncreasing(Map<Integer, Double> streamsSet) {

		Map<Integer, Double> factors = computeFactors(streamsSet);
		return UtilFunctions.ascendantSortMap(factors);

	}

	private Queue<Integer> sortByFactorDecreasing(Map<Integer, Double> streamsSet) {

		Map<Integer, Double> factors = computeFactors(streamsSet);
		return UtilFunctions.descendantsortMap(factors);
	}

	private Map<Integer, Double> computeFactors(Map<Integer, Double> streamsSet) {

		Map<Integer, Double> factors = new HashMap<Integer, Double>();
		for (int S_j : streamsSet.keySet()) {

			int F_j = getSourcesToResourceNodes().get(S_j);
			FogDevice fogDevice = (FogDevice) CloudSim.getEntity(F_j);
			int cm_j = fogDevice.getHost().getRam();
			double stream = streamsSet.get(S_j);
			factors.put(S_j, (stream / cm_j));

		}
		return factors;
	}
	
	public double getNd_FC() {
		return nd_FC;
	}

	public void setNd_FC(double nd_FC) {
		this.nd_FC = nd_FC;
	}

	public double getNd_EF() {
		return nd_EF;
	}

	public void setNd_EF(double nd_EF) {
		this.nd_EF = nd_EF;
	}
	
	public Map<Integer, Integer> getSourcesToResourceNodes() {
		return sourcesToResourceNodes;
	}

	public void setSourcesToResourceNodes(Map<Integer, Integer> sourcesToResourceNodes) {
		this.sourcesToResourceNodes = sourcesToResourceNodes;
	}

	public Map<Integer, Double> getStreamsSet() {
		return streamsSet;
	}

	public void setStreamsSet(Map<Integer, Double> streamsSet) {
		this.streamsSet = streamsSet;
	}

	public Map<Integer, Graph> getRemoving() {
		return removing;
	}

	public void setRemoving(Map<Integer, Graph> removing) {
		this.removing = removing;
	}

	public Map<Integer, Double> getCumulatedSelectivitiesForMigratedSreams() {
		return cumulatedSelectivitiesForMigratedSreams;
	}

	public void setCumulatedSelectivitiesForMigratedSreams(
			Map<Integer, Double> cumulatedSelectivitiesForMigratedSreams) {
		this.cumulatedSelectivitiesForMigratedSreams = cumulatedSelectivitiesForMigratedSreams;
	}

	public Map<Integer, Graph> getEffectiveMapping() {
		return effectiveMapping;
	}

	public void setEffectiveMapping(Map<Integer, Graph> effectiveMapping) {
		this.effectiveMapping = effectiveMapping;
	}

}
