#!/bin/bash
export MAVEN_OPTS="-Xmx12G"

#rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json


total_data_rates=(965 1221 1500 1792 2000 2256 2400 2656 2800 3023 3256)
#mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS_UR 8 ${total_data_rates[0]} 0 n"
#echo "Looping for RCS[0]: data stream=${total_data_rates[0]}, iteration=1 has finished!"		
#sleep 10	
execution_counter=1
for i in {1..165..1}
do	
			selected_total_data_rates=()
			counter=0
			while [  $counter -lt 10 ]; 
			do
				ctrl_value=0
				while [ $ctrl_value -lt 1 ]; 
				do
					let ctrl_value=ctrl_value+1 
					total_data_rate=${total_data_rates[RANDOM%${#total_data_rates[@]}]}
				 
					for selected in "${selected_total_data_rates[@]}" 
					do
						if [[ $selected -eq $total_data_rate ]]
						then
							ctrl_value=0
						fi
					done
				done 
				selected_total_data_rates=(${selected_total_data_rates[@]} $total_data_rate)
				let counter=counter+1 
				echo "Looping for RCS[$execution_counter]: data stream=$total_data_rate, iteration=$i has finished!"
				let execution_counter=execution_counter+1 
				sleep 2
			done
done

