import matplotlib.pyplot as plt
import numpy as np

operators = ['Source S_j','O_1','O_2','O_3','O_4','O_5','O_6','O_7', 'O_8']
CONTRIBUTION_CRU = [0,0.605859375,1.134609375,1.535578125,1.794665625,2.016740625,2.135180625,2.185517625,2.217496425]
CONTRIBUTION_NRU = [1.555862069,1.244689655,0.8712827586,0.5227696552,0.4182157241,0.2091078621,0.08364314483,0.0501858869,0.03513012083]
CONTRIBUTION_RU = [1.555862069,1.85054903,2.005892134,2.05834778,2.212881349,2.225848487,2.21882377,2.235703512,2.252626546]

fig, ax = plt.subplots()
ax.plot(operators, CONTRIBUTION_CRU, "-*b", label="CRU_j", markersize=8)
ax.plot(operators, CONTRIBUTION_NRU, "-+r", label="NRU_j",markersize=8)
ax.plot(operators, CONTRIBUTION_RU, "-y", label="RU_j",markersize=8)
ax.legend()
plt.ylabel('Resource usage costs')
plt.xlabel('Gim_j includes operators up to this one')
plt.show()


