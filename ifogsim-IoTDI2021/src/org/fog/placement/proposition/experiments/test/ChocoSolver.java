package org.fog.placement.proposition.experiments.test;
/* --------------------------------------------------------------------------
 * File: FixCost1.java
 * Version 12.10.0  
 * --------------------------------------------------------------------------
 * Licensed Materials - Property of IBM
 * 5725-A06 5725-A29 5724-Y48 5724-Y49 5724-Y54 5724-Y55 5655-Y21
 * Copyright IBM Corporation 2001, 2019. All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 * --------------------------------------------------------------------------
 * 
 * Problem Description
 * -------------------
 * 
 * A company must produce a product on a set of machines.
 * Each machine has limited capacity.
 * Producing a product on a machine has both a fixed cost
 * and a cost per unit of production.
 * 
 * Minimize the sum of fixed and variable costs so that the
 * company exactly meets demand.
 */


import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;

import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloNumVarType;
import ilog.cplex.IloCplex;

public class ChocoSolver {

	
	static double Bmax = 1450.0;
	static double w_c = 1.0;
	static double w_n = 1.0;
	static double[] source_data_stream_rate = {901.0, 289.0, 496.0, 570.0};
	static double[] max_fog_comp_resources = { 1024.0, 1024.0, 1024.0, 1024.0 };
	static double[] csel_edge_cut = {1.0, 0.8372024240134905, 0.4017040542371155, 0.4128752793675769, 0.33166569728337786, 0.3133470785980327, 0.0977588891068062};
	static double[] cost = { 1.0, 1.0596891582237213, 1.0199796797730463, 1.0808677322445686, 1.0559230256306749, 1.0716408691591865, 1.0013314187308762 };
	
		
	public static void main(String[] args) {
		
		
		//solve(source_data_stream_rate.length, csel_edge_cut.length);
		
		// 1. Create a Model
		Model model =new Model("my first problem");
		
		IntVar x = model.intVar("X", 0, 5);
		
		IntVar y = model.intVar("Y",new int[]{2, 3, 8});
		
		model.arithm(x, "+", y, "<", 5).post();
		model.times(x,y,4).post();
		
		model.getSolver().solve();
		System.out.println(x);
		System.out.println(y);
		
		
	}
	
	public static void solve(int N, int K) {
		
		double[][] fog_cpumemory_usage = new double[N][K];
		double[][] fog_to_cloud_bwd_usage = new double[N][K];
		
		try{

			IloCplex cplex = new IloCplex();
			for (int j = 0; j < N; j++) {

				double cmu = 0.0;
				
				for (int k = 0; k < K; k++){
					
					
					if (k == 0) {

						fog_cpumemory_usage[j][k] = 1;
						fog_to_cloud_bwd_usage[j][k] = source_data_stream_rate[j] * csel_edge_cut[k];
						System.out.print("\n|S"+(j+1)+"|="+source_data_stream_rate[j]+"\t"+fog_cpumemory_usage[j][k]+"\t");
						
					} else{
						
						cmu = cmu + fog_to_cloud_bwd_usage[j][k-1] * cost[k];
						fog_cpumemory_usage[j][k] = cmu;
						fog_to_cloud_bwd_usage[j][k] = source_data_stream_rate[j] * csel_edge_cut[k];
						System.out.print(fog_cpumemory_usage[j][k]+"\t");
					}
					
				}
			}
			
			IloNumVar[][] X = new IloNumVar[N][];
			for (int j = 0; j < N; j++) {
				X[j] = cplex.numVarArray(K, 0, 1,IloNumVarType.Int);
				
				// Constraint for the decision variable
				cplex.addEq(cplex.sum(X[j]), 1);
				//cplex.addGe(1, cplex.sum(X[j]));
				
			}
			
			
            IloLinearNumExpr [] expr_cmu = new IloLinearNumExpr[N];
            IloLinearNumExpr [] expr_nbu = new IloLinearNumExpr[N];
			for(int j=0; j<N; j++) {
				
				expr_cmu[j]= cplex.linearNumExpr();
				expr_nbu[j]= cplex.linearNumExpr();
				for(int k=0; k<K; k++) {
					
					//nru = cplex.sum(nru, cplex.prod(fog_to_cloud_bwd_usage[j][k]/Bmax, X[j][k]));
					//cru = cplex.sum(cru, cplex.prod(fog_cpumemory_usage[j][k]/max_fog_comp_resources[j], X[j][k]));	
					expr_cmu[j].addTerm(fog_cpumemory_usage[j][k],  X[j][k]);
					expr_nbu[j].addTerm(fog_to_cloud_bwd_usage[j][k],  X[j][k]);
				}
				
			}
			
			
			//Overall computational resource usage
			IloNumExpr cru = cplex.numExpr();
			
			//Overall network resource usage
			IloNumExpr nru = cplex.numExpr();
			
			for(int j=0; j<N; j++) {
				
				cru = cplex.sum(cru, cplex.prod(1/(max_fog_comp_resources[j] * N), expr_cmu[j]));
				nru = cplex.sum(nru, cplex.prod(1/Bmax, expr_nbu[j]));
				
				//Constraint for the computational resource usage
				System.out.println("\nexpr_cmu["+j+"]="+expr_cmu[j]);
				cplex.addLe(expr_cmu[j], max_fog_comp_resources[j]);	
			}
			//Constraint for the network resource usage
			cplex.addLe(cplex.sum(expr_nbu), Bmax);
			
			//Normalization of cru
			IloNumExpr CRU = cplex.numExpr();
			CRU = cplex.prod(1, cru);
		
			//Normalization of nru
			IloNumExpr NRU = cplex.numExpr();
			NRU = cplex.prod(1, nru);
			
			 // Objective: minimize w_c * CRU + w_n * NRU
	         cplex.addMinimize(cplex.sum(cplex.prod(w_n, NRU), cplex.prod(w_c, CRU)));
	       
	        	
	        
	         cplex.setParam(IloCplex.Param.MIP.Strategy.Search, IloCplex.MIPSearch.Traditional);
	        
	        if ( cplex.solve() ) {
	            System.out.println("Solution status: " + cplex.getStatus());
	            
	            System.out.println("NRU="+cplex.getValue(NRU)+"\tnru="+cplex.getValue(cplex.sum(expr_nbu)));
	            System.out.print("CRU="+cplex.getValue(CRU));
	            for(int j=0; j<N; j++) {
	            	
	            	 System.out.print("\tcmu_F"+(j+1)+"="+cplex.getValue(expr_cmu[j]));
	            }
	            System.out.println("\nRU " + cplex.getObjValue());
	            for(int j=0; j<N; j++) {
	            	System.out.print("   S_" + (j+1) + ": ");
	            	for(int k=0; k<K; k++) {
	            		
	                       System.out.print("edge-cut("+k+1+")" + cplex.getValue(X[j][k]) + "\t");
	                   
	            	}
	            	 System.out.println();
	            }
	           
	       }else {
	    	   
	    	   
	    	   System.out.println("Solution status = " + cplex.getStatus());
	    	 
	       }
	        if(cplex.isMIP()) {
	        	
	        	
	        	System.out.println("isMIP");
	        }
	        if(cplex.isPrimalFeasible()) {
	        	
	        	System.out.println("isPrimalFeasible");
	        }
	        if(cplex.isDualFeasible()) {
	        	
	        	System.out.println("isDualFeasible");
	        }
	        cplex.exportModel("file_name.lp");
	        

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			//System.err.println("Concert exception caught: " + );
		}

	}
}

/*
 * Solution Obj 1788 E5 is used for 22
 */
