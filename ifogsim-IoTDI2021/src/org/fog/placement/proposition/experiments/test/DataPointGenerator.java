package org.fog.placement.proposition.experiments.test;

import java.util.ArrayList;

import org.fog.application.Application;
import org.fog.utils.JsonToApplication;
import org.json.simple.JSONObject;

public class DataPointGenerator {

	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		double ratio = 1.5;
		ArrayList<Double> totalDataStreamRates = new ArrayList<Double>();
		totalDataStreamRates.add(512.0 * ratio);
		totalDataStreamRates.add(700.0 * ratio);
	    totalDataStreamRates.add(965.0 * ratio);
	   
		totalDataStreamRates.add(1221.0 * ratio);
		
		totalDataStreamRates.add(1500.0 * ratio);
		totalDataStreamRates.add(1792.0 * ratio);
		
		
		totalDataStreamRates.add(2000.0 * ratio);
		totalDataStreamRates.add(2100.0 * ratio);
		
		totalDataStreamRates.add(2256.0 * ratio);
		
		
		for(double total_data_rate : totalDataStreamRates) {
			
			System.out.println("add(\""+(int)total_data_rate+"\");");
			
		}
		
	}
	
}
