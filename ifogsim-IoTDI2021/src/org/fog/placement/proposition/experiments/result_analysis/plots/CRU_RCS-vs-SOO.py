import matplotlib.pyplot as plt
import numpy as np

operators = [256,512,700,965, 1221, 1500, 1792, 2000, 2256, 2400]
CRU_RCS = [0.0004340277778,0.0004784169823,0.001310961174,0.02049038563,0.07775739162,0.2378960503,0.3655751854,0.3571358112,0.5459748856,0.4958810106]
CRU_SOO = [0.133186849,0.2510025817,0.3628580729,0.4673397091,0.5407552083,0.6057128906,0.6967285156,0.7061686198,0.7612792969,0.7719563802]

fig, ax = plt.subplots()
ax.plot(operators, CRU_RCS, "-*b", label="RCS", markersize=8)
ax.plot(operators, CRU_SOO, "-+r", label="SOO",markersize=8)
ax.legend()
plt.ylabel('Normalized computational resource usage cost (CRU)')
plt.xlabel('Sum-rates of data streams produced at the Edge (KB/sec)')
plt.show()


