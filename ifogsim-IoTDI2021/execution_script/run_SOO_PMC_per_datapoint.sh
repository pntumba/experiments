#!/bin/bash

rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json

total_data_rates=(256 512 700 965 1221 1500 1792 2000 2256 2400)
#mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS_UR 8 ${total_data_rates[0]} 0 n"
#echo "Looping for RCS[0]: data stream=${total_data_rates[0]}, iteration=1 has finished!"		
#sleep 10	
k=0
execution_counter=1
total_data_rate=2400
for i in {1..15..1}
do	
			
				mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_SOO_MinCompNetCut" -Dexec.args="SOO_MinCompNetCut_NoMonotonic 10 $total_data_rate $execution_counter n"
				echo "Looping for SOO_MinCompNetCut_NoMonotonic[$execution_counter]: data stream=$total_data_rate, iteration=$i has finished!"
				let execution_counter=execution_counter+1 
				sleep 10
done
