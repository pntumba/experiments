import matplotlib.pyplot as plt
import numpy as np

data_point_965 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_1221 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_1500 = [0.001062095905,0,0,0,0,0,0,0,0,0,0,0,0,0.009444369612,0]
data_point_1792 = [0.01922615841,0,0,0,0,0,0,0,0.01204606681,0.008342537716,0,0,0,0.005530037716,0]
data_point_2000 = [0,0,0.001649380388,0.006868938578,0,0,0,0,0,0,0,0.008505522629,0,0,0.009229525862]
data_point_2100 = [0.008001751078,0.009275323276,0,0,0.005813577586,0,0,0,0.007306707974,0,0,0,0,0,0]
data_point_2256 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


labels = ['965', '1221', '1500','1792','2000','2100','2256']
x_pos = np.arange(len(labels))

## combine these different collections into a list    
data_to_plot = [data_point_965, data_point_1221, data_point_1500, data_point_1792,data_point_2000,data_point_2100,data_point_2256]

# Create a figure instance
fig = plt.figure(1, figsize=(9, 6))


# Create an axes instance
ax = fig.add_subplot(111)

ax.set_ylabel('Difference of RU between SOO and SOO-CPLEX')
ax.set_xlabel('Data stream rate produced at the Edge (KB/sec)')
ax.set_xticks(x_pos)
#ax.set_xticklabels(labels)

colors = ['blue', 'blue', 'blue', 'blue','blue','blue', 'blue']

# Create the boxplot
bp = ax.boxplot(data_to_plot, showfliers=False,patch_artist = True)

for patch, color in zip(bp['boxes'], colors): 
    patch.set_facecolor(color) 
    
for median in bp['medians']: 
    median.set(color ='red', 
               linewidth = 2,
               linestyle ="--") 


# changing color and linewidth of 
# whiskers 
for whisker in bp['whiskers']: 
    whisker.set(color ='black', 
                linewidth = 1.5) 
  
# changing color and linewidth of 
# caps 
for cap in bp['caps']: 
    cap.set(color ='black', 
            linewidth = 2) 
    
# x-axis labels 
ax.set_xticklabels(labels) 
ax.yaxis.grid(True)
    
# Save the figure
fig.savefig('fig1.png', bbox_inches='tight')
plt.show()





