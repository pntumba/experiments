import matplotlib.pyplot as plt
import numpy as np

data_point_965 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_1221 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_1500 = [0.001062095905,0,0,0,0,0,0,0,0,0,0,0,0,0.009444369612,0]
data_point_1792 = [0.01922615841,0,0,0,0,0,0,0,0.01204606681,0.008342537716,0,0,0,0.005530037716,0]
data_point_2000 = [0,0,0.001649380388,0.006868938578,0,0,0,0,0,0,0,0.008505522629,0,0,0.009229525862]
data_point_2100 = [0.008001751078,0.009275323276,0,0,0.005813577586,0,0,0,0.007306707974,0,0,0,0,0,0]
data_point_2256 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


# Calculate the average
data_point_965_mean = np.mean(data_point_965)
data_point_1221_mean = np.mean(data_point_1221)
data_point_1500_mean = np.mean(data_point_1500)
data_point_1792_mean = np.mean(data_point_1792)
data_point_2000_mean = np.mean(data_point_2000)
data_point_2100_mean = np.mean(data_point_2100)
data_point_2256_mean = np.mean(data_point_2256)



print '\n'


# Calculate the standard deviation
data_point_965_std = np.std(data_point_965)
data_point_1221_std = np.std(data_point_1221)
data_point_1500_std = np.std(data_point_1500)
data_point_1792_std = np.std(data_point_1792)
data_point_2000_std = np.std(data_point_2000)
data_point_2100_std = np.std(data_point_2100)
data_point_2256_std = np.std(data_point_2256)




# Define labels, positions, bar heights and error bar heights
labels = ['965', '1221', '1500','1792','2000','2100','2256']
CTEs = [data_point_965_mean, data_point_1221_mean, data_point_1500_mean,data_point_1792_mean, data_point_2000_mean, data_point_2100_mean,data_point_2256_mean]
error = [data_point_965_std, data_point_1221_std, data_point_1500_std,data_point_1792_std, data_point_2000_std, data_point_2100_std,data_point_2256_std]
x_pos = np.arange(len(CTEs))

means_tostring = np.array(map(str, CTEs))

width = 0.35

plt.figure()
plt.title('Average Age')
plt.bar(x_pos, CTEs, width, align='center', yerr=error, ecolor='k', capsize=5)
plt.ylabel('Difference of RU between SOO and SOO-CPLEX')
plt.xticks(x_pos,labels)

def autolabel(bars,means_tostring):
    for ii,bar in enumerate(bars):
        height = bars[ii]
        plt.text(x_pos[ii], height-5, '%s'% (means_tostring[ii]), ha='center', va='bottom')
autolabel(CTEs,means_tostring) 
plt.show()


#print CTEs
#print error

# Build the plot
#fig, ax = plt.subplots()
#ax.bar(x_pos, CTEs,
 #      yerr=error,
 #      align='center',
 #      alpha=0.5,
 #      ecolor='red',
 #      capsize=10)

#ax.set_ylabel('Difference of RU between SOO and SOO-CPLEX')
#ax.set_xlabel('Data stream rate produced at the Edge (KB/sec)')
#ax.set_xticks(x_pos)
#ax.set_xticklabels(labels)
#ax.yaxis.grid(True)


# Save the figure and show
#plt.tight_layout()
#plt.savefig('bar_plot_with_error_bars.png')
#plt.show()





