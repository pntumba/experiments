import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [2000,3000,4000,5000,6000,7000,8000,9000,10000]
ResTime_SOO = [97.33333333,94.73333333,122.2,114.6666667,118.0666667,126.8,123.6,118.2,135.6]
RU_SOOCPLEX_OG_0_0 = [140.2666667,148.2666667,168.7333333,184.0666667,213.3333333,223.6666667,263.6666667,255.2666667,279.6]
RU_SOOCPLEX_OG_0_5=[128,126.4,137.4666667,166.1333333,170,146.4,146.4,166.5333333,156.7333333]
fig, ax = plt.subplots()
ax.plot(data_stream_rates, ResTime_SOO, "--sr", label="SOO",markersize=8, markevery=1)
ax.plot(data_stream_rates, RU_SOOCPLEX_OG_0_0, "--^b", label="SOO-CPLEX with OG=0.0", markersize=8, markevery=1)
ax.plot(data_stream_rates, RU_SOOCPLEX_OG_0_5, "--ok", label="SOO-CPLEX with OG=0.5", markersize=8, markevery=1)
ax.legend()
ax.set_ylabel('Resolution time (ms)')
ax.set_xlabel('Total data stream rate produced at the Edge (KB/sec)')
data_streams = [4,6,8,10,12,14,16,18,20]
x_pos = np.arange(len(data_streams))
plt.show()


