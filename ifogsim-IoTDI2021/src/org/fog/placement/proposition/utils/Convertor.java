package org.fog.placement.proposition.utils;

import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.application.AppEdge;
import org.fog.application.AppModule;
import org.fog.application.Application;
import org.fog.entities.Actuator;
import org.fog.entities.Sensor;
import org.fog.placement.proposition.configuration.Configuration;

public class Convertor {
private Configuration reconfigure;

	
public 	Convertor(Configuration reconfigure) {
	
	this.reconfigure = reconfigure;
	
}
public void applicationToGraph(){
		
		// applicationToString();
		Graph graph = new Graph();
		int vertex = 0;
				
		
				for (AppEdge edge : reconfigure.getApplication().getEdges()) {

					/**
					 * Add operator selectivity to vertices of type source
					 */
					if (!reconfigure.getApplicationToGraph().containsKey(edge.getSource())) {

						reconfigure.getApplicationToGraph().put(edge.getSource(), vertex);
						reconfigure.getGraphToApplication().put(vertex, edge.getSource());
						graph.addVertex(vertex);
						reconfigure.addOperatorSelectivity(reconfigure.getApplication().getModuleByName(edge.getSource()), vertex);
						vertex++;
					}

					/**
					 * Add operator selectivity to vertices of type operator and sink
					 */
					if (!reconfigure.getApplicationToGraph().containsKey(edge.getDestination())){

						reconfigure.getApplicationToGraph().put(edge.getDestination(), vertex);
						reconfigure.getGraphToApplication().put(vertex, edge.getDestination());
						graph.addVertex(vertex);
						reconfigure.addOperatorSelectivity(reconfigure.getApplication().getModuleByName(edge.getDestination()), vertex);
						vertex++;
					}
					
					// System.err.println("edge.getSource() "+edge.getSource());
					int vertex1 = reconfigure.getApplicationToGraph().get(edge.getSource());
					int vertex2 = reconfigure.getApplicationToGraph().get(edge.getDestination());
					//System.err.println("addEdge "+vertex1+" to "+vertex2);

										
					// System.err.println("addEdge "+edge.getSource()+"("+vertex1+") to
					// "+edge.getDestination()+"("+vertex2+")");
					graph.addEdge(vertex1, vertex2);

					/**
					 * Add the link between the source and the computing resource
					 */
					for (Sensor sensor : reconfigure.getSensors()) {

						if (sensor.getName().equals(edge.getSource())) {
							
							int sourceVertex = reconfigure.getApplicationToGraph().get(sensor.getName());
							double datastream = reconfigure.getApplication().getEdgeMap().get(sensor.getName()).getTupleNwLength();
							reconfigure.getInitialDataStreams().put(sourceVertex, datastream);
							if (!reconfigure.getSourcesToResourceNodes().containsKey(sourceVertex)) {

								reconfigure.getSourcesToResourceNodes().put(sourceVertex, sensor.getGatewayDeviceId());
								reconfigure.getResourceNodeToSource().put(sensor.getGatewayDeviceId(),sourceVertex);
								
							}
						}
					}

					/**
					 * Add the link between the sink at the edge with the computing resource
					 */
					for (Actuator actuator : reconfigure.getActuators()) {

						if (actuator.getName().equals(edge.getDestination())) {

							int sinkVertex = reconfigure.getApplicationToGraph().get(actuator.getName());
							if (!reconfigure.getResourcesToSinkNodes().containsKey(sinkVertex)) {
								
								
								reconfigure.getResourcesToSinkNodes().put(actuator.getGatewayDeviceId(), sinkVertex);
							}
						}
					}

					/**
					 * Add Type of operator
					 * 
					 */
					
					AppModule module = reconfigure.getApplication().getModuleByName(edge.getSource());
					if (module != null) {

						if (reconfigure.getApplicationToGraph().containsKey(module.getName())) {

							int local_vertex = reconfigure.getApplicationToGraph().get(module.getName());
							if (!reconfigure.getOperatorsType().containsKey(local_vertex)) {

								reconfigure.getOperatorsType().put(local_vertex, module.getType());
							}
						}
					}

					module = reconfigure.getApplication().getModuleByName(edge.getDestination());
					if (module != null) {
						
						
						//reconfigure.addOperatorToMappingModel(CloudSim.getEntityId("cloud"), module.getName());
						if (!reconfigure.getApplicationToGraph().containsKey(module.getName())) {

							int local_vertex = reconfigure.getApplicationToGraph().get(module.getName());
							if (!reconfigure.getOperatorsType().containsKey(local_vertex)) {

								reconfigure.getOperatorsType().put(local_vertex, module.getType());
							}
							
						}
					}
				}

				for (int local_vertex : graph.getAdjVertices().keySet()) {

					if (!reconfigure.getOperatorsReplicability().containsKey(local_vertex)) {

						if (graph.isSink(local_vertex)) {

							reconfigure.getOperatorsReplicability().put(local_vertex, false);

						} else if (graph.isSource(local_vertex)) {

							reconfigure.getOperatorsReplicability().put(local_vertex, true);

						} else {
							
							
							String operatorName = reconfigure.getGraphToApplication().get(local_vertex);
							AppModule module = reconfigure.getApplication().getModuleByName(operatorName);
							reconfigure.getOperatorsReplicability().put(local_vertex, module.isLocallyReplicable());
						}
					}

				}
				
		reconfigure.setInitialGraph(graph);
		Graph staticGraph = new Graph();
		for(int vertex1: graph.getAdjVertices().keySet()) {
			
			staticGraph.addVertex(vertex1);
			for(int vertex2: graph.getAdjVertices(vertex1).keySet()){
				
				double weight = graph.getEdgeWeight(vertex1, vertex2);
				staticGraph.addEdge(vertex1, vertex2, weight);
			}
		}
		reconfigure.setStaticGraph(staticGraph);
		
	}
}
