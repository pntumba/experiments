package org.fog.placement.proposition.experiments.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.fog.placement.proposition.utils.StaticVariables;
import org.fog.utils.JsonToApplication;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class TestJson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		//datapointValue();
		
		
		
	}
	
	
	public static void datapointValue() {
		

		String datapointvalue = "4512";
		String datapointsFile = StaticVariables.input_directory + "datapoints_large_scale.json";
		JSONObject datapointsObject = JsonToApplication.convertJsonFileToObject(datapointsFile);
		JSONArray experiments = (JSONArray) datapointsObject.get(datapointvalue);
		for (int i = 0; i < experiments.size(); i++) {

			JSONObject experiment = (JSONObject) experiments.get(i);
			JSONArray datarates = (JSONArray) experiment.get("experiment_" + (i + 1));
			List<Double> distribution = new ArrayList<Double>();
			for (int j = 0; j < datarates.size(); j++) {
				
				double data_rate = new BigDecimal((double) datarates.get(j)).doubleValue();
				System.out.print(data_rate);
				if(j < datarates.size() - 1) {
					
					System.out.print("\t");
				}
			}
			System.out.println();
		}
		
	}
	public static void print() {
		
		String jsonLogFile = "src/org/fog/placement/proposition/experiments/Results/RCS_2/executionLogFile.json";
		JSONObject jsonLogObject = JsonToApplication.convertJsonFileToObject(jsonLogFile);
		JSONArray executions = (JSONArray) jsonLogObject.get("executions");
		StringBuilder sequence = new StringBuilder();
		StringBuilder count = new StringBuilder();
		StringBuilder Bmin = new StringBuilder();
		StringBuilder Bmax = new StringBuilder();
		sequence.append("[");
		count.append("[");
		Bmin.append("[");
		Bmax.append("[");
		for(int i=1; i<executions.size(); i++) {
			
			JSONObject execution = (JSONObject) executions.get(i);
			double data_rate = new BigDecimal( (double)(execution.get("Total_data_rates"))).doubleValue();
			//System.out.println(data_rate);
			count.append((i)+",");
			Bmin.append(800.0+",");
			Bmax.append(1450.0+",");
			sequence.append(data_rate+",");
		}
		sequence.append("]");
		count.append("]");
		Bmin.append("]");
		Bmax.append("]");
		System.out.println(sequence.toString());
		System.out.println(count.toString());
		System.out.println(Bmin.toString());
		System.out.println(Bmax.toString());
	}

}
