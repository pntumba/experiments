package org.fog.placement.proposition.experiments.result_analysis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class ResultTimeAnalysis{

	private final static String path = "/home/pntumba/inria_code/repositories/Experiments/IoTDI2021/ifogsim-IoTDI2021/src/org/fog/placement/proposition/experiments/";

	
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
	   String directory ="output/";
	  //String directory ="Results/RCS_2/"; 
	  
	  int data_stream_rate = 2000; 
	  String  pattern = "Experiment_RCS_10_"+data_stream_rate+".0";
      pattern  = "Experiment_SOO_10_"+data_stream_rate+".0";
   //  pattern = "Experiment_SOO_CPLEX_10_"+data_stream_rate+".0";
    
       String file_pattern = directory+pattern;
		readJsonFile(file_pattern);
	}

	public static void readJsonFile(String pattern) throws IOException {
		
		for (int i = 1; i <= 15; i++){

			JSONObject metric_object = new JSONObject();
			ArrayList<String> header = new ArrayList<String>();
			int countFiles = 0;
			String jsonFile = path+pattern + "_" + i + ".json";

			try {

				File isFile = new File(jsonFile);
				if (!isFile.exists()) {
					System.out.println("Can not find "+pattern + "_" + i + ".json");
					continue;
				}
				JSONObject object = (JSONObject) JSONValue.parse(new FileReader(jsonFile));
				JSONArray metrics = (JSONArray) object.get("metrics");
				for (int index = 0; index < metrics.size(); index++) {

					JSONObject obj = (JSONObject) metrics.get(index);
					metric_object.putAll(obj);
				}
				long RecTime = new BigDecimal((long) (metric_object.get("RecTime"))).longValue();
				
				System.out.println(RecTime );
				//System.out.println("B="+_B + "\t" +"RU="+_RU + "\t" +"CRU="+_CRU + "\t" +"NRU="+_NRU);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
