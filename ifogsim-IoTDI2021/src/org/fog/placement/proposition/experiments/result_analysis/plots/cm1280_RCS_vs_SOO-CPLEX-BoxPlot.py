import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


data_point_965 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_1221 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
data_point_1500 = [0.001062095905,0,0,0,0,0,0,0,0,0,0,0,0,0.009444369612,0]
data_point_1792 = [0.01922615841,0,0,0,0,0,0,0,0.01204606681,0.008342537716,0,0,0,0.005530037716,0]
data_point_2000 = [0,0,0.001649380388,0.006868938578,0,0,0,0,0,0,0,0.008505522629,0,0,0.009229525862]
data_point_2100 = [0.008001751078,0.009275323276,0,0,0.005813577586,0,0,0,0.007306707974,0,0,0,0,0,0]
data_point_2256 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


labels = ['965', '1221', '1500','1792','2000','2100','2256']



fruits=['Apple', 'Papaya', 'Banana', 'Mango','Litchi','Avocado']
prices=[3, 1, 2, 4,4,5]

df = pd.DataFrame({
                    '965':np.sort(data_point_965),
                   '1221':np.sort(data_point_1221),
                   '1500':np.sort(data_point_1500),
                   '1792':np.sort(data_point_1792),
                   '2000':np.sort(data_point_2000),
                   '2100':np.sort(data_point_2100),
                   '2256':np.sort(data_point_2256)
                   
                   }
                  )
print(df)

boxplot = df.boxplot(column=['965','1221', '1500','1792','2000','2100','2256'], showfliers=False)
boxplot.plot()

plt.show()
