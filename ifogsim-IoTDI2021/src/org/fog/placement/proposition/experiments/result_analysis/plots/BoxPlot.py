import matplotlib.pyplot as plt
import numpy as np

data_point_965 = [0,0,0,0,0,0,0,0,0,0]
data_point_1221 = [0,0,0,0,0,0,0,0,0,0]
data_point_1500 = [0,0.02823005722,0,0.05903976545,0.04513127301,0,0,0,0,0,0,0,0.01977444833,0,0]
data_point_1792 = [0.04018458475,0.04785878469,0.04709847192,0,0.04028258064,0,0,0,0,0,0,0,0.04710262284,0,0.05490585441]
data_point_2000 = [0.008361737133,0,0.001922990427,0,0,0.04610744225,0.02322691102,0,0.01801038927,0,0.00544738639,0,0,0,0]
data_point_2256 = [0,0,0,0,0.01588670036,0,0.0341216838,0,0.04123730029,0,0,0.04453757511,0.03404845913,0,0.04409568307]
data_point_2400 = [0,0.03396136171,0,0,0.02031157454,0.05000838416,0,0,0.00544738639,0,0,0,0.02807742836,0,0]
data_point_2656 = [0,0,0.04206503398,0,0,0.0387413365,0,0,0.04453757511,0,0,0,0,0,0]
data_point_2800 = [0,0,0,0,0.03661764759,0,0,0,0,0.05675172576,0.01860725832,0,0,0.02148906376,0]

data = list([data_point_965, data_point_1221, data_point_1500, data_point_1792,data_point_2000,data_point_2256,data_point_2400,data_point_2656,data_point_2800])

fig, ax = plt.subplots()

# build a box plot
ax.boxplot(data, meanline=True,showmeans=True, showfliers=False)

# title and axis labels
ax.set_title('Box plot')
ax.set_xlabel('Total data stream rates (KB/sec)')
ax.set_ylabel('Difference of resource usage costs between SOO and SOO CPLEX')
xticklabels=['965', '1221', '1500','1792','2000','2256','2400','2656','2800']
ax.set_xticklabels(xticklabels)

# add horizontal grid lines
ax.yaxis.grid(True)

# show the plot
plt.show()




