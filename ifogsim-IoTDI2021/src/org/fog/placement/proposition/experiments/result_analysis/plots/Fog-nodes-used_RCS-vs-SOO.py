
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from json.decoder import _decode_uXXXX
from matplotlib.rcsetup import validate_fontsizelist


labels = ['256','512','700','965', '1221', '1500', '1792', '2000', '2256', '2400']
RCS = [0,0,0,0.02,0.1,0.35,0.4,0.45,0.68,0.7]
SOO = [1.0,1.0,1.0, 1.0,1.0,1.0,1.0,1.0,1.0,1.0]

x = np.arange(len(labels))  # the label locations
width = 0.35  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, RCS, width, label='RCS')
rects2 = ax.bar(x + width/2, SOO, width, label='SOO')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('Average number of fog nodes used (Normalized)')
ax.set_xlabel('Sum-rates of data streams produced at the Edge (KB/sec)')
# ax.set_title('Scores by group and gender')
ax.set_xticks(x)
ax.set_xticklabels(x)
ax.set_xticklabels(labels)
ax.legend()

def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


autolabel(rects1)
autolabel(rects2)

fig.tight_layout()

plt.show()