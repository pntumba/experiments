#!/bin/bash
export MAVEN_OPTS="-Xmx12G"

rm src/org/fog/placement/proposition/experiments/output/executionLogFile.json

#total_data_rates=(256 512 700 965 1221 1500 1792 2000 2256 2400)
total_data_rates=(512 700 965 1221 1500 1792 2000 2100 2256)
mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS_UR 10 ${total_data_rates[0]} 0 s"
echo "Looping for RCS[0]: data stream=${total_data_rates[0]}, iteration=1 has finished!"		
#sleep 10	
execution_counter=1
for i in {1..15..1}
do	
			selected_total_data_rates=()
			counter=0
			while [  $counter -lt 9 ]; 
			do
				ctrl_value=0
				while [ $ctrl_value -lt 1 ]; 
				do
					let ctrl_value=ctrl_value+1 
					total_data_rate=${total_data_rates[RANDOM%${#total_data_rates[@]}]}
				    echo "Select total_data_rate=$total_data_rate for experiment=$i"
					for selected in "${selected_total_data_rates[@]}" 
					do
						if [[ $selected -eq $total_data_rate ]]
						then
							ctrl_value=0
							 echo "total_data_rate=$total_data_rate has been already selected for experiment=$i"
						fi
					done
				done 
				selected_total_data_rates=(${selected_total_data_rates[@]} $total_data_rate)
				let counter=counter+1 
				#index=$(( ( RANDOM % 10 )  + 1 ))
				#total_data_rate=${total_data_rates[RANDOM%${#total_data_rates[@]}]}
				mvn exec:java -Dexec.mainClass="org.fog.placement.proposition.experiments.simulation.Start_RCS" -Dexec.args="RCS_UR 10 $total_data_rate $execution_counter y"
				echo "Looping for RCS[$execution_counter]: data stream=$total_data_rate, experiment=$i has finished!"
				let execution_counter=execution_counter+1 
				sleep 5
			done
done


