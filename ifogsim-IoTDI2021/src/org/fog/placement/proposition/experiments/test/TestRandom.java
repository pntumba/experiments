package org.fog.placement.proposition.experiments.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;


public class TestRandom {

	private static Map<String, Double> datasources = null;
	private static Random rand = new Random();
	private static double threshold = 800;
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		//networkCapacityFactor();
		 for(int i=0; i<15; i++) {
			//dataPoints();
			 dataPoints();
		}
		
	}
	
	
	
	public static void dataPointSequence() {
		
		ArrayList<Double> factors = new ArrayList<Double>(); 
		ArrayList<Double> generated = new ArrayList<Double>(); 
		        
		factors.add(512.0);factors.add(700.0);factors.add(965.0);factors.add(1221.0);factors.add(1500.0);
		factors.add(1792.0);factors.add(2000.0);factors.add(2100.0);factors.add(2256.0);
		int max = 9;
		int min = 1;
		int i = 0;
		while(i<max) {
			
			int index = rand.nextInt(max-min+1) + min;
			double datapoint = factors.get(index-1);
			if(!generated.contains(datapoint)) {
				
				generated.add(datapoint);
				i++;
			}
			
		}
		System.out.println(Arrays.toString(generated.toArray()));
	
		
	}
	public static void networkCapacityFactor() {
		
		Map<String, Double> fogResources = new ConcurrentHashMap<String, Double>();
		//double cm_Fj = 1024;
		double Bmax = 1650;
		int fogNumber = 4;
		//double total_cm_Fj = cm_Fj * 4;
		
		ArrayList<Double> factors = new ArrayList<Double>(); 
		factors.add(0.2);factors.add(0.4);factors.add(0.6);factors.add(0.8);factors.add(1.0);factors.add(1.2);
		factors.add(1.4);factors.add(1.6);factors.add(1.8);factors.add(2.0);
		
		for(double factor : factors) {
			
			double cm_Fj = Bmax * factor;
			System.out.println("Factor="+factor+" cm_Fj="+cm_Fj);
		}
	
		
	}
	
	public static void dataPoints() {
			
		double max = 2100;
		 int source_nbr = 4;
		 double min = 0.0;	
		 double total = 0.0;
		 while(total != max){	
			 
			 datasources = new ConcurrentHashMap<String, Double>();
			 total = generateInitialRate(min,max,source_nbr);
		 }
		 for(String source: datasources.keySet()) {
			 
			 System.out.print(","+datasources.get(source));
		 }
	}
	private static double generateInitialRate(double min, double max, int source_nbr) {
		
		boolean hasBig = false;
		double total = 0.0;
		for(int i=1; i<=source_nbr-1; i++) {
			 double x = rand.nextDouble()*(max-min) + min;
			 double rate = Math.ceil(x);
			 datasources.put("S_"+(i), rate);
			 if(rate > threshold) {
				 
				 hasBig = true;
			 }
			 total = total + rate; 
		 }
		if(total < max) {
			
			double rate = max - total; 
			datasources.put("S_"+source_nbr, rate);
			total = total + rate;
			if(rate > threshold) {
				 
				 hasBig = true;
			 }
		}
	
	
		if(hasBig){
			
			return generateInitialRate(min, max, source_nbr);
		}
		return total;
	}
	
	
}
