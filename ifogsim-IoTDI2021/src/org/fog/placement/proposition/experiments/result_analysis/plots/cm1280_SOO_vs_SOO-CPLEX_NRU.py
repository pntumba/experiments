import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [512,700,965,1221,1500,1792,2000,2100,2256]
NRU_SOO = [0.3531034483,0.4827586207,0.6655172414,0.8420689655,0.9763678161,0.9833103448,0.9828965517,0.9892413793,1.003586207]
NRU_SOOCPLEX = [0.3531034483,0.4827586207,0.6655172414,0.8420689655,0.9762298851,0.984137931,0.987908046,0.9912643678,1.003586207]
NRU_RCS = [0.3531034483,0.4827586207,0.6482298851,0.7016551724,0.8386206897,0.9365977011,1.006804598,1.034988506,1.042022989]

fig, ax = plt.subplots()
ax.plot(data_stream_rates, NRU_SOO, "-sr", label="SOO",markersize=8, markevery=2)
ax.plot(data_stream_rates, NRU_SOOCPLEX, "--^b", label="SOO-CPLEX", markersize=8, markevery=1)
ax.plot(data_stream_rates, NRU_RCS, "-ok", label="RCS", markersize=8)
ax.legend()
plt.ylabel('Network resource usage costs- NRU')
plt.xlabel('Data stream rate produced at the Edge (KB/sec)')
plt.show()


