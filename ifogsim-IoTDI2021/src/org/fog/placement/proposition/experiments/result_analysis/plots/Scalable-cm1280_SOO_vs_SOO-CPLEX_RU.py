import matplotlib.pyplot as plt
import numpy as np

data_stream_rates = [2000,3000,4000,5000,6000,7000,8000,9000,10000]
RU_SOO = [1.492491604,1.483683758,1.493587419,1.478146749,1.485146836,1.487307612,1.482578552,1.481244782,1.481108746]
RU_SOOCPLEX_OG_0_0 = [1.490741379,1.483538554,1.491885147,1.472698024,1.482179747,1.483213554,1.477301522,1.477553321,1.475700988]
RU_SOOCPLEX_OG_0_5 = [1.506873429,1.507622426,1.521704517,1.49967306,1.524362413,1.521022052,1.521022052,1.510130321,1.514047184]

fig, ax = plt.subplots()

ax.plot(data_stream_rates, RU_SOO, "-sr", label="SOO",markersize=8, markevery=2)
ax.plot(data_stream_rates, RU_SOOCPLEX_OG_0_0, "--^b", label="SOO-CPLEX with OG=0.0", markersize=8, markevery=1)
ax.plot(data_stream_rates, RU_SOOCPLEX_OG_0_5, "--ok", label="SOO-CPLEX with OG=0.5", markersize=8, markevery=1)
ax.legend()
ax.set_ylabel('Overall resource usage cost, RU')
ax.set_xlabel('Total data stream rate produced at the Edge (KB/sec)')
data_streams = [4,6,8,10,12,14,16,18,20]
x_pos = np.arange(len(data_streams))
plt.show()


