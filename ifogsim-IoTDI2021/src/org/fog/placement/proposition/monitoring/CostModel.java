package org.fog.placement.proposition.monitoring;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.cloudbus.cloudsim.core.CloudSim;
import org.fog.placement.proposition.utils.MinMax;
import org.fog.placement.proposition.utils.UtilFunctions;

public class CostModel {

	// Variables for the cost  model
	private static double comp_resource_usage_cost = 0.0;
	private static double netw_resource_usage_cost = 0.0;
	private static double norm_netw_resource_usage_cost = 0.0;
	private static double norm_comp_resource_usage_cost = 0.0;
	private static double resource_usage_cost = 0.0;
	private static double weight_for_comp = 1.0;
	private static double weight_for_netw = 1.0;
	private static double stdev = 0;
	
	
	// variables for the computational resources
	private static Map<Integer, Double> edge_cpu_memory_usage = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Double> edge_max_cpu_memory = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Double> fog_cpu_memory_usage = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Double> edgeCutMin_fog_cpu_memory_usage = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Double> fog_max_cpu_memory = new ConcurrentHashMap<Integer, Double>();
	
	// Fill the gap when resources are released from Fog
	private static Map<Integer, Double> fog_cpu_memory_released = new ConcurrentHashMap<Integer, Double>();
	
	private static double cloud_cpu_memory_usage;
	private static double cloud_max_cpu_memory;
	
	// Variables for the network resources
	private static Map<Integer, Double>  fog_to_cloud_netw_bwd_usage = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Double>  edgeCutMin_fog_to_cloud_netw_bwd_usage = new ConcurrentHashMap<Integer, Double>();
	private static double fog_to_cloud_max_netw_bwd = 0.0;
	private static double fog_to_cloud_overall_netw_bwd_usage;
	private static double fog_to_cloud_upper_threshold_netw_bwd;
	private static double fog_to_cloud_lower_threshold_netw_bwd;
	private static Map<Integer, Double> fog_to_cloud_netw_delay = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Map<Integer, Double>>  edge_to_fog_netw_bwd_usage = new ConcurrentHashMap<Integer, Map<Integer,Double>>();
	private static Map<Integer, Double> edge_to_fog_max_netw_bwd = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Double>  edge_to_fog_netw_delay = new ConcurrentHashMap<Integer, Double>();
	
	// Variables used in SOO approach
	private static Map<Integer, Double> fog_comp_resource_usage_cost = new ConcurrentHashMap<Integer, Double>();
	private static Map<Integer, Double> fog_to_cloud_netw_resource_usage_cost = new ConcurrentHashMap<Integer, Double>();
	
	private static int source_number;
	
	
	
	public static void calculateStdev(){
	
		int N = getFog_comp_resource_usage_cost().size() + 1;
		double aggregated_resource_usage = 0;
		int i =0;
		double[] array_aggregated_resource_usage = new double[N];
		array_aggregated_resource_usage[i] = getNetw_resource_usage_cost();
		aggregated_resource_usage += array_aggregated_resource_usage[i];
		i=1;
		
		//Fill the population set
		for(int F_j : getFog_comp_resource_usage_cost().keySet()){
			
			array_aggregated_resource_usage[i] = getFog_comp_resource_usage_cost().get(F_j);
			aggregated_resource_usage +=  array_aggregated_resource_usage[i]; 
			i++;
		}
		
		//Compute the mean value
		double mean_of_aggregated_resource_usage = aggregated_resource_usage/N;
		
		//Compute the standard deviation
		double[] deviations_from_mean = new double[N];
		double[] sqrt_deviations_from_mean = new double[N];
		double mean_sqrt_deviations_from_mean = 0;
		for(int j = 0; j< N; j++) {
			
			deviations_from_mean[j] = array_aggregated_resource_usage[j] - mean_of_aggregated_resource_usage;	
			sqrt_deviations_from_mean[j] = Math.pow(deviations_from_mean[j],2);
			mean_sqrt_deviations_from_mean += sqrt_deviations_from_mean[j];
		}
		CostModel.setStdev(Math.sqrt(mean_sqrt_deviations_from_mean/N));
	}
	
	public static void calculateComp_resource_usage_cost(){
		
		//System.err.println("fog_cpu_memory_usage "+fog_cpu_memory_usage);
		// System.err.println("fog_max_cpu_memory "+fog_max_cpu_memory);
		comp_resource_usage_cost = 0.0;
		for(int F_j : fog_cpu_memory_usage.keySet()) {
			
			double fog_comp_resource_usage_cost = fog_cpu_memory_usage.get(F_j) * (1 / fog_max_cpu_memory.get(F_j));
			getFog_comp_resource_usage_cost().put(F_j, fog_comp_resource_usage_cost);
			comp_resource_usage_cost +=  fog_comp_resource_usage_cost; 
			
		}
	}
	
	public static void calculateNetw_resource_usage_cost(){
		
		netw_resource_usage_cost = 0.0;
		double  min_netw_delay = calculateEdge_to_fog_min_netw_delay();
		
		//Compute Fog to Cloud network resource usage cost
		fog_to_cloud_overall_netw_bwd_usage = 0.0;
		for(int F_j : fog_to_cloud_netw_bwd_usage.keySet()){
			
			double cloud_netw_bwd_usage = fog_to_cloud_netw_bwd_usage.get(F_j);
			double cloud_netw_bwd_weight = 1/fog_to_cloud_max_netw_bwd;
			double cloud_netw_delay_weight = fog_to_cloud_netw_delay.get(F_j)/min_netw_delay;
			double fog_to_cloud_netw_resource_usage_cost = cloud_netw_bwd_usage * cloud_netw_bwd_weight * cloud_netw_delay_weight;
			getFog_to_cloud_netw_resource_usage_cost().put(F_j,fog_to_cloud_netw_resource_usage_cost); 
			netw_resource_usage_cost += fog_to_cloud_netw_resource_usage_cost;
			fog_to_cloud_overall_netw_bwd_usage += fog_to_cloud_netw_bwd_usage.get(F_j);	
		}
	}
	
	public static void calculateResource_usage_cost(){
		
		norm_netw_resource_usage_cost = (getNetw_resource_usage_cost() - UtilFunctions.netwMinMax.getMin())/(UtilFunctions.netwMinMax.getMax() -  UtilFunctions.netwMinMax.getMin());
		
		if(Double.isNaN(norm_netw_resource_usage_cost)) {
			
			norm_netw_resource_usage_cost = 0.0;
		}

		norm_comp_resource_usage_cost = (getComp_resource_usage_cost() - UtilFunctions.compMinMax.getMin())/(UtilFunctions.compMinMax.getMax() - UtilFunctions.compMinMax.getMin());
		if(Double.isNaN(norm_comp_resource_usage_cost)) {
			
			norm_comp_resource_usage_cost = 0.0;
		}
		resource_usage_cost = weight_for_comp * norm_comp_resource_usage_cost + weight_for_netw * norm_netw_resource_usage_cost;
	}
	
	

	
	public static void addEdge_to_fog_netw_bwd_usage(int F_j, int E_i, double netw_bwd_usage){
		
		if(edge_to_fog_netw_bwd_usage.get(F_j) == null) {
			
			Map<Integer, Double> netw_bwd_usage_map = new ConcurrentHashMap<Integer, Double>();
			netw_bwd_usage_map.put(E_i, netw_bwd_usage);
			edge_to_fog_netw_bwd_usage.put(F_j, netw_bwd_usage_map);
			
		}else {
			
			edge_to_fog_netw_bwd_usage.get(F_j).put(E_i, netw_bwd_usage);
		}
		// TODO Auto-generated method stub
		
	}

	public static void addFog_cpu_memory_usage(int F_j, double cpu_memory_usage, double max_cpu_memory) {
		
		if(!getFog_cpu_memory_usage().containsKey(F_j)){
			
			getFog_cpu_memory_usage().put(F_j, 0.0);
		}
		getFog_max_cpu_memory().put(F_j, max_cpu_memory);
	}


	private static double calculateEdge_to_fog_min_netw_delay() {
		
		double edge_to_fog_min_netw_delay = Double.MAX_VALUE;
		for(int i: edge_to_fog_netw_delay.keySet()) {
			
			if(edge_to_fog_netw_delay.get(i) < edge_to_fog_min_netw_delay){
				
				edge_to_fog_min_netw_delay = edge_to_fog_netw_delay.get(i);
			}
		}
		return edge_to_fog_min_netw_delay;
		
	} 
	 public static void printOut(){
		 
		CostModel.calculateComp_resource_usage_cost();
		CostModel.calculateNetw_resource_usage_cost();
		CostModel.calculateResource_usage_cost();

		System.out.println("\tInitial B=" + CostModel.getFog_to_cloud_overall_netw_bwd_usage());
		System.out.println("\tInitial CRU=" + CostModel.getNorm_comp_resource_usage_cost());
		System.out.println("\tInitial NRU=" + CostModel.getNorm_netw_resource_usage_cost());
		System.out.println("\tInitial RU=" + CostModel.getResource_usage_cost());
		
	 }
	 
	 public static void clear(int F_j) {
		 
		 CostModel.getFog_cpu_memory_usage().remove(F_j);
		 CostModel.getFog_to_cloud_netw_bwd_usage().remove(F_j);
	 }
	public static double getWeight_for_comp(){
		return weight_for_comp;
	}

	public static void setWeight_for_comp(double weight_for_comp) {
		CostModel.weight_for_comp = weight_for_comp;
	}

	public static double getWeight_for_netw(){
		
		return weight_for_netw;
	}

	public static void setWeight_for_netw(double weight_for_netw) {
		CostModel.weight_for_netw = weight_for_netw;
	}

	public static void setComp_resource_usage_cost(double comp_resource_usage_cost) {
		CostModel.comp_resource_usage_cost = comp_resource_usage_cost;
	}
	public static Map<Integer, Map<Integer, Double>> getEdge_to_fog_netw_bwd_usage() {
		return edge_to_fog_netw_bwd_usage;
	}
	public static void setEdge_to_fog_netw_bwd_usage(Map<Integer, Map<Integer, Double>> edge_to_fog_netw_bwd_usage) {
		CostModel.edge_to_fog_netw_bwd_usage = edge_to_fog_netw_bwd_usage;
	}
	public static Map<Integer, Double> getEdge_to_fog_max_netw_bwd() {
		return edge_to_fog_max_netw_bwd;
	}
	public static void setEdge_to_fog_max_netw_bwd(Map<Integer, Double> edge_to_fog_max_netw_bwd) {
		CostModel.edge_to_fog_max_netw_bwd = edge_to_fog_max_netw_bwd;
	}
	public static Map<Integer, Double> getEdge_to_fog_netw_delay() {
		return edge_to_fog_netw_delay;
	}
	public static void setEdge_to_fog_netw_delay(Map<Integer, Double> edge_to_fog_netw_delay) {
		CostModel.edge_to_fog_netw_delay = edge_to_fog_netw_delay;
	}
	
	public static Map<Integer, Double> getFog_cpu_memory_usage() {
		return fog_cpu_memory_usage;
	}
	public static void setFog_cpu_memory_usage(Map<Integer, Double> fog_cpu_memory_usage) {
		CostModel.fog_cpu_memory_usage = fog_cpu_memory_usage;
	}
	public static Map<Integer, Double> getFog_max_cpu_memory() {
		return fog_max_cpu_memory;
	}
	public static void setFog_max_cpu_memory(Map<Integer, Double> fog_max_cpu_memory) {
		CostModel.fog_max_cpu_memory = fog_max_cpu_memory;
	}

	
	public static double getComp_resource_usage_cost() {
		return comp_resource_usage_cost;
	}
	

	public static double getNetw_resource_usage_cost() {
		return netw_resource_usage_cost;
	}
	
	public static void setNetw_resource_usage_cost(double netw_resource_usage_cost) {
		CostModel.netw_resource_usage_cost = netw_resource_usage_cost;
	}

	public static double getNorm_netw_resource_usage_cost() {
		return norm_netw_resource_usage_cost;
	}

	public static void setNorm_netw_resource_usage_cost(double norm_netw_resource_usage_cost) {
		CostModel.norm_netw_resource_usage_cost = norm_netw_resource_usage_cost;
	}

	public static double getNorm_comp_resource_usage_cost() {
		return norm_comp_resource_usage_cost;
	}

	public static void setNorm_comp_resource_usage_cost(double norm_comp_resource_usage_cost) {
		CostModel.norm_comp_resource_usage_cost = norm_comp_resource_usage_cost;
	}

	public static double getFog_to_cloud_overall_netw_bwd_usage() {
		return fog_to_cloud_overall_netw_bwd_usage;
	}

	public static void setFog_to_cloud_overall_netw_bwd_usage(double fog_to_cloud_overall_netw_bwd_usage) {
		CostModel.fog_to_cloud_overall_netw_bwd_usage = fog_to_cloud_overall_netw_bwd_usage;
	}
	public static double getFog_to_cloud_upper_threshold_netw_bwd() {
		return fog_to_cloud_upper_threshold_netw_bwd;
	}
	public static void setFog_to_cloud_upper_threshold_netw_bwd(double fog_to_cloud_upper_threshold_netw_bwd) {
		CostModel.fog_to_cloud_upper_threshold_netw_bwd = fog_to_cloud_upper_threshold_netw_bwd;
		CostModel.setFog_to_cloud_max_netw_bwd(fog_to_cloud_upper_threshold_netw_bwd);
	}
	public static Map<Integer, Double> getFog_to_cloud_netw_delay() {
		return fog_to_cloud_netw_delay;
	}
	public static void setFog_to_cloud_netw_delay(Map<Integer, Double> fog_to_cloud_netw_delay) {
		CostModel.fog_to_cloud_netw_delay = fog_to_cloud_netw_delay;
	}
	public static double getCloud_max_cpu_memory() {
		return cloud_max_cpu_memory;
	}
	public static void setCloud_max_cpu_memory(double cloud_max_cpu_memory) {
		CostModel.cloud_max_cpu_memory = cloud_max_cpu_memory;
	}
	public static double getCloud_cpu_memory_usage() {
		return cloud_cpu_memory_usage;
	}
	public static void setCloud_cpu_memory_usage(double cloud_cpu_memory_usage) {
		CostModel.cloud_cpu_memory_usage = cloud_cpu_memory_usage;
	}
	public static double getFog_to_cloud_lower_threshold_netw_bwd() {
		return fog_to_cloud_lower_threshold_netw_bwd;
	}
	public static void setFog_to_cloud_lower_threshold_netw_bwd(double fog_to_cloud_lower_threshold_netw_bwd) {
		CostModel.fog_to_cloud_lower_threshold_netw_bwd = fog_to_cloud_lower_threshold_netw_bwd;
	}
	public static Map<Integer, Double> getEdge_cpu_memory_usage() {
		return edge_cpu_memory_usage;
	}
	public static void setEdge_cpu_memory_usage(Map<Integer, Double> edge_cpu_memory_usage) {
		CostModel.edge_cpu_memory_usage = edge_cpu_memory_usage;
	}
	public static Map<Integer, Double> getEdge_max_cpu_memory() {
		return edge_max_cpu_memory;
	}
	public static void setEdge_max_cpu_memory(Map<Integer, Double> edge_max_cpu_memory) {
		CostModel.edge_max_cpu_memory = edge_max_cpu_memory;
	}

	public static Map<Integer, Double> getFog_to_cloud_netw_bwd_usage() {
		return fog_to_cloud_netw_bwd_usage;
	}

	public static void setFog_to_cloud_netw_bwd_usage(Map<Integer, Double> fog_to_cloud_netw_bwd_usage) {
		CostModel.fog_to_cloud_netw_bwd_usage = fog_to_cloud_netw_bwd_usage;
	}

	public static double getFog_to_cloud_max_netw_bwd() {
		return fog_to_cloud_max_netw_bwd;
	}

	public static void setFog_to_cloud_max_netw_bwd(double fog_to_cloud_max_netw_bwd) {
		CostModel.fog_to_cloud_max_netw_bwd = fog_to_cloud_max_netw_bwd;
	}

	public static Map<Integer, Double> getFog_comp_resource_usage_cost() {
		return fog_comp_resource_usage_cost;
	}

	public static void setFog_comp_resource_usage_cost(Map<Integer, Double> fog_comp_resource_usage_cost) {
		CostModel.fog_comp_resource_usage_cost = fog_comp_resource_usage_cost;
	}

	public static Map<Integer, Double> getFog_to_cloud_netw_resource_usage_cost() {
		return fog_to_cloud_netw_resource_usage_cost;
	}

	public static void setFog_to_cloud_netw_resource_usage_cost(Map<Integer, Double> fog_to_cloud_netw_resource_usage_cost) {
		CostModel.fog_to_cloud_netw_resource_usage_cost = fog_to_cloud_netw_resource_usage_cost;
	}

	public static double getResource_usage_cost() {
		return resource_usage_cost;
	}

	public static void setResource_usage_cost(double resource_usage_cost) {
		CostModel.resource_usage_cost = resource_usage_cost;
	}

	public static int getSource_number(){
		return source_number;
	}

	public static void setSource_number(int source_number) {
		CostModel.source_number = source_number;
	}



	public static Map<Integer, Double> getFog_cpu_memory_released() {
		return fog_cpu_memory_released;
	}

	public static void setFog_cpu_memory_released(Map<Integer, Double> fog_cpu_memory_released) {
		CostModel.fog_cpu_memory_released = fog_cpu_memory_released;
	}

	public static double getStdev() {
		return stdev;
	}

	public static void setStdev(double stdev) {
		CostModel.stdev = stdev;
	}

	public static Map<Integer, Double> getEdgeCutMin_fog_cpu_memory_usage() {
		return edgeCutMin_fog_cpu_memory_usage;
	}

	public static void setEdgeCutMin_fog_cpu_memory_usage(Map<Integer, Double> edgeCutMin_fog_cpu_memory_usage) {
		CostModel.edgeCutMin_fog_cpu_memory_usage = edgeCutMin_fog_cpu_memory_usage;
	}

	public static Map<Integer, Double> getEdgeCutMin_fog_to_cloud_netw_bwd_usage() {
		return edgeCutMin_fog_to_cloud_netw_bwd_usage;
	}

	public static void setEdgeCutMin_fog_to_cloud_netw_bwd_usage(Map<Integer, Double> edgeCutMin_fog_to_cloud_netw_bwd_usage) {
		CostModel.edgeCutMin_fog_to_cloud_netw_bwd_usage = edgeCutMin_fog_to_cloud_netw_bwd_usage;
	}

	

	
	
	
}
