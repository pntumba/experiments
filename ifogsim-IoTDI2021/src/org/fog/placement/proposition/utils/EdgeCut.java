package org.fog.placement.proposition.utils;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EdgeCut {
	
	private  Map<Integer, ArrayList<Integer>> edgeCut = null;
	
	public EdgeCut() {
		
		edgeCut = new ConcurrentHashMap<Integer, ArrayList<Integer>>();
	}
	public EdgeCut(Map<Integer, ArrayList<Integer>> edgeCut) {
		// TODO Auto-generated constructor stub
		setEdgeCut(edgeCut);
	}

	public Map<Integer, ArrayList<Integer>> getEdgeCut() {
		return edgeCut;
	}

	public void setEdgeCut(Map<Integer, ArrayList<Integer>> edgeCut) {
		this.edgeCut = edgeCut;
	}
	
	
}
